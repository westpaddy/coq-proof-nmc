Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity NMC_Step NMC_EquivType NMC_TypingClosed NMC_TypingWeaken.

Definition subst_env x u E :=
  map (option_map (subst x u)) E.

Lemma subst_env_empty : forall x u,
    subst_env x u empty = empty.
Proof. unfold subst_env. intros. rewrite~ map_empty. Qed.

Lemma subst_env_singleton : forall x u y T,
    subst_env x u (y *~ T) = (y *~ ([x ~> u]T)).
Proof. unfold subst_env. intros. rewrite~ map_single. Qed.

Lemma subst_env_concat : forall x u E F,
    subst_env x u (E & F) = subst_env x u E & subst_env x u F.
Proof. unfold subst_env. intros. rewrite~ map_concat. Qed.

Lemma subst_env_push : forall x u E y T,
    subst_env x u (E & y *~ T) = subst_env x u E & y *~ ([x ~> u]T).
Proof. unfold subst_env. intros. rewrite~ map_push. Qed.

Lemma subst_env_push_s : forall x u E y,
    subst_env x u (E & 'y) = subst_env x u E & 'y.
Proof. unfold subst_env. intros. rewrite~ map_push. Qed.

Tactic Notation "apply_subst_ih" "*" constr(H) :=
  rewrite <- concat_assoc;
  first [rewrite <- subst_env_push | rewrite <- subst_env_push_s];
  apply* H; try solve [rewrite concat_assoc; reflexivity].

Local Hint Extern 1 (_ # ?E) =>
match E with
| context [ subst_env _ _ _ ] => unfold subst_env; repeat simpl_dom; notin_solve
end.

Lemma wf_bits_subst : forall E F x T u b,
    wf_bits (E & x *~ T & F) b ->
    wf_bits (E & subst_env x u F) b.
Proof.
  introv Wf. gen_eq E': (E & x *~ T & F). move: E F x T u.
  induction Wf; introv Eq; subst; simpls*.
  - apply wf_bits_var. binds_cases H.
    + apply* binds_concat_left.
    + apply binds_concat_right.
      replace None with (option_map (subst x0 u) None); last by simpl.
      apply~ binds_map.
Qed.

Lemma wf_typ_typing_subst :
    (forall E' D T, E' |=D T -> forall E F x T' u,
          E' = (E & x *~ T' & F) ->
          E |=D u ~: T' ->
          (E & subst_env x u F) |=D ([x ~> u]T)) /\
    (forall E' D e T, E' |=D e ~: T -> forall E F x T' u,
          E' = (E & x *~ T' & F) ->
          E |=D u ~: T' ->
          (E & subst_env x u F) |=D ([x ~> u]e) ~: ([x ~> u]T)).
Proof.
  apply wf_typ_typing_ind; intros;
    try solve [false* empty_push_inv];
    try solve [false* empty_middle_inv];
    subst; simpls*.
  - apply_fresh wf_typ_arrow. rewrite~ subst_open_var. apply_subst_ih* H0.
  - apply_fresh~ wf_typ_forall. rewrite~ subst_open_var_s. apply_subst_ih* H0.
  - apply_fresh wf_typ_refine. rewrite~ subst_open_var. apply_subst_ih* H0.
  - destruct F using env_ind; destruct_eq_push H4.
    + rewrite subst_env_empty. rew_env_concat.
      rewrite~ subst_fresh. apply* wf_typ_closed.
    + rewrite subst_env_push. rewrite concat_assoc. apply* wf_typ_weaken.
  - destruct F using env_ind; destruct_eq_push H2.
    + inversion H4.
    + rewrite subst_env_push_s. rewrite concat_assoc. apply* wf_typ_sweaken.
  - destruct F using env_ind; destruct_eq_push H2.
    + case_var. rewrite subst_env_empty. rew_env_concat.
      rewrite~ subst_fresh. apply* typing_closed_typ.
    + case_var.
      * simpl_dom. notin_false.
      * rewrite subst_env_push. rew_env_concat. apply* typing_var.
  - rewrite~ subst_open. apply* typing_app.
  - rewrite~ subst_open. apply* typing_sapp. rewrite subst_fresh; first by apply* wf_bits_closed.
    apply* wf_bits_subst.
  - apply_fresh typing_fix. rewrite~ subst_open_var2. rewrite~ subst_open_var.
    replace (trm_arrow ([x ~> u]T1) ([x ~> u]T2)) with ([x ~> u](trm_arrow T1 T2)); last by simpl.
    do 2 rewrite <- subst_env_singleton. do 2 rewrite <- concat_assoc. do 2 rewrite <- subst_env_concat.
    apply* H0.
    by rew_env_concat.
  - apply_fresh~ typing_sabs. do 2 rewrite~ subst_open_var_s. apply_subst_ih* H0.
  - rewrite [_ b] subst_fresh; first by apply* wf_bits_closed.
    apply typing_choice.
    + apply* H0. apply* (proj2 wf_typ_typing_orth_weaken). apply* extend_orthant_l.
    + apply* H2. apply* (proj2 wf_typ_typing_orth_weaken). apply* extend_orthant_r.
    + apply* H4.
    + apply* wf_bits_subst.
  - apply* typing_cast. apply* consist_typ_subst.
  - destruct F using env_ind; destruct_eq_push H4.
    + rewrite subst_env_empty. rew_env_concat.
      rewrite~ subst_fresh. rewrite~ subst_fresh. apply* typing_closed_typ. apply* typing_closed_exp.
    + rewrite subst_env_push. rewrite concat_assoc. apply* typing_weaken.
  - destruct F using env_ind; destruct_eq_push H2.
    + inversion H4.
    + rewrite subst_env_push_s. rewrite concat_assoc. apply* typing_sweaken.
Qed.

Definition subst_s_env x u E :=
  map (option_map (subst_s x u)) E.

Lemma subst_s_env_empty : forall x u,
    subst_s_env x u empty = empty.
Proof. unfold subst_s_env. intros. rewrite~ map_empty. Qed.

Lemma subst_s_env_singleton : forall x u y T,
    subst_s_env x u (y *~ T) = (y *~ (subst_s x u T)).
Proof. unfold subst_s_env. intros. rewrite~ map_single. Qed.

Lemma subst_s_env_concat : forall x u E F,
    subst_s_env x u (E & F) = subst_s_env x u E & subst_s_env x u F.
Proof. unfold subst_s_env. intros. rewrite~ map_concat. Qed.

Lemma subst_s_env_push : forall x u E y T,
    subst_s_env x u (E & y *~ T) = subst_s_env x u E & y *~ (subst_s x u T).
Proof. unfold subst_s_env. intros. rewrite~ map_push. Qed.

Lemma subst_s_env_push_s : forall x u E y,
    subst_s_env x u (E & 'y) = subst_s_env x u E & 'y.
Proof. unfold subst_s_env. intros. rewrite~ map_push. Qed.

Tactic Notation "apply_subst_s_ih" "*" constr(H) :=
  rewrite <- concat_assoc;
  first [rewrite <- subst_s_env_push | rewrite <- subst_s_env_push_s];
  apply* H; try solve [rewrite concat_assoc; reflexivity].

Local Hint Extern 1 (_ # ?E) =>
match E with
| context [ subst_s_env _ _ _ ] => unfold subst_s_env; repeat simpl_dom; notin_solve
end.

Lemma wf_bits_subst_s : forall E F x u b,
    wf_bits (E & 'x & F) b ->
    wf_bits E u ->
    ok (E & subst_s_env x u F) ->
    wf_bits (E & subst_s_env x u F) (subst_s x u b).
Proof.
  introv Wf. gen_eq E': (E & 'x & F). move: E F x u.
  induction Wf; introv Eq; subst; intros; simpls*.
  - case_var.
    + replace (E0 & subst_s_env x0 u F) with (E0 & subst_s_env x0 u F & empty); last by rewrite concat_empty_r.
      apply wf_bits_weaken.
      * rew_env_concat. apply H0.
      * rew_env_concat. assumption.
    + apply wf_bits_var. binds_cases H.
      * apply* binds_concat_left.
      * apply* binds_concat_right.
        replace None with (option_map (subst_s x0 u) None); last by simpl.
        apply* binds_map.
Qed.

Lemma wf_typ_typing_orthant_subst_s :
  (forall E D T, E |=D T -> forall x b, x # E -> bits b -> E |=(Im D (subst_s_axis x b)) T) /\
  (forall E D e T, E |=D e ~: T -> forall x b, x # E -> bits b -> E |=(Im D (subst_s_axis x b)) e ~: T).
Proof.
  apply wf_typ_typing_ind; intros; eauto.
  - apply_fresh* wf_typ_forall. apply (include_fv_s_orth_Im x b) in H1. auto.
  - apply_fresh* typing_sabs. apply (include_fv_s_orth_Im x b) in H1. auto.
  - apply* typing_choice.
    + rewrite <- (@subst_s_fresh x b0 b); last by apply* wf_bits_closed_s.
      replace (inl (subst_s x b0 b)) with (subst_s_axis x b0 (inl b)); last by easy.
      rewrite* <- Im_add.
    + rewrite <- (@subst_s_fresh x b0 b); last by apply* wf_bits_closed_s.
      replace (inr (subst_s x b0 b)) with (subst_s_axis x b0 (inr b)); last by easy.
      rewrite* <- Im_add.
  - apply* typing_active. apply* mstep_orthant_subst_s.
    set K1 := (typing_closed_s_exp H2 H7).
    set K2 := (wf_typ_closed_s H4 H7). simpl in K2.
    apply* open_fv_s.
  - apply* typing_conv. apply* equiv_typ_orthant_subst_s.
  - apply* typing_exact. apply* mstep_orthant_subst_s.
    set K1 := (typing_closed_s_exp H0 H5).
    set K2 := (wf_typ_closed_s H2 H5). simpl in K2.
    apply* open_fv_s.
Qed.

Lemma wf_typ_typing_subst_s :
  (forall E' D T, E' |=D T -> forall E F x u,
        E' = (E & 'x & F) ->
        wf_bits E u ->
        (E & subst_s_env x u F) |=(Im D (subst_s_axis x u)) (subst_s x u T)) /\
  (forall E' D e T, E' |=D e ~: T -> forall E F x u,
        E' = (E & 'x & F) ->
        wf_bits E u ->
        (E & subst_s_env x u F) |=(Im D (subst_s_axis x u)) (subst_s x u e) ~: (subst_s x u T)).
Proof.
  apply wf_typ_typing_ind; intros;
    try solve [false* empty_push_inv];
    try solve [false* empty_middle_inv];
    subst; simpls*.
  - apply_fresh wf_typ_arrow. rewrite* subst_s_open_var. apply_subst_s_ih* H0.
  - apply_fresh wf_typ_forall.
    + rewrite* subst_s_open_var_s. apply_subst_s_ih* H0.
    + apply (include_fv_s_orth_Im x u) in H1. auto.
  - apply_fresh wf_typ_refine. rewrite* subst_s_open_var. apply_subst_s_ih* H0.
  - destruct F using env_ind; destruct_eq_push H4.
    + inversion H6.
    + rewrite concat_assoc_map_push. apply* wf_typ_weaken.
  - destruct F using env_ind; destruct_eq_push H2.
    + rewrite subst_s_env_empty. rew_env_concat.
      rewrite subst_s_fresh.
      * apply* wf_typ_closed_s.
      * apply* (proj1 wf_typ_typing_orthant_subst_s).
    + rewrite concat_assoc_map_push. simpl. apply* wf_typ_sweaken.
  - destruct F using env_ind; destruct_eq_push H2.
    + inversion H4.
    + rewrite concat_assoc_map_push. apply* typing_var.
  - rewrite* subst_s_open.
  - rewrite* subst_s_open. apply* typing_sapp. apply* wf_bits_subst_s.
    apply environment_ok. apply* (proj2 wf_typ_typing_regular).
  - apply_fresh typing_fix. rewrite* subst_s_open_var2. rewrite* subst_s_open_var.
    replace (trm_arrow (subst_s x u T1) (subst_s x u T2)) with (subst_s x u (trm_arrow T1 T2)); last by simpl.
    do 2 rewrite <- subst_s_env_singleton. do 2 rewrite <- concat_assoc. do 2 rewrite <- subst_s_env_concat.
    apply* H0. by rew_env_concat.
  - apply_fresh typing_sabs.
    + do 2 rewrite* subst_s_open_var_s. apply_subst_s_ih* H0.
    + apply (include_fv_s_orth_Im x u) in H1. auto.
  - apply typing_choice.
    + replace (inl (subst_s x u b)) with (subst_s_axis x u (inl b)); last by easy.
      rewrite <- Im_add. apply* H0.
    + replace (inr (subst_s x u b)) with (subst_s_axis x u (inr b)); last by easy.
      rewrite <- Im_add. apply* H2.
    + apply* H4.
    + apply* wf_bits_subst_s. apply* environment_ok. apply* (proj1 wf_typ_typing_regular).
  - apply* typing_cast. apply* consist_typ_subst_s.
  - destruct F using env_ind; destruct_eq_push H4.
    + discriminate.
    + rewrite concat_assoc_map_push. apply* typing_weaken.
  - destruct F using env_ind; destruct_eq_push H2.
    + rewrite subst_s_env_empty. rew_env_concat.
      rewrite subst_s_fresh; first by apply* typing_closed_s_exp.
      rewrite subst_s_fresh; first by apply* typing_closed_s_typ.
      apply* (proj2 wf_typ_typing_orthant_subst_s).
    + rewrite concat_assoc_map_push. simpl. apply* typing_sweaken.
Qed.

Lemma wf_typ_subst : forall E x u T' D T,
    E & x *~ T' |=D T ->
    E |=D u ~: T' ->
    E |=D [x ~> u]T.
Proof.
  intros. replace E with (E & subst_env x u empty).
  - apply* wf_typ_typing_subst. by rew_env_concat.
  - rewrite subst_env_empty. by rew_env_concat.
Qed.

Lemma typing_subst : forall E x u T' D e T,
    E & x *~ T' |=D e ~: T ->
    E |=D u ~: T' ->
    E |=D [x ~> u]e ~: [x ~> u]T.
Proof.
  intros. replace E with (E & subst_env x u empty).
  - apply* wf_typ_typing_subst. by rew_env_concat.
  - rewrite subst_env_empty. by rew_env_concat.
Qed.

Lemma wf_typ_subst_s : forall E x u D T,
    E & 'x |=D T ->
    wf_bits E u ->
    E |=(Im D (subst_s_axis x u)) (subst_s x u T).
Proof.
  intros. replace E with (E & subst_s_env x u empty).
  - apply* wf_typ_typing_subst_s. by rew_env_concat.
  - rewrite subst_s_env_empty. by rew_env_concat.
Qed.

Lemma typing_subst_s : forall E x u D e T,
    E & 'x |=D e ~: T ->
    wf_bits E u ->
    E |=(Im D (subst_s_axis x u)) (subst_s x u e) ~: (subst_s x u T).
Proof.
  intros. replace E with (E & subst_s_env x u empty).
  - apply* wf_typ_typing_subst_s. by rew_env_concat.
  - rewrite subst_s_env_empty. by rew_env_concat.
Qed.