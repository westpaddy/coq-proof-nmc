Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity NMC_Step NMC_EquivType.

Inductive extend_orthant : orth -> orth -> Prop :=
| extend_orthant_l : forall D b,
    bits b ->
    extend_orthant D (D ⊌ inl b)
| extend_orthant_r : forall D b,
    bits b ->
    extend_orthant D (D ⊌ inr b).

Notation "D ⪯ D'" := (extend_orthant D D') (at level 54).

Lemma extend_is_super : forall D D',
    D ⪯ D' ->
    D ⊆ D'.
Proof. introv Ext. inverts Ext; auto with sets. Qed.

Local Hint Resolve extend_is_super.

Lemma extend_orthant_add_l : forall D D' b,
    bits b ->
    D ⪯ D' ->
    D ⊌ inl b ⪯ D' ⊌ inl b.
Proof.
  introv Bit Ext. inverts Ext.
  - rewrite Add_commutative. apply~ extend_orthant_l.
  - rewrite Add_commutative. apply~ extend_orthant_r.
Qed.

Lemma extend_orthant_add_r : forall D D' b,
    bits b ->
    D ⪯ D' ->
    D ⊌ inr b ⪯ D' ⊌ inr b.
Proof.
  introv Bit Ext. inverts Ext.
  - rewrite Add_commutative. apply~ extend_orthant_l.
  - rewrite Add_commutative. apply~ extend_orthant_r.
Qed.

Lemma extend_orthant_regular : forall D D',
    orthant D ->
    D ⪯ D' ->
    orthant D'.
Proof.
  inversion 2; subst.
  - apply~ orthant_union_combine. apply orthant_intro.
    + intros. apply Singleton_inv in H2. inverts~ H2.
    + intros. apply Singleton_inv in H2. inverts~ H2.
  - apply~ orthant_union_combine. apply orthant_intro.
    + intros. apply Singleton_inv in H2. inverts~ H2.
    + intros. apply Singleton_inv in H2. inverts~ H2.
Qed.

Local Hint Extern 1 (orthant ?D') =>
match goal with
| [ H: ?D ⪯ D' |- _ ] => simple apply (@extend_orthant_regular D D')
end.

Lemma extend_orthant_wrap : forall D D' L,
    D ⪯ D' ->
    include_fv_s_orth L D ->
    exists L', (forall x, x \notin L' -> x \notin L) /\ include_fv_s_orth L' D'.
Proof.
  intros. inverts H0. inverts H.
  - exists (L \u fv_s b). split; first by auto. apply include_fv_s_orth_intro.
    + intros. apply Add_inv in H as [H | H].
      * auto.
      * inverts* H.
    + intros. apply Add_inv in H as [H | H].
      * auto.
      * discriminate.
  - exists (L \u fv_s b). split; first by auto. apply include_fv_s_orth_intro.
    + intros. apply Add_inv in H as [H | H].
      * auto.
      * discriminate.
    + intros. apply Add_inv in H as [H | H].
      * auto.
      * inverts* H.
Qed.

Lemma wf_typ_typing_orth_weaken :
  (forall E D T, E |=D T -> forall D', D ⪯ D' -> E |=D' T) /\
  (forall E D e T, E |=D e ~: T -> forall D', D ⪯ D' -> E |=D' e ~: T).
Proof.
  apply wf_typ_typing_ind; intros; eauto.
  - destruct (extend_orthant_wrap H2 H1) as [L' [K1 K2]].
    apply~ (@wf_typ_forall L').
  - destruct (extend_orthant_wrap H2 H1) as [L' [K1 K2]].
    apply~ (@typing_sabs L').
  - apply~ typing_choice.
    + apply~ H0. apply~ extend_orthant_add_l.
    + apply~ H2. apply~ extend_orthant_add_r.
  - apply~ typing_active. apply* mstep_orth_weaken.
  - apply~ typing_conv. apply* equiv_typ_orth_weaken.
  - apply~ typing_exact. apply* mstep_orth_weaken.
Qed.

Lemma wf_bits_weaken : forall E F G b,
    wf_bits (E & G) b ->
    ok (E & F & G) ->
    wf_bits (E & F & G) b.
Proof.
  introv Wf. gen_eq E': (E & G). move: E F G.
  induction Wf; intros; subst; auto.
  - apply wf_bits_var. apply* binds_weaken.
Qed.

Lemma include_fv_s_orth_wrap : forall D L L',
    (forall x, x \notin L' -> x \notin L) ->
    include_fv_s_orth L D ->
    include_fv_s_orth L' D.
Proof.
  inversion 2. intros. apply include_fv_s_orth_intro; auto.
Qed.

Hint Extern 1 (include_fv_s_orth ?L' ?D) =>
match goal with
| [ _: include_fv_s_orth ?L D |- _ ] => simple apply (@include_fv_s_orth_wrap D L L')
end.

Lemma wf_typ_typing_weaken :
  (forall E' D T, E' |=D T -> forall E F x T',
        E' = E & F -> x # E' -> E |=D T' -> (E & x *~ T' & F) |=D T) /\
  (forall E' D e T, E' |=D e ~: T -> forall E F x T',
        E' = E & F -> x # E' -> E |=D T' -> (E & x *~ T' & F) |=D e ~: T).
Proof.
  apply wf_typ_typing_ind; intros;
    (destruct F using env_ind; rew_env_concat in *; subst;
     [autos* | try solve [false* empty_push_inv]]).
  - apply_fresh wf_typ_arrow. do 2 rewrite <- concat_assoc. apply~ (H0 y). by rew_env_concat.
  - apply_fresh~ wf_typ_forall. do 2 rewrite <- concat_assoc. apply~ (H0 y). by rew_env_concat.
  - apply_fresh wf_typ_refine. do 2 rewrite <- concat_assoc. apply~ (H0 y). by rew_env_concat.
  - destruct_eq_push H4. auto.
  - destruct_eq_push H2. auto.
  - destruct_eq_push H2. auto.
  - eapply typing_app.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
  - apply~ typing_sapp.
    + apply_ih_bind* H0.
    + apply_ih_bind* wf_bits_weaken. apply environment_ok.
      eapply (proj2 wf_typ_typing_regular). apply_ih_bind* H0.
  - apply_fresh typing_fix. do 3 rewrite <- concat_assoc. apply~ (H0 f y). by rew_env_concat.
  - apply typing_succ. apply_ih_bind* H0.
  - apply typing_pred. apply_ih_bind* H0.
  - apply typing_iszero. apply_ih_bind* H0.
  - apply typing_if.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
    + apply_ih_bind* H4.
  - apply_fresh~ typing_sabs. do 2 rewrite <- concat_assoc. apply~ (H0 y). by rew_env_concat.
  - apply typing_choice.
    + apply_ih_bind* H0. apply* (proj1 wf_typ_typing_orth_weaken). apply* extend_orthant_l.
    + apply_ih_bind* H2. apply* (proj1 wf_typ_typing_orth_weaken). apply* extend_orthant_r.
    + apply_ih_bind* H4.
    + apply_ih_bind* wf_bits_weaken. apply environment_ok.
      eapply (proj1 wf_typ_typing_regular). apply_ih_bind* H4.
  - apply~ typing_cast.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
  - destruct_eq_push H4. auto.
  - destruct_eq_push H2. auto.
Qed.

Lemma wf_typ_typing_weaken_s :
  (forall E' D T, E' |=D T ->
             forall E F x, E' = E & F -> x # E' -> (E & 'x & F) |=D T)
  /\ (forall E' D e T, E' |=D e ~: T ->
                 forall E F x, E' = E & F -> x # E' -> (E & 'x & F) |=D e ~: T).
Proof.
  apply wf_typ_typing_ind; intros;
    (destruct F using env_ind; rew_env_concat in *; subst;
     [autos* | try solve [false* empty_push_inv]]).
  - apply_fresh wf_typ_arrow. do 2 rewrite <- concat_assoc. apply~ (H0 y). by rew_env_concat.
  - apply_fresh~ wf_typ_forall. do 2 rewrite <- concat_assoc. apply~ (H0 y). by rew_env_concat.
  - apply_fresh wf_typ_refine. do 2 rewrite <- concat_assoc. apply~ (H0 y). by rew_env_concat.
  - destruct_eq_push H4. auto.
  - destruct_eq_push H2. auto.
  - destruct_eq_push H2. auto.
  - eapply typing_app.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
  - apply~ typing_sapp.
    + apply_ih_bind* H0.
    + apply_ih_bind* wf_bits_weaken. apply environment_ok.
      eapply (proj2 wf_typ_typing_regular). apply_ih_bind* H0.
  - apply_fresh typing_fix. do 3 rewrite <- concat_assoc. apply~ (H0 f y). by rew_env_concat.
  - apply typing_succ. apply_ih_bind* H0.
  - apply typing_pred. apply_ih_bind* H0.
  - apply typing_iszero. apply_ih_bind* H0.
  - apply typing_if.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
    + apply_ih_bind* H4.
  - apply_fresh~ typing_sabs. do 2 rewrite <- concat_assoc. apply~ (H0 y). by rew_env_concat.
  - apply typing_choice.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
    + apply_ih_bind* H4.
    + apply_ih_bind* wf_bits_weaken. apply environment_ok.
      eapply (proj1 wf_typ_typing_regular). apply_ih_bind* H4.
  - apply~ typing_cast.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
  - destruct_eq_push H4. auto.
  - destruct_eq_push H2. auto.
Qed.

Lemma wf_typ_gen_weaken : forall E F D x T' T,
    E |=D T' ->
    (E & F) |=D T ->
    x # (E & F) ->
    (E & x *~ T' & F) |=D T.
Proof. intros. apply* wf_typ_typing_weaken. Qed.

Lemma typing_gen_weaken : forall E F D x T' e T,
    E |=D T' ->
    (E & F) |=D e ~: T ->
    x # (E & F) ->
    (E & x *~ T' & F) |=D e ~: T.
Proof. intros. apply* wf_typ_typing_weaken. Qed.

Lemma wf_typ_gen_sweaken : forall E F D x T,
    (E & F) |=D T ->
    x # (E & F) ->
    (E & 'x & F) |=D T.
Proof. intros. apply* wf_typ_typing_weaken_s. Qed.

Lemma typing_gen_sweaken : forall E F D x e T,
    (E & F) |=D e ~: T ->
    x # (E & F) ->
    (E & 'x & F) |=D e ~: T.
Proof. intros. apply* wf_typ_typing_weaken_s. Qed.
