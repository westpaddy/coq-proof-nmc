Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity.

Lemma wf_bits_closed : forall E x b,
    wf_bits E b ->
    x \notin fv b.
Proof. induction 1; simpl; auto. Qed.

Lemma wf_bits_closed_s : forall E x b,
    wf_bits E b ->
    x # E ->
    x \notin fv_s b.
Proof.
  induction 1; simpl; auto.
  - intro. apply notin_singleton. intro. subst. apply (binds_fresh_inv H H0).
Qed.

Inductive env_closing_term (E : env) (t : trm) :=
| env_closing_term_intro :
    (forall x, x # E -> x \notin fv_a t) ->
    env_closing_term E t.

Lemma env_closing_term_spec : forall x E t,
    env_closing_term E t ->
    x # E ->
    x \notin fv_a t.
Proof. inversion 1; auto. Qed.

Local Hint Resolve env_closing_term_spec.

Inductive closed_env : env -> Prop :=
| closed_env_empty :
    closed_env empty
| closed_env_push : forall E x T,
    closed_env E ->
    env_closing_term E T ->
    closed_env (E & x *~ T)
| close_env_push_s : forall E x,
    closed_env E ->
    closed_env (E & 'x).

Local Hint Constructors closed_env.

Lemma closed_env_push_inv : forall E x T,
    closed_env (E & x *~ T) ->
    closed_env E /\ env_closing_term E T.
Proof.
  inversion 1.
  - false* empty_push_inv.
  - destruct_eq_push H0. auto.
  - falsify_eq_push H0.
Qed.

Lemma closed_env_push_s_inv : forall E x,
    closed_env (E & 'x) ->
    closed_env E.
Proof.
  inversion 1.
  - false* empty_push_inv.
  - falsify_eq_push H0.
  - destruct_eq_push H0. auto.
Qed.

Ltac notin_simpl_in_H :=
  repeat match goal with
         | [ H: _ \notin _ \u _ |- _ ] => destruct (notin_union_r H); clear H
         end.

Lemma notin_fv_a_open : forall x t u,
    x \notin fv_a (t ^^ u) ->
    x \notin fv_a t.
Proof.
  introv. unfold open. move: 0.
  induction t; simpl; intros; notin_simpl_in_H; autos*.
Qed.

Lemma notin_fv_a_open2 : forall x t u1 u2,
    x \notin fv_a (t ^^ u1 ^^ u2) ->
    x \notin fv_a t.
Proof.
  introv. unfold open2. move: 0.
  induction t; simpl; intros; notin_simpl_in_H; autos*.
Qed.

Local Hint Extern 1 (closed_env ?E) =>
match goal with
| [ _: context [ closed_env (E & ?x *~ ?t) ] |- _ ] => apply (@closed_env_push_inv E x t)
| [ _: context [ closed_env (E & '?x) ] |- _ ] => apply (@closed_env_push_s_inv E x)
end.

Local Hint Extern 1 (?x \notin fv_a ?t) =>
match goal with
| [ _: context [ closed_env (?E & ?y *~ ?u) ] |- _ ] => apply (@closed_env_push_inv E y u)
| [ _: context [ _ \notin fv_a (t ^^ trm_fvar ?y) ] |- _ ] => apply (@notin_fv_a_open x y t)
end.

Lemma notin_open_fv_a : forall x u t,
    x \notin fv_a t ->
    x \notin fv_a u ->
    x \notin fv_a (t ^^ u).
Proof. introv. unfold open. move: 0. induction t; simpl; intros; autos*. case_nat~. Qed.

Ltac lc_ih_clear_fresh :=
  match goal with
  | H: forall x, x \notin ?L -> _ |- _ =>
    let y := fresh "y" in
    let Nin := fresh in
    pick_fresh y; assert (Nin: y \notin L) by notin_solve;
    specialize (H y Nin); clear Nin
  | H: forall f x, fresh ?L 2 (x::f::nil) -> _ |- _ =>
    let f := fresh "f" in
    let y := fresh "y" in
    let Nin := fresh in
    pick_fresh f; pick_fresh y; assert (Nin: fresh L 2 (y::f::nil)) by (fresh_solve; exact I);
    specialize (H f y Nin); clear Nin
  end.

Lemma notin_refine : forall x T e,
    x \notin fv_a (trm_refine T e) ->
    x \notin fv_a T /\ x \notin fv_a e.
Proof. simpl; auto. Qed.

Local Hint Extern 1 (?x \notin fv_a ?t) =>
match goal with
| [ H: env_closing_term empty (trm_refine t ?e) |- _ ] => apply (@notin_refine x t e)
| [ H: env_closing_term empty (trm_refine ?T t) |- _ ] => apply (@notin_refine x T t)
end.

Lemma wf_typ_typing_closed :
  (forall E D T, E |=D T -> closed_env E /\ env_closing_term E T)
  /\ (forall E D e T, E |=D e ~: T -> closed_env E /\ env_closing_term E e /\ env_closing_term E T).
Proof.
  apply wf_typ_typing_ind; intros; first [splits 3 | split];
    try (apply env_closing_term_intro; intros);
    repeat lc_ih_clear_fresh; unpack; simpl; notin_simpl; autos*;
      try solve [apply* notin_fv_a_open | apply* notin_fv_a_open2].
  - simpl_dom. notin_solve.
  - apply* notin_open_fv_a.
    assert (K: x \notin fv_a (trm_arrow T1 T2) -> x \notin fv_a T2) by (simpl; auto).
    apply* K.
  - apply fv_a_spec. split.
    + apply* wf_bits_closed.
    + apply* wf_bits_closed_s.
  - apply* notin_open_fv_a.
    + assert (K: x \notin fv_a (trm_forall T) -> x \notin fv_a T) by (simpl; auto). apply* K.
    + apply fv_a_spec. split.
      * apply* wf_bits_closed.
      * apply* wf_bits_closed_s.
  - do 2 apply* closed_env_push_inv.
  - apply fv_a_spec. split.
    + apply* wf_bits_closed.
    + apply* wf_bits_closed_s.
Qed.

Lemma wf_typ_closed : forall x E D T,
    E |=D T ->
    x # E ->
    x \notin fv T.
Proof. intros. apply fv_a_spec. apply* (proj1 wf_typ_typing_closed). Qed.

Lemma wf_typ_closed_s : forall x E D T,
    E |=D T ->
    x # E ->
    x \notin fv_s T.
Proof. intros. apply fv_a_spec. apply* (proj1 wf_typ_typing_closed). Qed.

Lemma typing_closed_exp : forall x E D e T,
    E |=D e ~: T ->
    x # E ->
    x \notin fv e.
Proof. intros. apply fv_a_spec. apply* ((proj2 wf_typ_typing_closed) E D e T). Qed.

Lemma typing_closed_s_exp : forall x E D e T,
    E |=D e ~: T ->
    x # E ->
    x \notin fv_s e.
Proof. intros. apply fv_a_spec. apply* ((proj2 wf_typ_typing_closed) E D e T). Qed.

Lemma typing_closed_typ : forall x E D e T,
    E |=D e ~: T ->
    x # E ->
    x \notin fv T.
Proof. intros. apply fv_a_spec. apply* ((proj2 wf_typ_typing_closed) E D e T). Qed.

Lemma typing_closed_s_typ : forall x E D e T,
    E |=D e ~: T ->
    x # E ->
    x \notin fv_s T.
Proof. intros. apply fv_a_spec. apply* ((proj2 wf_typ_typing_closed) E D e T). Qed.