Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity NMC_Step.

Lemma equiv_typ_spec : forall D T x e1 e2,
    typ T ->
    (forall x, x \notin fv e1 \u fv_s e1) ->
    e1 -->D e2 ->
    [x ~> e1]T ==D [x ~> e2]T.
Proof.
  induction 1; simpl; intros; auto.
  - apply_fresh* equiv_typ_arrow. do 2 rewrite* subst_open_var.
  - apply_fresh* equiv_typ_refine. do 2 rewrite* subst_open_var. apply* csr_exp_step.
  - apply_fresh* equiv_typ_forall. do 2 rewrite* subst_open_var_s.
Qed.

Lemma equiv_typ_refl : forall D T,
    typ T ->
    orthant D ->
    T ==D T.
Proof.
  intros. pick_fresh y.
  rewrite -{1} (@subst_fresh y (trm_iszero trm_zero) T); first by notin_solve.
  rewrite -{2} (@subst_fresh y trm_true T); first by notin_solve.
  apply* equiv_typ_spec. simpl. auto.
Qed.

Lemma equiv_typ_sym : forall D T1 T2,
    T1 ==D T2 ->
    T2 ==D T1.
Proof.
  induction 1; auto.
  - apply_fresh* equiv_typ_refine. apply* csr_exp_sym.
Qed.

Lemma equiv_typ_trans : forall D T1 T2 T3,
    T1 ==D T2 ->
    T2 ==D T3 ->
    T1 ==D T3.
Proof.
  introv Eq. move: T3. induction Eq; intros; auto.
  - inverts* H1.
  - inverts* H0. apply_fresh* equiv_typ_refine. apply* csr_exp_trans.
  - inverts* H1.
Qed.

Lemma csr_exp_orth_weaken : forall D D' e1 e2,
    csr_exp e1 D e2 ->
    orthant D' ->
    D ⊆ D' ->
    csr_exp e1 D' e2.
Proof.
  induction 1; intros.
  - apply* csr_exp_step. apply* step_orth_weaken.
  - apply* csr_exp_sym.
  - apply* csr_exp_trans.
Qed.

Lemma equiv_typ_orth_weaken : forall D D' T1 T2,
    T1 ==D T2 ->
    orthant D' ->
    D ⊆ D' ->
    T1 ==D' T2.
Proof.
  induction 1; intros; auto.
  - apply_fresh* equiv_typ_refine. forwards~ K: (H0 y). apply* csr_exp_orth_weaken.
Qed.

Lemma pull_out_closed_subst : forall y x u x0 e2 e,
    x \notin fv e2 ->                (* e2 is closed *)
    y \notin \{x} \u fv e \u fv u -> (* y is fresh *)
    exp e ->
    exp e2 ->
    exp u ->
    ([x ~> u] ([x0 ~> e2] e)) = [y ~> e2]([x ~> u]([x0 ~> trm_fvar y]e)).
Proof.
  intros.
  set t := [y ~> e2]([x ~> u]([x0 ~> trm_fvar y]e)).
  rewrite* (@subst_as_close_open_exp x0 e2 e 0).
  rewrite* subst_open_gen.
  rewrite* subst_fresh.
  rewrite* (@subst_intro_gen 0 y e2).
  rewrite* <- (@subst_fresh x u (trm_fvar y)).
  rewrite* <- subst_open_gen.
  rewrite* <- subst_as_close_open_exp.
  simpl. notin_solve.
  apply fv_subst. notin_simpl. apply fv_close_var_rec.
  auto. auto.
Qed.

Lemma csr_exp_subst : forall D e1 e2 x u,
    csr_exp e1 D e2 ->
    exp u ->
    csr_exp ([x ~> u]e1) D ([x ~> u]e2).
Proof.
  induction 1; intros.
  - pick_fresh y.
    assert (x \notin fv e1).
    { specialize (H1 x). notin_solve. }
    assert (x \notin fv e2).
    { apply* step_keep_fv. }
    rewrite* (@pull_out_closed_subst y).
    rewrite* (@pull_out_closed_subst y x u x0 e2).
    apply* csr_exp_step.
  - apply* csr_exp_sym.
  - apply* csr_exp_trans.
Qed.

Lemma equiv_subst : forall D T1 T2 x u,
    T1 ==D T2 ->
    exp u ->
    [x ~> u]T1 ==D [x ~> u]T2.
Proof.
  induction 1; simpl; intros; auto.
  - apply_fresh* equiv_typ_arrow. do 2 rewrite* subst_open_var.
  - apply_fresh* equiv_typ_refine. do 2 rewrite* subst_open_var. apply* csr_exp_subst.
  - apply_fresh* equiv_typ_forall. do 2 rewrite* subst_open_var_s.
Qed.

Lemma pull_out_closed_subst_s : forall y x u x0 e2 e,
    x \notin fv_s e2 ->
    y \notin \{x} \u fv e \u fv u ->
    exp e ->
    exp e2 ->
    bits u ->
    (subst_s x u ([x0 ~> e2]e)) = [y ~> e2](subst_s x u ([x0 ~> trm_fvar y]e)).
Proof.
  intros.
  set t := [y ~> e2](subst_s x u ([x0 ~> trm_fvar y]e)).
  rewrite* (@subst_as_close_open_exp x0 e2 e 0).
  rewrite* subst_s_open_gen.
  rewrite* subst_s_fresh.
  rewrite* (@subst_intro_gen 0 y e2).
  rewrite* <- (@subst_s_fresh x u (trm_fvar y)).
  rewrite* <- subst_s_open_gen.
  rewrite* <- subst_as_close_open_exp.
  simpl. notin_solve.
  apply fv_subst_s. notin_simpl. apply fv_close_var_rec.
  auto. auto.
Qed.

Lemma csr_exp_subst_s : forall D e1 e2 x u,
    csr_exp e1 D e2 ->
    bits u ->
    csr_exp (subst_s x u e1) D (subst_s x u e2).
Proof.
  induction 1; intros.
  - pick_fresh y.
    assert (x \notin fv_s e1).
    { specialize (H1 x); notin_solve. }
    assert (x \notin fv_s e2).
    { apply* step_keep_fv_s. }
    rewrite* (@pull_out_closed_subst_s y).
    rewrite* (@pull_out_closed_subst_s y x u x0 e2).
    apply* csr_exp_step.
  - apply* csr_exp_sym.
  - apply* csr_exp_trans.
Qed.

Lemma equiv_subst_s : forall D T1 T2 x u,
    T1 ==D T2 ->
    bits u ->
    subst_s x u T1 ==D subst_s x u T2.
Proof.
  induction 1; simpl; intros; auto.
  - apply_fresh* equiv_typ_arrow. do 2 rewrite* subst_s_open_var.
  - apply_fresh* equiv_typ_refine. do 2 rewrite* subst_s_open_var. apply* csr_exp_subst_s.
  - apply_fresh* equiv_typ_forall. do 2 rewrite* subst_s_open_var_s.
Qed.

Lemma csr_exp_orthant_subst_s : forall D e1 e2 x b,
    csr_exp e1 D e2 ->
    bits b ->
    csr_exp e1 (Im D (subst_s_axis x b)) e2.
Proof.
  induction 1; intros.
  - apply* csr_exp_step. apply* step_orthant_subst_s. specialize (H1 x). auto.
  - apply* csr_exp_sym.
  - apply* csr_exp_trans.
Qed.

Lemma equiv_typ_orthant_subst_s : forall D T1 T2 x b,
    T1 ==D T2 ->
    bits b ->
    T1 ==(Im D (subst_s_axis x b)) T2.
Proof.
  induction 1; intros; auto.
  - apply_fresh* equiv_typ_refine. forwards~ K: (H0 y). apply* csr_exp_orthant_subst_s.
Qed.

(** * Properties for consistency between types  *)

Lemma consist_typ_refine_inv : forall T1 T2 e,
    trm_refine T1 e ~= T2 ->
    T1 ~= T2.
Proof.
  introv C. gen_eq T1': (trm_refine T1 e). move: T1 e.
  induction C; introv Eq; inverts Eq; auto.
  - forwards~ : IHC.
Qed.

Lemma consist_typ_sym : forall T1 T2,
    T1 ~= T2 ->
    T2 ~= T1.
Proof. induction 1; auto. Qed.

Lemma consist_typ_subst : forall x u1 u2 T1 T2,
    T1 ~= T2 ->
    exp u1 ->
    exp u2 ->
    [x ~> u1]T1 ~= [x ~> u2]T2.
Proof.
  induction 1; simpl; intros; auto.
  - apply_fresh* consist_typ_arrow. do 2 rewrite~ subst_open_var.
  - apply* consist_typ_refine_l.
    replace (trm_refine ([x ~> u1]T1) ([x ~> u1]e)) with ([x ~> u1](trm_refine T1 e)); last by simpl.
    auto.
  - apply* consist_typ_refine_r.
    replace (trm_refine ([x ~> u2]T2) ([x ~> u2]e)) with ([x ~> u2](trm_refine T2 e)); last by simpl.
    auto.
  - apply_fresh* consist_typ_forall. do 2 rewrite~ subst_open_var_s.
Qed.

Lemma consist_typ_subst_s : forall x u1 u2 T1 T2,
    T1 ~= T2 ->
    bits u1 ->
    bits u2 ->
    subst_s x u1 T1 ~= subst_s x u2 T2.
Proof.
  induction 1; simpl; intros; auto.
  - apply_fresh* consist_typ_arrow. do 2 rewrite~ subst_s_open_var.
  - apply* consist_typ_refine_l.
    replace (trm_refine (subst_s x u1 T1) (subst_s x u1 e)) with (subst_s x u1 (trm_refine T1 e)); last by simpl.
    auto.
  - apply* consist_typ_refine_r.
    replace (trm_refine (subst_s x u2 T2) (subst_s x u2 e)) with (subst_s x u2 (trm_refine T2 e)); last by simpl.
    auto.
  - apply_fresh* consist_typ_forall. do 2 rewrite~ subst_s_open_var_s.
Qed.
