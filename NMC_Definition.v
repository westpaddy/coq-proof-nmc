Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Export Image.
From TLC Require Export LibLN.
Open Scope list_scope.

(** ** raw terms *)
(** Note that both term and string bound variables are represented by
    [trm_bvar], which are distinguished how those are bound. *)
Inductive trm : Set :=
| trm_bvar : nat -> trm
(* terms for bit strings *)
| trm_fvar_s : var -> trm
| trm_on : trm
| trm_off : trm
| trm_concat : trm -> trm -> trm
(* terms for types *)
| trm_nat : trm
| trm_bool : trm
| trm_arrow : trm -> trm -> trm
| trm_refine : trm -> trm -> trm
| trm_forall : trm -> trm
(* terms for expressions *)
| trm_fvar : var -> trm
| trm_app : trm -> trm -> trm
| trm_fix : trm -> trm -> trm
| trm_zero : trm
| trm_succ : trm -> trm
| trm_pred : trm -> trm
| trm_iszero : trm -> trm
| trm_true : trm
| trm_false : trm
| trm_if : trm -> trm -> trm -> trm
| trm_sapp : trm -> trm -> trm
| trm_sabs : trm -> trm
| trm_choice : trm -> trm -> trm -> trm
| trm_cast : trm -> trm -> trm -> trm
| trm_waiting : trm -> trm -> trm
| trm_active : trm -> trm -> trm -> trm
| trm_blame : trm.

Definition axis := (trm + trm)%type.
Definition orth := Ensemble axis.
Notation "∅" := (Empty_set axis).
Notation "D ⊌ x" := (Add axis D x) (at level 52, left associativity).
Notation "D1 ∪ D2" := (Union axis D1 D2) (at level 53, right associativity).
Notation "D1 ⊆ D2" := (Included axis D1 D2) (at level 54).
Notation "x ∈ D" := (In axis D x) (at level 55).
Notation "x ∉ D" := (~ (x ∈ D)) (at level 55).

(** ** type environment *)
(** Environment binds a term variable to a type [x *~ T] and records
    when a string variable is introduced ['x]. *)
Definition env := LibEnv.env (option trm).
Notation "x *~ t" := (single x (Some t)) (at level 27) : env_scope.
Notation "' x" := (single x None) (at level 9) : env_scope.

(** * Meta-level operations for raw terms *)

(** ** opening operation  *)
(** We assume [u] is locally-closed, and then there is no need to
    shifting indices, cf. [trm_fix] case. *)
Reserved Notation "{ k ~> u } t" (at level 67, t at level 9).
Fixpoint open_rec (k : nat) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => If i = k then u else trm_bvar i
  | trm_fvar_s x => trm_fvar_s x
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_concat t1 t2 => trm_concat ({k ~> u}t1) ({k ~> u}t2)
  | trm_nat => trm_nat
  | trm_bool => trm_bool
  | trm_arrow t1 t2 => trm_arrow ({k ~> u}t1) ({S k ~> u}t2)
  | trm_refine t1 t2 => trm_refine ({k ~> u}t1) ({S k ~> u}t2)
  | trm_forall t1 => trm_forall ({S k ~> u}t1)
  | trm_fvar x => trm_fvar x
  | trm_app t1 t2 => trm_app ({k ~> u}t1) ({k ~> u}t2)
  | trm_fix t1 t2 => trm_fix ({k ~> u}t1) ({S (S k) ~> u}t2)
  | trm_zero => trm_zero
  | trm_succ t1 => trm_succ ({k ~> u}t1)
  | trm_pred t1 => trm_pred ({k ~> u}t1)
  | trm_iszero t1 => trm_iszero ({k ~> u}t1)
  | trm_true => trm_true
  | trm_false => trm_false
  | trm_if t1 t2 t3 => trm_if ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_sapp t1 t2 => trm_sapp ({k ~> u}t1) ({k ~> u}t2)
  | trm_sabs t1 => trm_sabs ({S k ~> u}t1)
  | trm_choice t1 t2 t3 => trm_choice ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_cast t1 t2 t3 => trm_cast ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_waiting t1 t2 => trm_waiting ({k ~> u}t1) ({k ~> u}t2)
  | trm_active t1 t2 t3 => trm_active ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_blame => trm_blame
  end
where "{ k ~> u } t" := (open_rec k u t).

Definition open t u := {0 ~> u}t.
Definition open2 t u1 u2 := {1 ~> u2}({0 ~> u1}t).
Notation "t ^^ u" := (open t u) (at level 67).
Notation "t ^^ u1 ^^ u2" := (open2 t u1 u2) (at level 67, u1 at next level).
Notation "t $^ x" := (open t (trm_fvar x)) (at level 67, x global).
Notation "t $^ x $^ y" := (open2 t (trm_fvar x) (trm_fvar y)) (at level 67, x global, y global).
Notation "t %^ x" := (open t (trm_fvar_s x)) (at level 67, x global).

(** ** closing operation *)
(** This is only for a term variable. No operation for string
    variables is required in this formalization. *)
Fixpoint close_var_rec (k : nat) (z : var) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => trm_bvar i
  | trm_fvar_s x => trm_fvar_s x
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_concat t1 t2 => trm_concat (close_var_rec k z t1) (close_var_rec k z t2)
  | trm_nat => trm_nat
  | trm_bool => trm_bool
  | trm_arrow t1 t2 => trm_arrow (close_var_rec k z t1) (close_var_rec (S k) z t2)
  | trm_refine t1 t2 => trm_refine (close_var_rec k z t1) (close_var_rec (S k) z t2)
  | trm_forall t1 => trm_forall (close_var_rec (S k) z t1)
  | trm_fvar x => If x = z then trm_bvar k else trm_fvar x
  | trm_app t1 t2 => trm_app (close_var_rec k z t1) (close_var_rec k z t2)
  | trm_fix t1 t2 => trm_fix (close_var_rec k z t1) (close_var_rec (S (S k)) z t2)
  | trm_zero => trm_zero
  | trm_succ t1 => trm_succ (close_var_rec k z t1)
  | trm_pred t1 => trm_pred (close_var_rec k z t1)
  | trm_iszero t1 => trm_iszero (close_var_rec k z t1)
  | trm_true => trm_true
  | trm_false => trm_false
  | trm_if t1 t2 t3 => trm_if (close_var_rec k z t1) (close_var_rec k z t2) (close_var_rec k z t3)
  | trm_sapp t1 t2 => trm_sapp (close_var_rec k z t1) (close_var_rec k z t2)
  | trm_sabs t1 => trm_sabs (close_var_rec (S k) z t1)
  | trm_choice t1 t2 t3 => trm_choice (close_var_rec k z t1) (close_var_rec k z t2) (close_var_rec k z t3)
  | trm_cast t1 t2 t3 => trm_cast (close_var_rec k z t1) (close_var_rec k z t2) (close_var_rec k z t3)
  | trm_waiting t1 t2 => trm_waiting (close_var_rec k z t1) (close_var_rec k z t2)
  | trm_active t1 t2 t3 => trm_active (close_var_rec k z t1) (close_var_rec k z t2) (close_var_rec k z t3)
  | trm_blame => trm_blame
  end.

Definition close_var z t := close_var_rec 0 z t.

(** ** collecting free variables *)
Fixpoint fv (t : trm) {struct t} : vars :=
  match t with
  | trm_fvar_s x => \{}
  | trm_on => \{}
  | trm_off => \{}
  | trm_concat t1 t2 => (fv t1) \u (fv t2)
  | trm_nat => \{}
  | trm_bool => \{}
  | trm_arrow t1 t2 => (fv t1) \u (fv t2)
  | trm_refine t1 t2 => (fv t1) \u (fv t2)
  | trm_forall t1 => (fv t1)
  | trm_bvar i => \{}
  | trm_fvar x => \{x}
  | trm_app t1 t2 => (fv t1) \u (fv t2)
  | trm_fix t1 t2 => (fv t1) \u (fv t2)
  | trm_zero => \{}
  | trm_succ t1 => (fv t1)
  | trm_pred t1 => (fv t1)
  | trm_iszero t1 => (fv t1)
  | trm_true => \{}
  | trm_false => \{}
  | trm_if t1 t2 t3 => (fv t1) \u (fv t2) \u (fv t3)
  | trm_sapp t1 t2 => (fv t1) \u (fv t2)
  | trm_sabs t1 => (fv t1)
  | trm_choice t1 t2 t3 => (fv t1) \u (fv t2) \u (fv t3)
  | trm_cast t1 t2 t3 => (fv t1) \u (fv t2) \u (fv t3)
  | trm_waiting t1 t2 => (fv t1) \u (fv t2)
  | trm_active t1 t2 t3 => (fv t1) \u (fv t2) \u (fv t3)
  | trm_blame => \{}
  end.

(** ** collecting free string variables *)
Fixpoint fv_s (t : trm) {struct t} : vars :=
  match t with
  | trm_fvar_s x => \{x}
  | trm_on => \{}
  | trm_off => \{}
  | trm_concat t1 t2 => (fv_s t1) \u (fv_s t2)
  | trm_nat => \{}
  | trm_bool => \{}
  | trm_arrow t1 t2 => (fv_s t1) \u (fv_s t2)
  | trm_refine t1 t2 => (fv_s t1) \u (fv_s t2)
  | trm_forall t1 => (fv_s t1)
  | trm_bvar i => \{}
  | trm_fvar x => \{}
  | trm_app t1 t2 => (fv_s t1) \u (fv_s t2)
  | trm_fix t1 t2 => (fv_s t1) \u (fv_s t2)
  | trm_zero => \{}
  | trm_succ t1 => (fv_s t1)
  | trm_pred t1 => (fv_s t1)
  | trm_iszero t1 => (fv_s t1)
  | trm_true => \{}
  | trm_false => \{}
  | trm_if t1 t2 t3 => (fv_s t1) \u (fv_s t2) \u (fv_s t3)
  | trm_sapp t1 t2 => (fv_s t1) \u (fv_s t2)
  | trm_sabs t1 => (fv_s t1)
  | trm_choice t1 t2 t3 => (fv_s t1) \u (fv_s t2) \u (fv_s t3)
  | trm_cast t1 t2 t3 => (fv_s t1) \u (fv_s t2) \u (fv_s t3)
  | trm_waiting t1 t2 => (fv_s t1) \u (fv_s t2)
  | trm_active t1 t2 t3 => (fv_s t1) \u (fv_s t2) \u (fv_s t3)
  | trm_blame => \{}
  end.

(** * Syntax is defined by means of locally closed terms *)

(** ** bit strings *)
Inductive bits : trm -> Prop :=
| bits_var : forall x,
    bits (trm_fvar_s x)
| bits_on :
    bits trm_on
| bits_off :
    bits trm_off
| bits_concat : forall b1 b2,
    bits b1 ->
    bits b2 ->
    bits (trm_concat b1 b2).

(** ** numeral values *)
Inductive nvalue : trm -> Prop :=
| nvalue_zero :
    nvalue trm_zero
| nvalue_succ : forall nv,
    nvalue nv ->
    nvalue (trm_succ nv).

(** ** types, expressions, and values *)
Inductive typ : trm -> Prop :=
| typ_nat :
    typ trm_nat
| typ_bool :
    typ trm_bool
| typ_arrow : forall L T1 T2,
    typ T1 ->
    (forall x, x \notin L -> typ (T2 $^ x)) ->
    typ (trm_arrow T1 T2)
| typ_refine : forall L T e,
    typ T ->
    (forall x, x \notin L -> exp (e $^ x)) ->
    typ (trm_refine T e)
| typ_forall : forall L T,
    (forall x, x \notin L -> typ (T %^ x)) ->
    typ (trm_forall T)
with exp : trm -> Prop :=
| exp_var : forall x,
    exp (trm_fvar x)
| exp_app : forall e1 e2,
    exp e1 ->
    exp e2 ->
    exp (trm_app e1 e2)
| exp_fix : forall L T1 T2 e,
    typ (trm_arrow T1 T2) ->
    (forall f x, fresh L 2 (x::f::nil) -> exp (e $^ x $^ f)) ->
    exp (trm_fix (trm_arrow T1 T2) e)
| exp_zero :
    exp trm_zero
| exp_succ : forall e,
    exp e ->
    exp (trm_succ e)
| exp_pred : forall e,
    exp e ->
    exp (trm_pred e)
| exp_iszero : forall e,
    exp e ->
    exp (trm_iszero e)
| exp_true :
    exp trm_true
| exp_false :
    exp trm_false
| exp_if : forall e1 e2 e3,
    exp e1 ->
    exp e2 ->
    exp e3 ->
    exp (trm_if e1 e2 e3)
| exp_sapp : forall e b,
    exp e ->
    bits b ->
    exp (trm_sapp e b)
| exp_sabs : forall L e,
    (forall x, x \notin L -> exp (e %^ x)) ->
    exp (trm_sabs e)
| exp_choice : forall e1 e2 b,
    exp e1 ->
    exp e2 ->
    bits b ->
    exp (trm_choice e1 e2 b)
| exp_cast : forall T1 T2 e,
    exp e ->
    typ T1 ->
    typ T2 ->
    exp (trm_cast e T1 T2)
| exp_waiting : forall T e1 e2,
    exp e1 ->
    typ (trm_refine T e2) ->
    exp (trm_waiting e1 (trm_refine T e2))
| exp_active : forall T e1 e2 v,
    exp e1 ->
    value v ->
    typ (trm_refine T e2) ->
    exp (trm_active e1 v (trm_refine T e2))
| exp_blame :
    exp trm_blame
with value : trm -> Prop :=
| value_nvalue : forall nv,
    nvalue nv ->
    value nv
| value_true :
    value trm_true
| value_false :
    value trm_false
| value_fix : forall T1 T2 e,
    exp (trm_fix (trm_arrow T1 T2) e) ->
    value (trm_fix (trm_arrow T1 T2) e)
| value_sabs : forall e,
    exp (trm_sabs e) ->
    value (trm_sabs e).

(** ** orthant *)
Inductive orthant : orth -> Prop :=
| orthant_intro : forall D,
    (forall b, ((inl b) ∈ D -> bits b)) ->
    (forall b, ((inr b) ∈ D -> bits b)) ->
    orthant D.

(** ** result *)
Inductive result : orth -> trm -> Prop :=
| result_value : forall D v,
    orthant D ->
    value v ->
    result D v
| result_blame : forall D,
    orthant D ->
    result D trm_blame
| result_choice : forall D R1 R2 b,
    inl b ∉ D ->
    inr b ∉ D ->
    result (D ⊌ inl b) R1 ->
    result (D ⊌ inr b) R2 ->
    result D (trm_choice R1 R2 b).

(** ** type environment *)
Inductive environment : env -> Prop :=
| environment_empty :
    environment empty
| environment_push : forall E x T,
    environment E ->
    x # E ->
    typ T ->
    environment (E & x *~ T)
| environment_push_s : forall E x,
    environment E ->
    x # E ->
    environment (E & 'x).

(** * Meta-level operations for locally-closed terms *)

(** ** substitution for a free-variable *)
(** We assume [u] is locally-closed, especially as an expression.  As
    a result, substitution becomes just a map function. *)
Reserved Notation "[ x ~> u ] t" (at level 66, t at level 9).
Fixpoint subst (z : var) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_fvar_s x => trm_fvar_s x
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_concat t1 t2 => trm_concat ([z ~> u]t1) ([z ~> u]t2)
  | trm_nat => trm_nat
  | trm_bool => trm_bool
  | trm_arrow t1 t2 => trm_arrow ([z ~> u]t1) ([z ~> u]t2)
  | trm_refine t1 t2 => trm_refine ([z ~> u]t1) ([z ~> u]t2)
  | trm_forall t1 => trm_forall ([z ~> u]t1)
  | trm_bvar i => trm_bvar i
  | trm_fvar x => If x = z then u else trm_fvar x
  | trm_zero => trm_zero
  | trm_succ t1 => trm_succ ([z ~> u]t1)
  | trm_pred t1 => trm_pred ([z ~> u]t1)
  | trm_iszero t1 => trm_iszero ([z ~> u]t1)
  | trm_true => trm_true
  | trm_false => trm_false
  | trm_if t1 t2 t3 => trm_if ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_app t1 t2 => trm_app ([z ~> u]t1) ([z ~> u]t2)
  | trm_fix t1 t2 => trm_fix ([z ~> u]t1) ([z ~> u]t2)
  | trm_sapp t1 t2 => trm_sapp ([z ~> u]t1) ([z ~> u]t2)
  | trm_sabs t1 => trm_sabs ([z ~> u]t1)
  | trm_choice t1 t2 t3 => trm_choice ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_cast t1 t2 t3 => trm_cast ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_waiting t1 t2 => trm_waiting ([z ~> u]t1) ([z ~> u]t2)
  | trm_active t1 t2 t3 => trm_active ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_blame => trm_blame
  end
where "[ x ~> u ] t" := (subst x u t).

(** ** substitution for a free-string-variable *)
(** As [subst], we assume [u] is locally-closed, especially as a
    bit-string.  *)
Fixpoint subst_s (z : var) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_fvar_s x => If x = z then u else trm_fvar_s x
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_concat t1 t2 => trm_concat (subst_s z u t1) (subst_s z u t2)
  | trm_nat => trm_nat
  | trm_bool => trm_bool
  | trm_arrow t1 t2 => trm_arrow (subst_s z u t1) (subst_s z u t2)
  | trm_refine t1 t2 => trm_refine (subst_s z u t1) (subst_s z u t2)
  | trm_forall t1 => trm_forall (subst_s z u t1)
  | trm_bvar i => trm_bvar i
  | trm_fvar x => trm_fvar x
  | trm_zero => trm_zero
  | trm_succ t1 => trm_succ (subst_s z u t1)
  | trm_pred t1 => trm_pred (subst_s z u t1)
  | trm_iszero t1 => trm_iszero (subst_s z u t1)
  | trm_true => trm_true
  | trm_false => trm_false
  | trm_if t1 t2 t3 => trm_if (subst_s z u t1) (subst_s z u t2) (subst_s z u t3)
  | trm_app t1 t2 => trm_app (subst_s z u t1) (subst_s z u t2)
  | trm_fix t1 t2 => trm_fix (subst_s z u t1) (subst_s z u t2)
  | trm_sapp t1 t2 => trm_sapp (subst_s z u t1) (subst_s z u t2)
  | trm_sabs t1 => trm_sabs (subst_s z u t1)
  | trm_choice t1 t2 t3 => trm_choice (subst_s z u t1) (subst_s z u t2) (subst_s z u t3)
  | trm_cast t1 t2 t3 => trm_cast (subst_s z u t1) (subst_s z u t2) (subst_s z u t3)
  | trm_waiting t1 t2 => trm_waiting (subst_s z u t1) (subst_s z u t2)
  | trm_active t1 t2 t3 => trm_active (subst_s z u t1) (subst_s z u t2) (subst_s z u t3)
  | trm_blame => trm_blame
  end.

(** * Semantics and type system *)

(** ** single-step reduction *)
Reserved Notation "e --> D e'" (at level 68, D at level 9).
Inductive step : trm -> orth -> trm -> Prop :=
(* reductions with substitution *)
| step_beta : forall D T1 T2 e v,
    orthant D ->
    exp (trm_fix (trm_arrow T1 T2) e) ->
    value v ->
    (trm_app (trm_fix (trm_arrow T1 T2) e) v)
      -->D (e ^^ v ^^ (trm_fix (trm_arrow T1 T2) e))
| step_sigma : forall D e b,
    orthant D ->
    exp (trm_sabs e) ->
    bits b ->
    (trm_sapp (trm_sabs e) b) -->D (e ^^ b)
| step_check : forall D T e v,
    orthant D ->
    value v ->
    typ (trm_refine T e) ->
    (trm_waiting v (trm_refine T e))
      -->D (trm_active (e ^^ v) v (trm_refine T e))
(* reductions without substitution *)
| step_pred : forall D nv,
    orthant D ->
    nvalue nv ->
    (trm_pred (trm_succ nv)) -->D nv
| step_iszero_t : forall D,
    orthant D ->
    (trm_iszero trm_zero) -->D trm_true
| step_iszero_f : forall D nv,
    orthant D ->
    nvalue nv ->
    (trm_iszero (trm_succ nv)) -->D trm_false
| step_if_t : forall D e1 e2,
    orthant D ->
    exp e1 ->
    exp e2 ->
    (trm_if trm_true e1 e2) -->D e1
| step_if_f : forall D e1 e2,
    orthant D ->
    exp e1 ->
    exp e2 ->
    (trm_if trm_false e1 e2) -->D e2
| step_choice_l : forall D e1 e2 b,
    orthant D ->
    exp e1 ->
    exp e2 ->
    (inl b) ∈ D ->
    (trm_choice e1 e2 b) -->D e1
| step_choice_r : forall D e1 e2 b,
    orthant D ->
    exp e1 ->
    exp e2 ->
    (inr b) ∈ D ->
    (trm_choice e1 e2 b) -->D e2
| step_active_s : forall D T e v,
    orthant D ->
    typ (trm_refine T e) ->
    value v ->
    (trm_active trm_true v (trm_refine T e)) -->D v
| step_active_f : forall D T e v,
    orthant D ->
    typ (trm_refine T e) ->
    value v ->
    (trm_active trm_false v (trm_refine T e)) -->D trm_blame
(* reductions for cast *)
| step_c_nat : forall D v,
    orthant D ->
    value v ->
    (trm_cast v trm_nat trm_nat) -->D v
| step_c_bool : forall D v,
    orthant D ->
    value v ->
    (trm_cast v trm_bool trm_bool) -->D v
| step_c_arrow : forall D T11 T12 T21 T22 v x,
    orthant D ->
    typ (trm_arrow T11 T12) ->
    typ (trm_arrow T21 T22) ->
    value v ->
    x \notin (fv v) \u (fv T11) \u (fv T12) \u (fv T21) \u (fv T22) ->
    (trm_cast v (trm_arrow T11 T12) (trm_arrow T21 T22))
      -->D (trm_fix (trm_arrow T21 T22)
                  (trm_app (close_var x (trm_fix (trm_arrow T11 (T22 $^ x))
                                                 (trm_cast (trm_app v (trm_bvar 0)) T12 (T22 $^ x))))
                           (trm_cast (trm_bvar 0) T21 T11)))
| step_c_forall : forall D T1 T2 v,
    orthant D ->
    typ (trm_forall T1) ->
    typ (trm_forall T2) ->
    value v ->
    (trm_cast v (trm_forall T1) (trm_forall T2)) -->D (trm_sabs (trm_cast (trm_sapp v (trm_bvar 0)) T1 T2))
| step_c_forget : forall D T1 T2 e v,
    orthant D ->
    typ (trm_refine T1 e) ->
    typ T2 ->
    value v ->
    (trm_cast v (trm_refine T1 e) T2) -->D (trm_cast v T1 T2)
| step_c_precheck : forall D T1 T2 e v,
    orthant D ->
    typ T1 ->
    typ (trm_refine T2 e) ->
    value v ->
    (forall T' e', T1 <> (trm_refine T' e')) ->
    (trm_cast v T1 (trm_refine T2 e)) -->D (trm_waiting (trm_cast v T1 T2) (trm_refine T2 e))
(* reductions for branching *)
| step_dist_app_l : forall D e11 e12 e2 b,
    orthant D ->
    exp e11 ->
    exp e12 ->
    exp e2 ->
    bits b ->
    (trm_app (trm_choice e11 e12 b) e2) -->D (trm_choice (trm_app e11 e2) (trm_app e12 e2) b)
| step_dist_app_r : forall D e1 e2 v b,
    orthant D ->
    exp e1 ->
    exp e2 ->
    value v ->
    bits b ->
    (trm_app v (trm_choice e1 e2 b)) -->D (trm_choice (trm_app v e1) (trm_app v e2) b)
| step_dist_succ : forall D e1 e2 b,
    orthant D ->
    exp e1 ->
    exp e2 ->
    bits b ->
    (trm_succ (trm_choice e1 e2 b)) -->D (trm_choice (trm_succ e1) (trm_succ e2) b)
| step_dist_pred : forall D e1 e2 b,
    orthant D ->
    exp e1 ->
    exp e2 ->
    bits b ->
    (trm_pred (trm_choice e1 e2 b)) -->D (trm_choice (trm_pred e1) (trm_pred e2) b)
| step_dist_iszero : forall D e1 e2 b,
    orthant D ->
    exp e1 ->
    exp e2 ->
    bits b ->
    (trm_iszero (trm_choice e1 e2 b)) -->D (trm_choice (trm_iszero e1) (trm_iszero e2) b)
| step_dist_if : forall D e11 e12 e2 e3 b,
    orthant D ->
    exp e11 ->
    exp e12 ->
    exp e2 ->
    exp e3 ->
    bits b ->
    (trm_if (trm_choice e11 e12 b) e2 e3) -->D (trm_choice (trm_if e11 e2 e3) (trm_if e12 e2 e3) b)
| step_dist_sapp : forall D e1 e2 b1 b2,
    orthant D ->
    exp e1 ->
    exp e2 ->
    bits b1 ->
    bits b2 ->
    (trm_sapp (trm_choice e1 e2 b1) b2) -->D (trm_choice (trm_sapp e1 b2) (trm_sapp e2 b2) b1)
| step_dist_cast : forall D T1 T2 e1 e2 b,
    orthant D ->
    typ T1 ->
    typ T2 ->
    exp e1 ->
    exp e2 ->
    bits b ->
    (trm_cast (trm_choice e1 e2 b) T1 T2) -->D (trm_choice (trm_cast e1 T1 T2) (trm_cast e2 T1 T2) b)
| step_dist_waiting : forall D T e1 e2 e3 b,
    orthant D ->
    exp e1 ->
    exp e2 ->
    bits b ->
    typ (trm_refine T e3) ->
    (trm_waiting (trm_choice e1 e2 b) (trm_refine T e3))
      -->D (trm_choice (trm_waiting e1 (trm_refine T e3)) (trm_waiting e2 (trm_refine T e3)) b)
| step_dist_active : forall D T e1 e2 e3 v b,
    orthant D ->
    exp e1 ->
    exp e2 ->
    bits b ->
    value v ->
    typ (trm_refine T e3) ->
    (trm_active (trm_choice e1 e2 b) v (trm_refine T e3))
      -->D (trm_choice (trm_active e1 v (trm_refine T e3)) (trm_active e2 v (trm_refine T e3)) b)
(* reductions under context *)
| step_ctx_app_l : forall D e1 e1' e2,
    e1 -->D e1' ->
    exp e2 ->
    (trm_app e1 e2) -->D (trm_app e1' e2)
| step_ctx_app_r : forall D e e' v,
    value v ->
    e -->D e' ->
    (trm_app v e) -->D (trm_app v e')
| step_ctx_succ : forall D e1 e1',
    e1 -->D e1' ->
    (trm_succ e1) -->D (trm_succ e1')
| step_ctx_pred : forall D e1 e1',
    e1 -->D e1' ->
    (trm_pred e1) -->D (trm_pred e1')
| step_ctx_iszero : forall D t1 t1',
    t1 -->D t1' ->
    (trm_iszero t1) -->D (trm_iszero t1')
| step_ctx_if : forall D e1 e1' e2 e3,
    e1 -->D e1' ->
    exp e2 ->
    exp e3 ->
    (trm_if e1 e2 e3) -->D (trm_if e1' e2 e3)
| step_ctx_sapp : forall D e e' b,
    e -->D e' ->
    bits b ->
    (trm_sapp e b) -->D (trm_sapp e' b)
| step_ctx_choice_l : forall D e1 e1' e2 b,
    e1 -->(D ⊌ (inl b)) e1' ->
    exp e2 ->
    (trm_choice e1 e2 b) -->D (trm_choice e1' e2 b)
| step_ctx_choice_r : forall D e1 e2 e2' b,
    exp e1 ->
    e2 -->(D ⊌ (inr b)) e2' ->
    (trm_choice e1 e2 b) -->D (trm_choice e1 e2' b)
| step_ctx_cast : forall D T1 T2 e e',
    e -->D e' ->
    typ T1 ->
    typ T2 ->
    (trm_cast e T1 T2) -->D (trm_cast e' T1 T2)
| step_ctx_waiting : forall D T e1 e1' e2,
    e1 -->D e1' ->
    typ (trm_refine T e2) ->
    (trm_waiting e1 (trm_refine T e2)) -->D (trm_waiting e1' (trm_refine T e2))
| step_ctx_active : forall D T e1 e1' e2 v,
    e1 -->D e1' ->
    value v ->
    typ (trm_refine T e2) ->
    (trm_active e1 v (trm_refine T e2)) -->D (trm_active e1' v (trm_refine T e2))
(* reductions for context blaming *)
| step_blame_app_l : forall D e,
    orthant D ->
    exp e ->
    (trm_app trm_blame e) -->D trm_blame
| step_blame_app_r : forall D v,
    orthant D ->
    value v ->
    (trm_app v trm_blame) -->D trm_blame
| step_blame_succ : forall D,
    orthant D ->
    (trm_succ trm_blame) -->D trm_blame
| step_blame_pred : forall D,
    orthant D ->
    (trm_pred trm_blame) -->D trm_blame
| step_blame_iszero : forall D,
    orthant D ->
    (trm_iszero trm_blame) -->D trm_blame
| step_blame_if : forall D e1 e2,
    orthant D ->
    exp e1 ->
    exp e2 ->
    (trm_if trm_blame e1 e2) -->D trm_blame
| step_blame_sapp : forall D b,
    orthant D ->
    bits b ->
    (trm_sapp trm_blame b) -->D trm_blame
| step_blame_cast : forall D T1 T2,
    orthant D ->
    typ T1 ->
    typ T2 ->
    (trm_cast trm_blame T1 T2) -->D trm_blame
| step_blame_waiting : forall D T e,
    orthant D ->
    typ (trm_refine T e) ->
    (trm_waiting trm_blame (trm_refine T e)) -->D trm_blame
| step_blame_active : forall D T e v,
    orthant D ->
    value v ->
    typ (trm_refine T e) ->
    (trm_active trm_blame v (trm_refine T e)) -->D trm_blame
where "e --> D e'" := (step e D e').

(** ** multi-step reduction *)
Reserved Notation "e '-->*' D e'" (at level 68, D at level 9).
Inductive mstep : trm -> orth -> trm -> Prop :=
| mred_refl : forall D e,
    orthant D ->
    exp e ->
    e -->*D e
| mred_step : forall D e1 e2,
    e1 -->D e2 ->
    e1 -->*D e2
| mred_trans : forall D e1 e2 e3,
    e1 -->*D e2 ->
    e2 -->*D e3 ->
    e1 -->*D e3
where "e '-->*' D e'" := (mstep e D e').

(** ** closed subexpression reduction *)
Inductive csr_exp : trm -> orth -> trm -> Prop :=
| csr_exp_step : forall D e x e1 e2,
    orthant D ->
    exp e ->
    (forall x, x \notin fv e1 \u fv_s e1) ->
    e1 -->D e2 ->
    csr_exp ([x ~> e1]e) D ([x ~> e2]e)
| csr_exp_sym : forall D e1 e2,
    csr_exp e1 D e2 ->
    csr_exp e2 D e1
| csr_exp_trans : forall D e1 e2 e3,
    csr_exp e1 D e2 ->
    csr_exp e2 D e3 ->
    csr_exp e1 D e3.

(** ** type equivalence  *)
Reserved Notation "T1 == D T2" (at level 68, D at level 9).
Inductive equiv_typ : trm -> orth -> trm -> Prop :=
| equiv_typ_nat : forall D,
    orthant D ->
    trm_nat ==D trm_nat
| equiv_typ_bool : forall D,
    orthant D ->
    trm_bool ==D trm_bool
| equiv_typ_arrow : forall L D T11 T12 T21 T22,
    T11 ==D T21 ->
    (forall x,x \notin L -> (T12 $^ x) ==D  (T22 $^ x)) ->
    trm_arrow T11 T12 ==D trm_arrow T21 T22
| equiv_typ_refine : forall L D T1 T2 e1 e2,
    T1 ==D T2 ->
    (forall x, x \notin L -> csr_exp (e1 $^ x) D (e2 $^ x)) ->
    trm_refine T1 e1 ==D trm_refine T2 e2
| equiv_typ_forall : forall L D T1 T2,
    (forall x, x \notin L -> (T1 %^ x) ==D (T2 %^ x)) ->
    trm_forall T1 ==D trm_forall T2
where "T1 == D T2" := (equiv_typ T1 D T2).

(** ** type consistency *)
Reserved Notation "T1 ~= T2" (at level 68).
Inductive consist_typ : trm -> trm -> Prop :=
| consist_typ_nat :
    trm_nat ~= trm_nat
| consist_typ_bool :
    trm_bool ~= trm_bool
| consist_typ_arrow : forall L T11 T12 T21 T22,
    T11 ~= T21 ->
    (forall x, x \notin L -> (T12 $^ x) ~= (T22 $^ x)) ->
    (trm_arrow T11 T12) ~= (trm_arrow T21 T22)
| consist_typ_refine_l : forall T1 T2 e,
    T1 ~= T2 ->
    typ (trm_refine T1 e) ->
    (trm_refine T1 e) ~= T2
| consist_typ_refine_r : forall T1 T2 e,
    T1 ~= T2 ->
    typ (trm_refine T2 e) ->
    T1 ~= (trm_refine T2 e)
| consist_typ_forall : forall L T1 T2,
    (forall x, x \notin L -> (T1 %^ x) ~= (T2 %^ x)) ->
    (trm_forall T1) ~= (trm_forall T2)
where "T1 ~= T2" := (consist_typ T1 T2).

(** ugly relation for denoting a bound variable avoids free-variables
    in an orthant (cf. [wf_typ_forall] and [typing_sabs] *)
Inductive include_fv_s_orth (X : vars) (D : orth) :=
| include_fv_s_orth_intro :
    (forall b, (inl b) ∈ D -> forall x, x \notin X -> x \notin fv_s b) ->
    (forall b, (inr b) ∈ D -> forall x, x \notin X -> x \notin fv_s b) ->
    include_fv_s_orth X D.

(** ** well-formedness of bits *)
(** Note that [fv_s b \c sdom E] is enough, but there is no handy way
    to denote [sdom] *)
Inductive wf_bits : env -> trm -> Prop :=
| wf_bits_on : forall E,
    wf_bits E trm_on
| wf_bits_off : forall E,
    wf_bits E trm_off
| wf_bits_var : forall E x,
    binds x None E ->
    wf_bits E (trm_fvar_s x)
| wf_bits_concat : forall E b1 b2,
    wf_bits E b1 ->
    wf_bits E b2 ->
    wf_bits E (trm_concat b1 b2).

Reserved Notation "E |= D T" (at level 69, D at level 9).
Reserved Notation "E |= D e ~: T" (at level 69, D at level 9, e at next level).
Inductive wf_typ : env -> orth -> trm -> Prop :=
| wf_typ_bool : forall D,
    orthant D ->
    empty |=D trm_bool
| wf_typ_nat : forall D,
    orthant D ->
    empty |=D trm_nat
| wf_typ_arrow : forall L E D T1 T2,
    (forall x, x \notin L -> (E & x *~ T1) |=D (T2 $^ x)) ->
    E |=D (trm_arrow T1 T2)
| wf_typ_forall : forall L E D T,
    (forall x, x \notin L -> (E & 'x |=D (T %^ x))) ->
    include_fv_s_orth L D ->
    E |=D (trm_forall T)
| wf_typ_refine : forall L E D T e,
    (forall x, x \notin L -> (E & x *~ T) |=D (e $^ x) ~: trm_bool) ->
    E |=D (trm_refine T e)
| wf_typ_weaken : forall E D T1 T2 x,
    E |=D T1 ->
    E |=D T2 ->
    x # E ->
    (E & x *~ T1) |=D T2
| wf_typ_sweaken : forall E D T x,
    E |=D T ->
    x # E ->
    (E & 'x) |=D T
where "E |= D T" := (wf_typ E D T)
with typing : env -> orth -> trm -> trm -> Prop :=
| typing_true : forall D,
    orthant D ->
    empty |=D trm_true ~: trm_bool
| typing_false : forall D,
    orthant D ->
    empty |=D trm_false ~: trm_bool
| typing_zero : forall D,
    orthant D ->
    empty |=D trm_zero ~: trm_nat
| typing_var : forall D E x T,
    E |=D T ->
    x # E ->
    (E & x *~ T) |=D (trm_fvar x) ~: T
| typing_app : forall D E T1 T2 e1 e2,
    E |=D e1 ~: (trm_arrow T1 T2) ->
    E |=D e2 ~: T1 ->
    E |=D (trm_app e1 e2) ~: (T2 ^^ e2)
| typing_sapp : forall D E T e b,
    E |=D e ~: (trm_forall T) ->
    wf_bits E b ->
    E |=D (trm_sapp e b) ~: (T ^^ b)
| typing_fix : forall L D E T1 T2 e,
    (forall f x, fresh L 2 (x::f::nil) -> (E & f *~ (trm_arrow T1 T2) & x *~ T1) |=D (e $^ x $^ f) ~: (T2 $^ x)) ->
    E |=D (trm_fix (trm_arrow T1 T2) e) ~: (trm_arrow T1 T2)
| typing_succ : forall D E e,
    E |=D e ~: trm_nat ->
    E |=D (trm_succ e) ~: trm_nat
| typing_pred : forall D E e,
    E |=D e ~: (trm_refine trm_nat (trm_if (trm_iszero (trm_bvar 0)) trm_false trm_true)) ->
    E |=D (trm_pred e) ~: trm_nat
| typing_iszero : forall D E e,
    E |=D e ~: trm_nat ->
    E |=D (trm_iszero e) ~: trm_bool
| typing_if : forall D E T e1 e2 e3,
    E |=D e1 ~: trm_bool ->
    E |=D e2 ~: T ->
    E |=D e3 ~: T ->
    E |=D (trm_if e1 e2 e3) ~: T
| typing_sabs : forall L D E T e,
    (forall x, x \notin L -> (E & 'x) |=D (e %^ x) ~: (T %^ x)) ->
    include_fv_s_orth L D ->
    E |=D (trm_sabs e) ~: (trm_forall T)
| typing_choice : forall D E T e1 e2 b,
    E |=(D ⊌ (inl b)) e1 ~: T ->
    E |=(D ⊌ (inr b)) e2 ~: T ->
    E |=D T ->
    wf_bits E b ->
    E |=D (trm_choice e1 e2 b) ~: T
| typing_cast : forall D E T1 T2 e,
    E |=D e ~: T1 ->
    E |=D T2 ->
    T1 ~= T2 ->
    E |=D (trm_cast e T1 T2) ~: T2
| typing_waiting : forall D T e1 e2,
    empty |=D e1 ~: T ->
    empty |=D (trm_refine T e2) ->
    empty |=D (trm_waiting e1 (trm_refine T e2)) ~: (trm_refine T e2)
| typing_active : forall D T e1 e2 v,
    value v ->
    empty |=D e1 ~: trm_bool ->
    empty |=D v ~: T ->
    empty |=D (trm_refine T e2) ->
    (e2 ^^ v) -->*D e1 ->
    empty |=D (trm_active e1 v (trm_refine T e2)) ~: (trm_refine T e2)
| typing_blame : forall D T,
    empty |=D T ->
    empty |=D trm_blame ~: T
| typing_conv : forall D T1 T2 e,
    empty |=D e ~: T1 ->
    empty |=D T2 ->
    T1 ==D T2 ->
    empty |=D e ~: T2
| typing_forget : forall D T e v,
    value v ->
    empty |=D v ~: (trm_refine T e) ->
    empty |=D v ~: T
| typing_exact : forall D T e v,
    value v ->
    empty |=D v ~: T ->
    empty |=D (trm_refine T e) ->
    e ^^ v -->*D trm_true ->
    empty |=D v ~: (trm_refine T e)
| typing_weaken : forall D E T1 T2 e x,
    E |=D T1 ->
    E |=D e ~: T2 ->
    x # E ->
    (E & x *~ T1) |=D e ~: T2
| typing_sweaken : forall D E T e x,
    E |=D e ~: T ->
    x # E ->
    (E & 'x) |=D e ~: T
where "E |= D e ~: T" := (typing E D e T).