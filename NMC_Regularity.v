Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Export NMC_Infrastructure.

(** ** automation hints for digging locally-closed terms *)

Hint Extern 1 (bits ?b) =>
match goal with
| [ H: exp ?t |- _ ] => match t with
                      | b => fail 1
                      | context [b] => inversion H
                      end
| [ H: typ ?t |- _ ] => match t with
                      | b => fail 1
                      | context [b] => inversion H
                      end
end.

Hint Extern 1 (typ ?T) =>
match goal with
| [ H: exp ?t |- _ ] => match t with
                      | T => fail 1
                      | context [T] => inversion H
                      end
| [ H: typ ?t |- _ ] => match t with
                      | T => first [exact H | fail 1]
                      | context [T] => inversion H
                      end
end.

Hint Extern 1 (exp ?e) =>
match goal with
| [ H: exp ?t |- _ ] => match t with
                      | e => first [exact H | fail 1]
                      | context [e] => inversion H
                      end
| [ H: typ ?t |- _ ] => match t with
                      | e => fail 1
                      | context [e] => inversion H
                      end
end.

Hint Extern 1 (value ?v) =>
match goal with
| [ H: exp ?t |- _ ] => match t with
                      | v => fail 1
                      | context [v] => inversion H
                      end
| [ H: typ ?t |- _ ] => match t with
                      | v => fail 1
                      | context [v] => inversion H
                      end
end.

(** * Subset relations on syntax categories *)

Lemma nvalue_is_value : forall nv,
    nvalue nv ->
    value nv.
Proof. induction 1; auto. Qed.

Lemma value_is_exp : forall v,
    value v ->
    exp v.
Proof. induction 1; auto. induction H; auto. Qed.

Hint Resolve nvalue_is_value value_is_exp.

Lemma orthant_in_bits_l : forall D b,
    orthant D ->
    inl b ∈ D ->
    bits b.
Proof. inversion 1. auto. Qed.

Lemma orthant_in_bits_r : forall D b,
    orthant D ->
    inr b ∈ D ->
    bits b.
Proof. inversion 1. auto. Qed.

Hint Extern 1 (bits ?b) =>
match goal with
| [ H: inl b ∈ ?D |- _ ] => simple apply (@orthant_in_bits_l D b); [|exact H]
| [ H: inr b ∈ ?D |- _ ] => simple apply (@orthant_in_bits_r D b); [|exact H]
end.

Lemma orthant_push_l_inv : forall D b,
    orthant (D ⊌ inl b) ->
    orthant D /\ bits b.
Proof.
  intros. split.
  - inverts H. apply orthant_intro; auto with sets.
  - apply (orthant_in_bits_l H). eauto with sets.
Qed.

Lemma orthant_push_r_inv : forall D b,
    orthant (D ⊌ inr b) ->
    orthant D /\ bits b.
Proof.
  intros. split.
  - inverts H. apply orthant_intro; auto with sets.
  - apply (orthant_in_bits_r H). eauto with sets.
Qed.

Hint Extern 1 (orthant ?D) =>
match goal with
| [ _: context [ (D ⊌ inl ?b) ] |- _ ] => apply (@orthant_push_l_inv D b)
| [ _: context [ (D ⊌ inr ?b) ] |- _ ] => apply (@orthant_push_r_inv D b)
end.

Hint Extern 1 (bits ?b) =>
match goal with
| [ _: context [ (?D ⊌ inl b) ] |- _ ] => apply (@orthant_push_l_inv D b)
| [ _: context [ (?D ⊌ inr b) ] |- _ ] => apply (@orthant_push_r_inv D b)
end.

(** * well definedness of substitution *)

Lemma subst_bits : forall x u b,
    bits b -> exp u -> bits ([x ~> u]b).
Proof. induction 1; simpl; f_equal; auto. Qed.

Lemma subst_nvalue : forall x u nv,
    nvalue nv -> exp u -> nvalue ([x ~> u]nv).
Proof. induction 1; simpl; f_equal; auto. Qed.

Hint Resolve subst_bits subst_nvalue.

Lemma subst_typ_exp_value :
  (forall T, typ T -> forall x u, exp u -> typ ([x ~> u]T)) /\
  (forall e, exp e -> forall x u, exp u -> exp ([x ~> u]e)) /\
  (forall v, value v -> forall x u, exp u -> value ([x ~> u]v)).
Proof.
  apply typ_exp_value_ind; try easy; simpl; intros; f_equal; autos*.
  - apply_fresh~ typ_arrow. rewrite~ subst_open_var.
  - apply_fresh~ typ_refine. rewrite~ subst_open_var.
  - apply_fresh~ typ_forall. rewrite~ subst_open_var_s.
  - calc_open_subst~.
  - apply_fresh~ exp_fix. rewrite~ subst_open_var2.
  - apply_fresh~ exp_sabs. rewrite~ subst_open_var_s.
Qed.

Hint Extern 1 (typ ([_ ~> _]_)) => simple apply (proj31 subst_typ_exp_value).
Hint Extern 1 (exp ([_ ~> _]_)) => simple apply (proj32 subst_typ_exp_value).
Hint Extern 1 (value ([_ ~> _]_)) => simple apply (proj33 subst_typ_exp_value).

Lemma subst_s_bits : forall x u b,
    bits b -> bits u -> bits (subst_s x u b).
Proof. induction 1; simpl; f_equal; auto. calc_open_subst~. Qed.

Lemma subst_s_nvalue : forall x u nv,
    nvalue nv -> bits u -> nvalue (subst_s x u nv).
Proof. induction 1; simpl; f_equal; auto. Qed.

Hint Resolve subst_s_bits subst_s_nvalue.

Lemma subst_s_typ_exp_value :
  (forall T, typ T -> forall x u, bits u -> typ (subst_s x u T)) /\
  (forall e, exp e -> forall x u, bits u -> exp (subst_s x u e)) /\
  (forall v, value v -> forall x u, bits u -> value (subst_s x u v)).
Proof.
  apply typ_exp_value_ind; try easy; simpl; intros; f_equal; autos*.
  - apply_fresh~ typ_arrow. rewrite~ subst_s_open_var.
  - apply_fresh~ typ_refine. rewrite~ subst_s_open_var.
  - apply_fresh~ typ_forall. rewrite~ subst_s_open_var_s.
  - apply_fresh~ exp_fix. rewrite~ subst_s_open_var2.
  - apply_fresh~ exp_sabs. rewrite~ subst_s_open_var_s.
Qed.

Hint Extern 1 (typ (subst_s _ _ _)) => simple apply (proj31 subst_s_typ_exp_value).
Hint Extern 1 (exp (subst_s _ _ _)) => simple apply (proj32 subst_s_typ_exp_value).
Hint Extern 1 (value (subst_s _ _ _)) => simple apply (proj33 subst_s_typ_exp_value).

Fixpoint subst_s_axis (x : var) (u : trm) (a : axis) {struct a} : axis :=
  match a with
  | inl b => inl (subst_s x u b)
  | inr b => inr (subst_s x u b)
  end.

Notation Im := (Im axis axis).

Lemma subst_orthant : forall D x b,
    orthant D ->
    bits b ->
    orthant (Im D (subst_s_axis x b)).
Proof.
  intros. inverts H. apply orthant_intro.
  - intros. inverts H. destruct x0.
    + simpl in H4. inverts* H4.
    + simpl in H4. discriminate.
  - intros. inverts H. destruct x0.
    + simpl in H4. discriminate.
    + simpl in H4. inverts* H4.
Qed.

Hint Resolve subst_orthant.

Lemma include_fv_s_orth_Im : forall L D x u,
    include_fv_s_orth L D ->
    include_fv_s_orth (L \u fv_s u) (Im D (subst_s_axis x u)).
Proof.
  intros. inverts H. apply include_fv_s_orth_intro.
  - intros. inverts H. destruct x1.
    + simpl in H4. inverts H4. apply* fv_s_subst_s.
    + simpl in H4. discriminate.
  - intros. inverts H. destruct x1.
    + simpl in H4. discriminate.
    + simpl in H4. inverts H4. apply* fv_s_subst_s.
Qed.

(** * correctness of binders *)
Lemma open_arrow_body_typ : forall T1 T2 u,
    typ (trm_arrow T1 T2) ->
    exp u ->
    typ (T2 ^^ u).
Proof.
  inversion 1. pick_fresh y. intros. rewrite* (@subst_intro y).
Qed.

Lemma open_refine_body_exp : forall T e u,
    typ (trm_refine T e) ->
    exp u ->
    exp (e ^^ u).
Proof.
  inversion 1. pick_fresh y. intros. rewrite* (@subst_intro y).
Qed.

Lemma open_forall_body_typ : forall T u,
    typ (trm_forall T) ->
    bits u ->
    typ (T ^^ u).
Proof.
  inversion 1. pick_fresh y. intros. rewrite* (@subst_s_intro y).
Qed.

Lemma open_fix_body_exp : forall T e u1 u2,
    exp (trm_fix T e) ->
    exp u1 ->
    exp u2 ->
    exp (e ^^ u1 ^^ u2).
Proof.
  inversion 1. pick_fresh f; pick_fresh y. intros. rewrite* (@subst_intro2 y f).
Qed.

Lemma open_sabs_body_exp : forall e u,
    exp (trm_sabs e) ->
    bits u ->
    exp (e ^^ u).
Proof.
  inversion 1. pick_fresh y. intros. rewrite* (@subst_s_intro y).
Qed.

Lemma delayed_cast_regular_in : forall x y y0 v T11 T12 T21 T22 L1 L2,
    typ T11 ->
    (forall x, x \notin L1 -> typ (T12 $^ x)) ->
    typ T21 ->
    (forall x, x \notin L2 -> typ (T22 $^ x)) ->
    value v ->
    x \notin fv v \u fv T11 \u fv T12 \u fv T21 \u fv T22 ->
    y \notin L1 \u L2 ->
    y0 \notin L1 \u L2 ->
    exp (trm_cast (trm_app ({2 ~> trm_fvar y} (close_var_rec 2 x v)) (trm_bvar 0))
                  ({2 ~> trm_fvar y} (close_var_rec 2 x T12))
                  ({2 ~> trm_fvar y} (close_var_rec 2 x ({0 ~> trm_fvar x} T22))) $^ y0).
Proof.
  intros.
  assert (K2: typ ({0 ~> trm_fvar x}T22)).
  { fold (T22 $^ x). pick_fresh z. rewrite~ (@subst_intro z). }
  assert (K3: forall k, typ ({k ~> trm_fvar y}(close_var_rec k x ({0 ~> trm_fvar x} T22)))).
  { intro. rewrite~ <- subst_as_close_open_typ. }
  calc_open_subst. apply exp_cast.
  -- apply~ exp_app. rewrite~ close_var_fresh. do 2 rewrite~ <- open_value.
  -- rewrite~ close_var_fresh.
     rewrite~ <- (@open_rec_term_core 0 (trm_fvar y) T12).
     ++ fold (T12 $^ y0). auto.
     ++ fold (T12 $^ y). rewrite~ <- open_typ.
  -- rewrite~ <- (@open_typ 0 (trm_fvar y0)).
Qed.

Lemma delayed_cast_regular : forall x y v T11 T12 T21 T22 L1 L2,
    typ T11 ->
    (forall x, x \notin L1 -> typ (T12 $^ x)) ->
    typ T21 ->
    (forall x, x \notin L2 -> typ (T22 $^ x)) ->
    value v ->
    x \notin fv v \u fv T11 \u fv T12 \u fv T21 \u fv T22 ->
    y \notin L1 \u L2 ->
    exp (trm_app (close_var x (trm_fix (trm_arrow T11 (T22 $^ x)) (trm_cast (trm_app v (trm_bvar 0)) T12 (T22 $^ x))))
                 (trm_cast (trm_bvar 0) T21 T11) $^ y).
Proof.
  assert (K1: forall t x y, exp (t $^ x) -> exp (t $^ x $^ y)).
  { unfold open, open2. intros. rewrite~ <- open_exp. }
  intros. calc_open_subst. apply exp_app.
    + assert (K2: typ ({0 ~> trm_fvar x}T22)).
      { fold (T22 $^ x). pick_fresh z. rewrite~ (@subst_intro z). }
      assert (K3: forall k, typ ({k ~> trm_fvar y}(close_var_rec k x ({0 ~> trm_fvar x} T22)))).
      { intro. rewrite~ <- subst_as_close_open_typ. }
      apply_fresh exp_fix.
      * rewrite~ close_var_fresh.
        rewrite~ <- (@open_typ 0 (trm_fvar y)).
        apply_fresh~ typ_arrow. unfold open. rewrite~ <- (@open_typ 0 (trm_fvar y0)).
      * apply K1. apply* (@delayed_cast_regular_in x y y0 v T11 T12 T21 T22).
    + apply~ exp_cast.
      * rewrite~ <- open_typ.
      * rewrite~ <- open_typ.
Qed.

Lemma cast_arrow_post_regular : forall D T11 T12 T21 T22 v x,
    orthant D ->
    typ (trm_arrow T11 T12) ->
    typ (trm_arrow T21 T22) ->
    value v ->
    x \notin fv v \u fv T11 \u fv T12 \u fv T21 \u fv T22 ->
    exp
      (trm_fix (trm_arrow T21 T22)
               (trm_app (close_var x (trm_fix (trm_arrow T11 (T22 $^ x))
                                              (trm_cast (trm_app v (trm_bvar 0)) T12 (T22 $^ x))))
                        (trm_cast (trm_bvar 0) T21 T11))).
Proof.
  intros.
  assert (K1: forall t x y, exp (t $^ x) -> exp (t $^ x $^ y)).
  { unfold open, open2. intros. rewrite~ <- open_exp. }
  inverts H0. inverts H1.
  apply_fresh~ exp_fix. apply K1. apply* delayed_cast_regular.
Qed.

Lemma cast_forall_post_regular : forall D T1 T2 v,
    orthant D ->
    typ (trm_forall T1) ->
    typ (trm_forall T2) ->
    value v ->
    exp (trm_sabs (trm_cast (trm_sapp v (trm_bvar 0)) T1 T2)).
Proof.
  intros. inverts H0. inverts H1. apply_fresh exp_sabs.
  unfold open in *. repeat (simpl; case_nat).
  apply~ exp_cast. rewrite~ <- open_exp.
Qed.

(** * well definedness of relations *)
Lemma step_regular_orthant : forall D e e',
    e -->D e' ->
    orthant D.
Proof. induction 1; auto. Qed.

Hint Extern 1 (orthant ?D) =>
match goal with
| [ H: ?e1 -->?D' ?e2 |- _ ] => match D' with
                              | D => exact (step_regular_orthant H)
                              | context [D] => lets : (step_regular_orthant H)
                              end
end.

Lemma step_regular_pre : forall D e e',
    e -->D e' ->
    exp e.
Proof. induction 1; auto. Qed.

Hint Extern 1 (exp ?e) =>
match goal with
| [ H: ?e1 -->?D ?e2 |- _ ] => match e1 with
                             | e => exact (step_regular_pre H)
                             | context [e] => lets : (step_regular_pre H)
                             end
end.

Lemma step_regular_post : forall D e e',
    e -->D e' ->
    exp e'.
Proof.
  induction 1; auto.
  - apply* open_fix_body_exp.
  - apply* open_sabs_body_exp.
  - apply~ exp_active. apply* open_refine_body_exp.
  - apply* cast_arrow_post_regular.
  - apply* cast_forall_post_regular.
Qed.

Hint Extern 1 (exp ?e) =>
match goal with
| [ H: ?e1 -->?D ?e2 |- _ ] => match e2 with
                             | e => exact (step_regular_post H)
                             | context [e] => lets : (step_regular_post H)
                             end
end.

Lemma mstep_regular : forall D e e',
    e -->*D e' ->
    orthant D /\ exp e /\ exp e'.
Proof. induction 1; intros; intuition. Qed.

Hint Extern 1 (orthant ?D) =>
match goal with
| [ _: context [ ?e -->*D ?e' ] |- _ ] => apply (@mstep_regular D e e')
end.

Hint Extern 1 (exp ?t) =>
match goal with
| [ _: context [ t -->*?D ?e ] |- _ ] => apply (@mstep_regular D t e)
| [ _: context [ ?e -->*?D t ] |- _ ] => apply (@mstep_regular D e t)
end.

(** auxiliary lemma simplifying induction hypothesis of locally closed terms *)
Lemma lc_ih_split : forall P Q R,
    (forall (x : var), P x -> Q x /\ R x) ->
    (forall (x : var), P x -> Q x) /\ (forall (x : var), P x -> R x).
Proof.
  intros. split; intros.
  - exact (proj1 (H x X)).
  - exact (proj2 (H x X)).
Qed.

Lemma lc_ih_split2 : forall P Q R,
    (forall (f x : var), P f x -> Q f x /\ R f x) ->
    (forall (f x : var), P f x -> Q f x) /\ (forall (f x : var), P f x -> R f x).
Proof.
  intros. split; intros.
  - exact (proj1 (H f x X)).
  - exact (proj2 (H f x X)).
Qed.

Ltac lc_ih_split_base :=
  match goal with
  | H: forall x, x \notin _ -> _ /\ _ |- _ => apply lc_ih_split in H; destruct H
  | H: forall f x, fresh _ 2 _ -> _ /\ _ |- _ => apply lc_ih_split2 in H; destruct H
  end.

Ltac lc_ih_split := repeat lc_ih_split_base.

Lemma csr_exp_regular : forall D e1 e2,
    csr_exp e1 D e2 ->
    orthant D /\ exp e1 /\ exp e2.
Proof. induction 1; intuition. Qed.

Lemma equiv_typ_regular : forall D T1 T2,
    T1 ==D T2 ->
    orthant D /\ typ T1 /\ typ T2.
Proof.
  induction 1; lc_ih_split; splits 3; intuition.
  - apply_fresh* typ_refine. forwards~ K: (H0 y). apply* (csr_exp_regular K).
  - apply_fresh* typ_refine. forwards~ K: (H0 y). apply* (csr_exp_regular K).
  - pick_fresh y. forwards* : (H0 y).
Qed.

Hint Extern 1 (orthant ?D) =>
match goal with
| [ _: context [ ?T1 ==D ?T2 ] |- _ ] => apply (equiv_typ_regular D T1 T2)
end.

Hint Extern 1 (typ ?T) =>
match goal with
| [ _: context [ T ==?D ?T'] |- _ ] => apply (equiv_typ_regular D T T')
| [ _: context [ ?T' ==?D T] |- _ ] => apply (equiv_typ_regular D T' T)
end.

Lemma consist_typ_regular : forall T1 T2,
    T1 ~= T2 ->
    typ T1 /\ typ T2.
Proof.
  induction 1; intros; split; intuition.
  - apply_fresh~ typ_arrow. apply~ H1.
  - apply_fresh~ typ_arrow. apply~ H1.
  - apply_fresh~ typ_forall. apply~ H0.
  - apply_fresh~ typ_forall. apply~ H0.
Qed.

Hint Extern 1 (typ ?t) =>
match goal with
| [ _: t ~= ?T |- _ ] => apply (@consist_typ_regular t T)
| [ _: ?T ~= t |- _ ] => apply (@consist_typ_regular T t)
end.

Lemma wf_typ_typing_orth_regular :
  (forall E D T, E |=D T -> orthant D) /\
  (forall E D e T, E |=D e ~: T -> orthant D).
Proof.
  apply wf_typ_typing_ind; intros; auto.
  - pick_fresh y. apply~ (H0 y).
  - pick_fresh y. apply~ (H0 y).
  - pick_fresh y. apply~ (H0 y).
  - pick_fresh f; pick_fresh y. apply~ (H0 f y).
  - pick_fresh y. apply~ (H0 y).
Qed.

Hint Extern 1 (orthant ?D) =>
match goal with
| [ _: context [ ?E |=D ?T ] |- _ ] => simple apply ((proj1 wf_typ_typing_orth_regular) E D T)
| [ _: context [ ?E |=D ?e ~: ?T ] |- _ ] => simple apply ((proj2 wf_typ_typing_orth_regular) E D e T)
end.

Lemma environment_ok : forall E,
    environment E ->
    ok E.
Proof. induction 1; auto. Qed.

Hint Resolve environment_ok.

Lemma environment_push_inv : forall E x t,
    environment (E & x *~ t) ->
    environment E /\ typ t.
Proof.
  inversion 1; subst.
  - false* empty_push_inv.
  - destruct_eq_push H0. auto.
  - falsify_eq_push H0.
Qed.

Lemma environment_push_s_inv : forall E x,
    environment (E & 'x) ->
    environment E.
Proof.
  inversion 1; subst.
  - false* empty_push_inv.
  - falsify_eq_push H0.
  - destruct_eq_push H0. auto.
Qed.

Local Hint Extern 1 =>
match goal with
| [ _: context [ environment (?E & ?x *~ ?t) ] |- environment ?E ] =>
  apply (@environment_push_inv E x t)
| [ _: context [ environment (?E & ?x *~ ?t) ] |- typ ?t ] =>
  apply (@environment_push_inv E x t)
| [ _: context [ environment (?E & '?x) ] |- environment ?E ] =>
  simple apply (@environment_push_s_inv E x)
end.

Local Hint Extern 1 (environment ?E) =>
match goal with
| H: forall x, x \notin _ -> environment (E & _ *~ _) |- _ =>
  let y := fresh in
  pick_fresh y; eapply environment_push_inv; apply (H y); notin_solve
| H: forall x, x \notin _ -> environment (E & '_) |- _ =>
  let y := fresh in
  pick_fresh y; eapply environment_push_s_inv; apply (H y); notin_solve
| H: forall f x, fresh _ 2 _ -> environment (E & _ *~ _) |- _ =>
  let f := fresh in
  let y := fresh in
  pick_fresh f; pick_fresh y; eapply environment_push_inv; apply (H f y); notin_solve
end.

Lemma wf_bits_regular : forall E b,
    wf_bits E b ->
    bits b.
Proof. induction 1; auto. Qed.

Hint Resolve wf_bits_regular.

Lemma wf_typ_typing_regular :
  (forall E D T, E |=D T -> environment E /\ typ T) /\
  (forall E D e T, E |=D e ~: T -> environment E /\ exp e /\ typ T).
Proof.
  apply wf_typ_typing_ind; intros; lc_ih_split; first [splits 3 | split]; autos*.
  - apply_fresh~ typ_arrow. pick_fresh y. forwards~ : (H0 y).
  - apply_fresh~ typ_refine. pick_fresh y. forwards~ : (H0 y).
  - apply* open_arrow_body_typ.
  - apply* open_forall_body_typ.
  - pick_fresh f; pick_fresh y. eapply (@environment_push_inv _ f). eapply (@environment_push_inv _ y). auto.
  - apply_fresh~ exp_fix. pick_fresh f; pick_fresh y.
    apply (@environment_push_inv E f). eapply (@environment_push_inv _ y). auto.
  - apply_fresh~ typ_arrow.
    + pick_fresh f; pick_fresh y. eapply (@environment_push_inv (E & f *~ _) y). auto.
    + pick_fresh f. apply~ (H2 f y).
Qed.

Hint Extern 1 (environment ?E) =>
match goal with
| H: E |=?D ?T |- _ => simple apply (proj1 ((proj1 wf_typ_typing_regular) E D T H))
| H: E |=?D ?e ~: ?T |- _ => simple apply (proj1 ((proj2 wf_typ_typing_regular) E D e T H))
end.

Hint Extern 1 (typ ?t) =>
match goal with
| H: ?E |=?D t |- _ => simple apply (proj2 ((proj1 wf_typ_typing_regular) E D t H))
| H: ?E |=?D ?e ~: t |- _ => simple apply (proj33 ((proj2 wf_typ_typing_regular) E D e t H))
end.

Hint Extern 1 (exp ?t) =>
match goal with
| H: ?E |=?D t ~: ?T |- _ => simple apply (proj32 ((proj2 wf_typ_typing_regular) E D t T H))
end.
