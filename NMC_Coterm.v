Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity NMC_Step NMC_Eval NMC_EvalStep.

Lemma result_var_inv : forall D x,
    result D (trm_fvar x) -> False.
Proof. inversion 1; intros; tryfalse. inverts H1; tryfalse. inverts H4; tryfalse. Qed.

Lemma coterm_var : forall D e1 e2 e x R,
    x \notin fv e1 ->
    normal_orth D ->
    e1 -->D e2 \/ e2 -->D e1 ->
    e = e1 ->
    e \\D R ->
    exists R', result D R' /\ R = [x ~> e1]R' /\ [x ~> e2](trm_fvar x) \\D [x ~> e2]R'.
Proof.
  intros. calc_open_subst. subst. exists R. assert (x \notin fv R).
  { apply (eval_keep_fv H3 H). }
  repeat rewrite~ subst_fresh. splits 3.
  - apply (eval_result H3).
  - reflexivity.
  - destruct H1.
    + apply (eval_red_left H1 H3).
    + apply (eval_red_right H1 H0 H3).
Qed.

Ltac invertsSubst :=
  match goal with
  | [ Eq: ?e0 = [?x ~> ?e1]?e' |- _ ] =>
    destruct e'; simpl in Eq; inverts Eq;
    [ case_var; apply coterm_var with (e := e0); auto | idtac]
  end.

Ltac examineGoal R :=
  exists R; calc_open_subst; splits 3; [ eauto | congruence | idtac ].

Ltac revertSimpl :=
  match goal with
  | [ H: exp ?e |- exp ?e' ] =>
    match e' with
    | context [ subst ?x ?e2 _ ] =>
      replace e' with ([x ~> e2]e) by reflexivity
    end
  | [ H: typ ?e |- typ ?e' ] =>
    match e' with
    | context [ subst ?x ?e2 _ ] =>
      replace e' with ([x ~> e2]e) by reflexivity
    end
  end.

Lemma co_bits : forall x e1 e2 b,
    exp e1 ->
    exp e2 ->
    bits ([x ~> e1]b) ->
    bits ([x ~> e2]b).
Proof. intros. induction b; simpl in H1; inverts H1; simpl; auto; case_var; subst; inversion H. Qed.

Lemma subst_nvalue_inv : forall D R x e,
    result D R ->
    nvalue ([x ~> e]R) ->
    nvalue R.
Proof.
  intros. inverts H.
  - inverts H2; auto; simpl in H0; inversion H0.
  - simpl in H0; inversion H0.
  - simpl in H0; inversion H0.
Qed.

Lemma co_nvalue : forall D R x e e',
    result D R ->
    nvalue ([x ~> e]R) ->
    nvalue ([x ~> e']R).
Proof.
  intros. induction R; simpl in H0; inverts H0; try solve [false (result_var_inv H)].
  - simpl. auto.
  - simpl. inverts H. inverts H1. inverts H. apply~ nvalue_succ.
Qed.

Lemma subst_value_inv : forall D R x e,
    result D R ->
    value ([x ~> e]R) ->
    value R.
Proof.
  intros. inverts H.
  - auto.
  - simpl in H0. inversion H0. inversion H.
  - simpl in H0. inversion H0. inversion H.
Qed.

Lemma co_value : forall D R x e e',
    result D R ->
    exp e ->
    exp e' ->
    value ([x ~> e]R) ->
    value ([x ~> e']R).
Proof. inversion 1; subst; intros; auto. simpl in H6. inversion H6. inversion H7. Qed.

Ltac destructSubst H :=
  match type of H with
  | _ = [ _ ~> _ ] ?e' =>
    destruct e'; simpl in H; inverts H;
    try solve [match goal with
               | H1: typ (trm_fvar _) |- _ => inversion H1
               | H1: result _ (trm_fvar _) |- _ => false (result_var_inv H1)
               end]
  end.

Lemma bits_fv : forall x b,
    bits b ->
    x \notin fv b.
Proof. induction 1; simpl; auto. Qed.

Lemma subst_close_var_rec : forall x y e t k,
    y <> x ->
    y \notin fv e ->
    [x ~> e](close_var_rec k y t) = close_var_rec k y ([x ~> e]t).
Proof.
  induction t; simpl; intros; f_equal; auto. case_var; case_var.
  - calc_open_subst~.
  - calc_open_subst~. rewrite~ close_var_fresh.
  - calc_open_subst~.
Qed.

Lemma coterm : forall D e e' x e1 e2 R,
    e \\D R ->
    exp e1 ->
    exp e2 ->
    x \notin fv e1 ->
    x \notin fv e2 ->
    (e1 -->D e2 \/ e2 -->D e1) ->
    normal_orth D ->
    e = [x ~> e1]e' ->
    exp e' ->
    exists R', result D R' /\ R = [x ~> e1]R' /\ [x ~> e2]e' \\D [x ~> e2]R'.
Proof.
  introv Eval Exp1 Exp2 Nin1 Nin2. assert (e \\D R) by auto. move: e' H.
  induction Eval; introv Eval' Step Normal Eq Exp'; invertsSubst.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H1. forwards* : (IHEval2 e'2). unpack. subst.
    destructSubst H6; first last. inversion H0. inversion H6. inversion H11.
    assert (exp (R'2 ^^ R' ^^ (trm_fix (trm_arrow R'1_1 R'1_2) R'2))).
    { apply* open_fix_body_exp. }
    forwards* : (IHEval3 (R'2 ^^ R' ^^ (trm_fix (trm_arrow R'1_1 R'1_2) R'2))).
    { rewrite~ subst_open2. } unpack. subst.
    examineGoal R'0. simpl in H4. rewrite subst_open2 in H9; auto.
    assert (value ([x ~> e2]R')) by apply* (@co_value D R' x e1 e2).
    apply* eval_app.
  - destruct e'1; simpl in H2; inverts H2.
    + examineGoal (trm_fix (trm_arrow e'1_1 e'1_2) e'2).
      apply~ eval_fix. revertSimpl. auto.
    + case_var. subst. inversion Exp1.
  - examineGoal trm_zero. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst.
    assert (nvalue R') by apply* subst_nvalue_inv. examineGoal (trm_succ R'). apply~ eval_succ.
  - inverts Exp'. forwards* : IHEval. unpack.
    destruct R'; simpl in H2; inverts H2.
    + false* result_var_inv.
    + assert (result D R').
      { inverts H0. inverts H4. inverts H0. auto. }
      assert (nvalue R') by apply* subst_nvalue_inv.
      examineGoal R'. apply~ eval_pred.
  - inverts Exp'. forwards* : IHEval. unpack.
    destruct R'; simpl in H1; inverts H1.
    + false* result_var_inv.
    + examineGoal trm_true. apply~ eval_iszero_t.
  - inverts Exp'. forwards* : IHEval. unpack.
    destruct R'; simpl in H2; inverts H2.
    + false* result_var_inv.
    + assert (result D R').
      { inverts H0. inverts H4. inverts H0. auto. }
      assert (nvalue R') by apply* subst_nvalue_inv. simpl in H3. assert (nvalue ([x ~> e2]R')) by auto.
      examineGoal trm_false. apply* eval_iszero_f.
  - examineGoal trm_true. auto.
  - examineGoal trm_false. auto.
  - inverts Exp'. forwards* : IHEval1. forwards* : IHEval2. unpack.
    destruct R'0; simpl in H7; inverts H7.
    + false* result_var_inv.
    + examineGoal R'. apply~ eval_if_t.
  - inverts Exp'. forwards* : IHEval1. forwards* : IHEval2. unpack.
    destruct R'0; simpl in H7; inverts H7.
    + false* result_var_inv.
    + examineGoal R'. auto.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H1.
    assert (exp (R' ^^ e'2)).
    { apply* open_sabs_body_exp. }
    forwards* : (IHEval2 (R' ^^ e'2)).
    { rewrite~ subst_open. } unpack. subst.
    simpl in H4. rewrite subst_open in H7; first by [].
    examineGoal R'0. apply* eval_sapp.
  - examineGoal (trm_sabs e'). apply~ eval_sabs. revertSimpl. auto.
  - inverts Exp'. forwards* : IHEval1. unpack. subst. simpl in H2. inverts H2.
    forwards~ : (IHEval2 (trm_active (e4 ^^ R') R' (trm_refine T0 e4))).
    + simpl. rewrite~ subst_open.
    + apply~ exp_active.
      * apply* open_refine_body_exp.
      * apply* subst_value_inv.
    + unpack. examineGoal R'0. assert (value ([x ~> e2]R')) by apply* (@co_value D R' x e1).
      apply* eval_waiting. rewrite* <- subst_open.
  - inverts Exp'. forwards* : IHEval. unpack. examineGoal e'2.
    apply* eval_active_s. revertSimpl. auto.
    destruct R'; simpl in H2; inverts H2; try false (result_var_inv H1). simpl in H3. auto.
  - inverts Exp'. forwards* : IHEval. unpack. examineGoal trm_blame.
    apply* eval_active_f. revertSimpl. auto.
    destruct R'; simpl in H2; inverts H2; try false (result_var_inv H1). simpl in H3. auto.
  - examineGoal trm_blame. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst.
    examineGoal R'. destruct e'2; simpl in H2; inverts H2; try solve [inversion H6].
    destruct e'3; simpl in H3; inverts H3; try solve [inversion H7].
    simpl. apply~ eval_c_bool. apply* (@co_value D R' x e1).
  - inverts Exp'. forwards* : IHEval. unpack. subst.
    examineGoal R'. destruct e'2; simpl in H2; inverts H2; try solve [inversion H6].
    destruct e'3; simpl in H3; inverts H3; try solve [inversion H7].
    simpl. apply~ eval_c_nat. apply* (@co_value D R' x e1).
  - inverts Exp'. forwards* : IHEval. unpack. subst.
    destructSubst H5. destructSubst H6. simpl.
    pick_fresh z from (fv ([x ~> e1] R') \u fv ([x ~> e1] e'2_1) \u fv ([x ~> e1] e'2_2) \u fv ([x ~> e1] e'3_1) \u fv ([x ~> e1] e'3_2) \u fv ([x ~> e2] R') \u fv ([x ~> e2] e'2_1) \u fv ([x ~> e2] e'2_2) \u fv ([x ~> e2] e'3_1) \u fv ([x ~> e2] e'3_2)).
    assert (exp (trm_fix (trm_arrow (e'3_1) (e'3_2))
    (trm_app
       (close_var z
          (trm_fix (trm_arrow (e'2_1) (e'3_2 $^ z))
             (trm_cast (trm_app (R') (trm_bvar 0)) (e'2_2) (e'3_2 $^ z))))
       (trm_cast (trm_bvar 0) (e'3_1) (e'2_1))))).
    { assert (K1: forall t x y, exp (t $^ x) -> exp (t $^ x $^ y)).
      { unfold open, open2. intros. rewrite~ <- open_exp. }
      inverts H9. inverts H10.
      apply_fresh~ exp_fix. apply K1. apply* delayed_cast_regular.
      apply* subst_value_inv. }
    exists (trm_fix (trm_arrow (e'3_1) (e'3_2))
    (trm_app
       (close_var z
          (trm_fix (trm_arrow (e'2_1) (e'3_2 $^ z))
             (trm_cast (trm_app (R') (trm_bvar 0)) (e'2_2) (e'3_2 $^ z))))
       (trm_cast (trm_bvar 0) (e'3_1) (e'2_1)))). splits 3.
    + auto.
    + rewrite (@delayed_cast_close_var _ _ _ _ _ z). auto. auto.
      unfold close_var. simpl. repeat rewrite~ subst_close_var_rec.
      rewrite~ subst_open_var.
    + unfold close_var. simpl. repeat rewrite~ subst_close_var_rec.
      replace (trm_fix (trm_arrow (close_var_rec 0 z ([x ~> e2] e'2_1)) (close_var_rec 1 z ([x ~> e2] (e'3_2 $^ z))))
          (trm_cast (trm_app (close_var_rec 2 z ([x ~> e2] R')) (trm_bvar 0))
             (close_var_rec 2 z ([x ~> e2] e'2_2)) (close_var_rec 2 z ([x ~> e2] (e'3_2 $^ z))))) with
       (close_var z (trm_fix (trm_arrow ([x ~> e2]e'2_1) ([x ~> e2](e'3_2 $^ z)))
                                 (trm_cast (trm_app ([x ~> e2]R') (trm_bvar 0))
                                           ([x ~> e2]e'2_2) ([x ~> e2](e'3_2 $^ z))))); last by reflexivity.
      rewrite~ <- subst_open_var. apply~ eval_c_arrow.
      * revertSimpl. auto.
      * revertSimpl. auto.
      * apply~ (@co_value D R' x e1).
  - inverts Exp'. forwards* : IHEval. unpack. subst.
    destructSubst H4. destructSubst H5.
    assert (exp (trm_sabs (trm_cast (trm_sapp R' (trm_bvar 0)) e'2 e'3))).
    { inverts H8. inverts H9. unfold open in *.
      apply_fresh exp_sabs. calc_open_subst. apply~ exp_cast. rewrite~ <- open_exp. }
    examineGoal (trm_sabs (trm_cast (trm_sapp R' (trm_bvar 0)) e'2 e'3)).
    apply~ eval_c_forall. revertSimpl. auto. revertSimpl. auto. apply* (@co_value D R' x e1).
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H3. assert (exp (trm_cast R' e'2_1 e'3)).
    { inverts H6. auto. }
    forwards~ : (IHEval2 (trm_cast R' e'2_1 e'3)). unpack. subst.
    assert (value ([x ~> e2]R')).
    { apply* (@co_value D R' x e1). }
    examineGoal R'0. apply* eval_c_forget. revertSimpl. auto.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H4.
    assert (exp (trm_waiting (trm_cast R' e'2 e'3_1) (trm_refine e'3_1 e'3_2))).
    { inverts H7. auto. }
    forwards~ : (IHEval2 (trm_waiting (trm_cast R' e'2 e'3_1) (trm_refine e'3_1 e'3_2))). unpack. subst.
    assert (value ([x ~> e2]R')).
    { apply* (@co_value D R' x e1). }
    examineGoal R'0. apply* eval_c_precheck.
    intros. intro.
    destruct e'2; simpl in H10; inverts H10.
    + specialize (H0 ([x ~> e1]e'2_1) ([x ~> e1]e'2_2)). simpl in H0. congruence.
    + inversion H6.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H0.
    assert (exp (trm_choice (trm_app R'1 e'2) (trm_app R'2 e'2) R'3)).
    { destruct (result_regular H). inverts H4. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_app R'1 e'2) (trm_app R'2 e'2) R'3)). unpack. subst.
    examineGoal R'. simpl in H3. eauto.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    forwards~ : (IHEval2 e'2). unpack. subst.
    destructSubst H5.
    assert (exp (trm_choice (trm_app R' R'0_1) (trm_app R' R'0_2) R'0_3)).
    { destruct (result_regular H1). inverts H7. auto. }
    forwards~ : (IHEval3 (trm_choice (trm_app R' R'0_1) (trm_app R' R'0_2) R'0_3)). unpack. subst.
    examineGoal R'0. simpl in H6.
    assert (value ([x ~> e2]R')).
    { apply* (@co_value D R' x e1). }
    apply* eval_dist_app_r.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H1.
    assert (exp (trm_choice (trm_succ R'1) (trm_succ R'2) R'3)).
    { destruct (result_regular H). inverts H3. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_succ R'1) (trm_succ R'2) R'3)). unpack. subst.
    examineGoal R'. simpl in H2. eauto.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H1.
    assert (exp (trm_choice (trm_pred R'1) (trm_pred R'2) R'3)).
    { destruct (result_regular H). inverts H3. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_pred R'1) (trm_pred R'2) R'3)). unpack. subst.
    examineGoal R'. simpl in H2. eauto.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H1.
    assert (exp (trm_choice (trm_iszero R'1) (trm_iszero R'2) R'3)).
    { destruct (result_regular H). inverts H3. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_iszero R'1) (trm_iszero R'2) R'3)). unpack. subst.
    examineGoal R'. simpl in H2. eauto.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H0.
    assert (exp (trm_choice (trm_if R'1 e'2 e'3) (trm_if R'2 e'2 e'3) R'3)).
    { destruct (result_regular H). inverts H5. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_if R'1 e'2 e'3) (trm_if R'2 e'2 e'3) R'3)). unpack. subst.
    examineGoal R'. simpl in H1. apply* eval_dist_if.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H0.
    assert (exp (trm_choice (trm_sapp R'1 e'2) (trm_sapp R'2 e'2) R'3)).
    { destruct (result_regular H). inverts H4. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_sapp R'1 e'2) (trm_sapp R'2 e'2) R'3)). unpack. subst.
    examineGoal R'. simpl in H3. apply* eval_dist_sapp.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H0.
    assert (exp (trm_choice (trm_cast R'1 e'2 e'3) (trm_cast R'2 e'2 e'3) R'3)).
    { destruct (result_regular H). inverts H5. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_cast R'1 e'2 e'3) (trm_cast R'2 e'2 e'3) R'3)). unpack. subst.
    examineGoal R'. simpl in H1. apply* eval_dist_cast.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H0. destructSubst H1.
    assert (exp (trm_choice (trm_waiting R'1 (trm_refine t1 t2)) (trm_waiting R'2 (trm_refine t1 t2)) R'3)).
    { destruct (result_regular H). inverts H1. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_waiting R'1 (trm_refine t1 t2)) (trm_waiting R'2 (trm_refine t1 t2)) R'3)). unpack. subst.
    examineGoal R'. simpl in H4. apply* eval_dist_waiting.
  - inverts Exp'. forwards* : IHEval1. unpack. subst.
    destructSubst H2. destructSubst H0.
    assert (exp (trm_choice (trm_active R'1 e'2 (trm_refine t1 t2)) (trm_active R'2 e'2 (trm_refine t1 t2)) R'3)).
    { destruct (result_regular H). inverts H2. auto. }
    forwards~ : (IHEval2 (trm_choice (trm_active R'1 e'2 (trm_refine t1 t2)) (trm_active R'2 e'2 (trm_refine t1 t2)) R'3)). unpack. subst.
    examineGoal R'. simpl in H1. apply* eval_dist_active.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H1.
    examineGoal trm_blame. auto.
  - inverts Exp'. forwards* : IHEval1. unpack. subst. forwards~ : (IHEval2 e'2). unpack. subst.
    destructSubst H5. examineGoal trm_blame. assert (value ([x ~> e2]R')).
    { apply* (@co_value D R' x e1). }
    apply* eval_blame_app_r.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H1.
    examineGoal trm_blame. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H1.
    examineGoal trm_blame. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H1.
    examineGoal trm_blame. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H2.
    examineGoal trm_blame. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H1.
    examineGoal trm_blame. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H2.
    examineGoal trm_blame. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H1.
    examineGoal trm_blame. apply* eval_blame_waiting. revertSimpl. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst. destructSubst H2.
    examineGoal trm_blame. apply* eval_blame_active. revertSimpl. auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst.
    examineGoal R'.
    replace ([x ~> e1]e'3) with e'3 in *; first last.
    { rewrite~ subst_fresh. apply~ bits_fv. }
    replace ([x ~> e2]e'3) with e'3 in *; first last.
    { rewrite~ subst_fresh. apply~ bits_fv. }
    auto.
  - inverts Exp'. forwards* : IHEval. unpack. subst.
    examineGoal R'.
    replace ([x ~> e1]e'3) with e'3 in *; first last.
    { rewrite~ subst_fresh. apply~ bits_fv. }
    replace ([x ~> e2]e'3) with e'3 in *; first last.
    { rewrite~ subst_fresh. apply~ bits_fv. }
    auto.
  - inverts Exp'.
    replace ([x ~> e1]e'3) with e'3 in *; first last.
    { rewrite~ subst_fresh. apply~ bits_fv. }
    replace ([x ~> e2]e'3) with e'3 in *; first last.
    { rewrite~ subst_fresh. apply~ bits_fv. }
    forwards* : IHEval1.
    { destruct Step.
      * left. apply* step_orth_weaken. auto with sets.
      * right. apply* step_orth_weaken. auto with sets. }
    { apply* normal_add_orhtant_l. }
    unpack. subst.
    forwards* : IHEval2.
    { destruct Step.
      * left. apply* step_orth_weaken. auto with sets.
      * right. apply* step_orth_weaken. auto with sets. }
    { apply* normal_add_orhtant_r. }
    unpack. subst.
    exists (trm_choice R' R'0 e'3). splits 3.
    + auto.
    + simpl. rewrite~ (@subst_fresh x e1 e'3). apply~ bits_fv.
    + simpl. rewrite~ (@subst_fresh x e2 e'3). apply~ bits_fv.
Qed.

Lemma coterm_true_left : forall D x e1 e2 e,
    normal_orth D ->
    exp e ->
    e1 -->D e2 ->
    (forall x, x \notin fv e1) ->
    [x ~> e1]e -->*D trm_true ->
    [x ~> e2]e -->*D trm_true.
Proof.
  intros. apply mstep_eval in H3; auto. apply~ eval_mstep.
  rewrite <- (@subst_fresh x e2 trm_true); last by simpls~.
  assert (exp e1) by auto. assert (exp e2) by auto.
  assert (x \notin fv e1) by auto. assert (x \notin fv e2) by apply* step_keep_fv.
  assert (e1 -->D e2 \/ e2 -->D e1) by auto. assert ([x ~> e1]e = [x ~> e1]e) by reflexivity.
  lets : (coterm H3 H4 H5 H6 H7 H8 H H9 H0). unpack.
  destruct R'; simpl in H11; inverts H11; try false (result_var_inv H10).
  assumption.
Qed.

Lemma coterm_true_right : forall D x e1 e2 e,
    normal_orth D ->
    exp e ->
    e2 -->D e1 ->
    (forall x, x \notin fv e2) ->
    [x ~> e1]e -->*D trm_true ->
    [x ~> e2]e -->*D trm_true.
Proof.
  intros. apply mstep_eval in H3; auto. apply~ eval_mstep.
  rewrite <- (@subst_fresh x e2 trm_true); last by simpls~.
  assert (exp e1) by auto. assert (exp e2) by auto.
  assert (x \notin fv e1) by apply* step_keep_fv. assert (x \notin fv e2) by auto.
  assert (e1 -->D e2 \/ e2 -->D e1) by auto. assert ([x ~> e1]e = [x ~> e1]e) by reflexivity.
  lets : (coterm H3 H4 H5 H6 H7 H8 H H9 H0). unpack.
  destruct R'; simpl in H11; inverts H11; try false (result_var_inv H10).
  assumption.
Qed.
