Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity.

Lemma step_keep_fv : forall D e e' x,
    e -->D e' ->
    x \notin fv e ->
    x \notin fv e'.
Proof.
  induction 1; simpl; intros; auto.
  - apply* fv_open2. simpl. auto.
  - apply* fv_open.
  - assert (x \notin fv (e ^^ v)) by apply* fv_open. auto.
  - assert (x \notin fv (close_var_rec 0 x0 T11)) by apply* fv_close_var_rec.
    assert (x \notin fv (close_var_rec 1 x0 (T22 $^ x0))).
    { apply* fv_ex_bound_lv. }
    assert (x \notin fv (close_var_rec 2 x0 (T22 $^ x0))).
    { apply* fv_ex_bound_lv. }
    assert (x \notin fv (close_var_rec 2 x0 v)) by apply* close_var_rec_fv.
    assert (x \notin fv (close_var_rec 2 x0 T12)) by apply* close_var_rec_fv.
    auto.
Qed.

Lemma step_keep_fv_s : forall D e e' x,
    e -->D e' ->
    x \notin fv_s e ->
    x \notin fv_s e'.
Proof.
  induction 1; simpl; intros; auto.
  - apply* open2_fv_s. simpl. auto.
  - apply* open_fv_s.
  - assert (x \notin fv_s (e ^^ v)) by apply* open_fv_s. auto.
  - assert (x \notin fv_s (close_var_rec 0 x0 T11)) by apply* close_var_rec_fv_s.
    assert (x \notin fv_s (close_var_rec 1 x0 (T22 $^ x0))).
    { apply close_var_rec_fv_s. apply open_fv_s. simpl. auto. }
    assert (x \notin fv_s (close_var_rec 2 x0 (T22 $^ x0))).
    { apply close_var_rec_fv_s. apply open_fv_s. simpl. auto. }
    assert (x \notin fv_s (close_var_rec 2 x0 v)) by apply* close_var_rec_fv_s.
    assert (x \notin fv_s (close_var_rec 2 x0 T12)) by apply* close_var_rec_fv_s.
    auto.
Qed.

Lemma mstep_keep_fv : forall D e e' x,
    e -->*D e' ->
    x \notin fv e ->
    x \notin fv e'.
Proof.
  induction 1; intros; auto.
  - apply* step_keep_fv.
Qed.

Lemma mstep_keep_fv_s : forall D e e' x,
    e -->*D e' ->
    x \notin fv_s e ->
    x \notin fv_s e'.
Proof.
  induction 1; intros; auto.
  - apply* step_keep_fv_s.
Qed.

Lemma orthant_union_split : forall D D',
    orthant (D ∪ D') -> orthant D /\ orthant D'.
Proof. inversion 1; split; apply orthant_intro; auto with sets. Qed.

Lemma orthant_union_combine : forall D D',
    orthant D ->
    orthant D' ->
    orthant (D ∪ D').
Proof.
  do 2 inversion 1; subst.
  apply orthant_intro; introv U; apply Union_inv in U; intuition auto with sets.
Qed.

Lemma orthant_push_replace : forall D D' x,
    orthant (D ⊌ x) ->
    orthant D' ->
    orthant (D' ⊌ x).
Proof. intros. apply orthant_union_split in H. apply orthant_union_combine; intuition. Qed.

Lemma step_orth_weaken : forall D D' e e',
    e -->D e' ->
    orthant D' ->
    D ⊆ D' ->
    e -->D' e'.
Proof.
  introv Step. move: D'.
  induction Step; intros; auto.
  - apply~ step_ctx_choice_l. apply IHStep.
    + apply~ orthant_push_replace. eapply step_regular_orthant. apply Step.
    + auto with sets.
  - apply~ step_ctx_choice_r. apply IHStep.
    + apply~ orthant_push_replace. eapply step_regular_orthant. apply Step.
    + auto with sets.
Qed.

Lemma mstep_orth_weaken : forall D D' e e',
    e -->*D e' ->
    orthant D' ->
    D ⊆ D' ->
    e -->*D' e'.
Proof.
  introv Step. move: D'.
  induction Step; intros.
  - apply~ mred_refl.
  - apply mred_step. apply* step_orth_weaken.
  - apply~ mred_trans.
Qed.

Lemma step_orthant_subst_s : forall D e e' x b,
    e -->D e' ->
    x \notin fv_s e ->
    bits b ->
    e -->(Im D (subst_s_axis x b)) e'.
Proof. 
  induction 1; simpl; intros; auto.
  - apply* step_choice_l. apply Im_intro with (x := inl b0); first by easy.
    simpl. f_equal. rewrite* subst_s_fresh.
  - apply* step_choice_r. apply Im_intro with (x := inr b0); first by easy.
    simpl. f_equal. rewrite* subst_s_fresh.
  - apply* step_ctx_choice_l. rewrite <- (@subst_s_fresh x b b0); last by auto.
    replace (inl (subst_s x b b0)) with (subst_s_axis x b (inl b0)); last by easy.
    rewrite* <- Im_add.
  - apply* step_ctx_choice_r. rewrite <- (@subst_s_fresh x b b0); last by auto.
    replace (inr (subst_s x b b0)) with (subst_s_axis x b (inr b0)); last by easy.
    rewrite* <- Im_add.
Qed.

Lemma mstep_orthant_subst_s : forall D e e' x b,
    e -->*D e' ->
    x \notin fv_s e ->
    bits b ->
    e -->*(Im D (subst_s_axis x b)) e'.
Proof.
  induction 1; intros.
  - apply* mred_refl.
  - apply* mred_step. apply* step_orthant_subst_s.
  - apply* mred_trans. apply* IHmstep2. apply* mstep_keep_fv_s.
Qed.
