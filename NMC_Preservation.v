Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity NMC_Step NMC_EquivType NMC_TypingClosed NMC_TypingWeaken NMC_TypingSubst.

Inductive wf_env : orth -> env -> Prop :=
| wf_env_empty : forall D,
    wf_env D empty
| wf_env_push : forall D E x T,
    wf_env D E ->
    x # E ->
    E |=D T ->
    wf_env D (E & x *~ T)
| wf_env_push_s : forall D E x,
    wf_env D E ->
    x # E ->
    wf_env D (E & 'x).

Local Hint Constructors wf_env.

Lemma wf_env_push_inv : forall x D E T,
    wf_env D (E & x *~ T) ->
    wf_env D E /\ E |=D T.
Proof.
  inversion 1.
  - false* empty_push_inv.
  - destruct_eq_push H0. auto.
  - falsify_eq_push  H0.
Qed.

Lemma wf_env_push_s_inv : forall x D E,
    wf_env D (E & 'x) ->
    wf_env D E.
Proof.
  inversion 1.
  - false* empty_push_inv.
  - falsify_eq_push  H0.
  - destruct_eq_push H0. auto.
Qed.

Local Hint Extern 1 =>
match goal with
| [ _: context [ wf_env ?D (?E & ?x *~ ?T) ] |- wf_env ?D ?E ] => apply (@wf_env_push_inv x D E T)
| [ _: context [ wf_env ?D (?E & ?x *~ ?T) ] |- ?E |=?D ?T ] => apply (@wf_env_push_inv x D E T)
| [ _: context [ wf_env ?D (?E & '?x) ] |- wf_env ?D ?E ] => apply (@wf_env_push_s_inv x D E)
end.

Lemma wf_typ_typing_respects_wf_env :
  (forall E D T, E |=D T -> wf_env D E)
  /\ (forall E D e T, E |=D e ~: T -> wf_env D E).
Proof.
  apply wf_typ_typing_ind; intros; auto.
  - pick_fresh y. apply~ (@wf_env_push_inv y).
  - pick_fresh y. apply~ (@wf_env_push_s_inv y).
  - pick_fresh y. apply~ (@wf_env_push_inv y).
  - pick_fresh f; pick_fresh y. apply~ (@wf_env_push_inv f). apply~ (@wf_env_push_inv y).
  - pick_fresh y. apply~ (@wf_env_push_s_inv y).
Qed.

Lemma wf_typ_arrow_inv : forall E D T1 T2,
    E |=D (trm_arrow T1 T2) ->
    exists L, forall x, x \notin L -> (E & x *~ T1) |=D (T2 $^ x).
Proof.
  introv Wf. gen_eq T': (trm_arrow T1 T2). move: T1 T2.
  induction Wf; introv Eq; inverts Eq.
  - exists L. exact H.
  - forwards~ K: IHWf2. destruct K as [L].
    exists_fresh. apply* wf_typ_gen_weaken.
  - forwards~ K: IHWf. destruct K as [L].
    exists_fresh. apply* wf_typ_gen_sweaken.
Qed.

Lemma wf_typ_forall_inv : forall E D T,
    E |=D (trm_forall T) ->
    exists L, include_fv_s_orth L D /\ forall x, x \notin L -> (E & 'x) |=D (T %^ x).
Proof.
  introv Wf. gen_eq T': (trm_forall T). move: T.
  induction Wf; introv Eq; inverts Eq.
  - exists L. auto.
  - forwards~ K: IHWf2. destruct K as [L [K1 K2]].
    exists_fresh. split.
    + auto.
    + intros. apply* wf_typ_gen_weaken.
  - forwards~ K: IHWf. destruct K as [L [K1 K2]].
    exists_fresh. split.
    + auto.
    + intros. apply* wf_typ_gen_sweaken.
Qed.

Lemma wf_typ_refine_inv : forall E D T e,
    E |=D (trm_refine T e) ->
    exists L, forall x, x \notin L -> (E & x *~ T) |=D (e $^ x) ~: trm_bool.
Proof.
  introv Wf. gen_eq T': (trm_refine T e). move: T e.
  induction Wf; introv Eq; inverts Eq.
  - exists L. auto.
  - forwards~ K: IHWf2. destruct K as [L].
    exists_fresh. apply* typing_gen_weaken.
  - forwards~ K: IHWf. destruct K as [L].
    exists_fresh. apply* typing_gen_sweaken.
Qed.

Lemma subst_orthant_fresh : forall x u L D,
    include_fv_s_orth L D ->
    x \notin L ->
    D = (Im D (subst_s_axis x u)).
Proof.
  intros. inverts H. apply Extensionality_Ensembles. unfold Same_set. split.
  - unfold Included. intros. apply Im_intro with (x := x0); first easy.
    destruct x0; simpl; rewrite* subst_s_fresh.
  - unfold Included. intros. inverts H.
    destruct x1; simpl; rewrite* subst_s_fresh.
Qed.

Lemma wf_typ_nat_gen : forall E D,
    wf_env D E ->
    orthant D ->
    E |=D trm_nat.
Proof. induction 1; auto. Qed.

Lemma wf_typ_bool_gen : forall E D,
    wf_env D E ->
    orthant D ->
    E |=D trm_bool.
Proof. induction 1; auto. Qed.

Lemma typing_respect_wf_typ : forall E D e T,
    E |=D e ~: T ->
    E |=D T.
Proof.
  induction 1; auto.
  - apply wf_typ_arrow_inv in IHtyping1. destruct IHtyping1 as [L].
    pick_fresh y. rewrite* (@subst_intro y). apply* wf_typ_subst.
  - apply wf_typ_forall_inv in IHtyping. destruct IHtyping as [L [K1 K2]].
    pick_fresh y. rewrite* (@subst_s_intro y). rewrite* (@subst_orthant_fresh y b L D).
    apply* wf_typ_subst_s.
  - pick_fresh f; pick_fresh y. forwards~ K: (H0 f y).
    apply (@wf_env_push_inv f). apply (@wf_env_push_inv y _ _ T1). apply* (proj1 wf_typ_typing_respects_wf_env).
  - apply* wf_typ_nat_gen. apply* (proj1 wf_typ_typing_respects_wf_env E D _ IHtyping).
  - apply* wf_typ_bool_gen. apply~ (proj1 wf_typ_typing_respects_wf_env E D _ IHtyping).
  - apply wf_typ_refine_inv in IHtyping. destruct IHtyping as [L].
    pick_fresh y. apply* (@wf_env_push_inv y). apply* (proj2 wf_typ_typing_respects_wf_env).
Qed.

Lemma wf_typ_orthant_push_l : forall E D T b,
    bits b ->
    E |=D T ->
    E |=(D ⊌ inl b) T.
Proof. intros. apply* wf_typ_typing_orth_weaken. apply* extend_orthant_l. Qed.

Lemma wf_typ_orthant_push_r : forall E D T b,
    bits b ->
    E |=D T ->
    E |=(D ⊌ inr b) T.
Proof. intros. apply* wf_typ_typing_orth_weaken. apply* extend_orthant_r. Qed.

Lemma typing_orthant_push_l : forall E D e T b,
    bits b ->
    E |=D e ~: T ->
    E |=(D ⊌ inl b) e ~: T.
Proof. intros. apply* wf_typ_typing_orth_weaken. apply* extend_orthant_l. Qed.

Lemma typing_orthant_push_r : forall E D e T b,
    bits b ->
    E |=D e ~: T ->
    E |=(D ⊌ inr b) e ~: T.
Proof. intros. apply* wf_typ_typing_orth_weaken. apply* extend_orthant_r. Qed.

Local Hint Resolve wf_typ_orthant_push_l wf_typ_orthant_push_r typing_orthant_push_l typing_orthant_push_r.

Fixpoint unref_typ (t : trm) {struct t} : trm :=
  match t with
  | trm_fvar_s x => t
  | trm_on => t
  | trm_off => t
  | trm_concat t1 t2 => t
  | trm_nat => t
  | trm_bool => t
  | trm_arrow t1 t2 => t
  | trm_refine t1 t2 => unref_typ t1
  | trm_forall t1 => t
  | trm_bvar i => t
  | trm_fvar x => t
  | trm_app t1 t2 => t
  | trm_fix t1 t2 => t
  | trm_zero => t
  | trm_succ t1 => t
  | trm_pred t1 => t
  | trm_iszero t1 => t
  | trm_true => t
  | trm_false => t
  | trm_if t1 t2 t3 => t
  | trm_sapp t1 t2 => t
  | trm_sabs t1 => t
  | trm_choice t1 t2 t3 => t
  | trm_cast t1 t2 t3 => t
  | trm_waiting t1 t2 => t
  | trm_active t1 t2 t3 => t
  | trm_blame => t
  end.

Lemma unref_regular : forall T,
    typ T ->
    typ (unref_typ T).
Proof.
  induction 1; simpl; auto.
Qed.

Hint Resolve unref_regular.

Lemma unref_subst : forall T x e,
    exp e ->
    unref_typ ([x ~> e]T) = [x ~> e](unref_typ T).
Proof.
  induction T; simpl; intros; auto.
  - calc_open_subst~. induction H; simpl; auto.
Qed.

Lemma unref_equiv : forall D T1 T2,
    T1 ==D T2 ->
    unref_typ T1 ==D unref_typ T2.
Proof. induction 1; simpl; auto. Qed.

Lemma typing_succ_inv : forall D e T,
    empty |=D (trm_succ e) ~: T ->
    unref_typ T = trm_nat /\ empty |=D e ~: trm_nat.
Proof.
  introv Typing. gen_eq E: (@empty (option trm)). gen_eq t: (trm_succ e). move: e.
  induction Typing; introv Eq EqE; inverts Eq; inverts EqE; try solve [false* empty_push_inv]; simpls*.
  - forwards* : IHTyping. destruct H1 as [K1 K2]. apply unref_equiv in H0. rewrite K1 in H0. inverts* H0.
Qed.

Lemma typing_fix_inv : forall D T1 T2 e T,
    empty |=D (trm_fix (trm_arrow T1 T2) e) ~: T ->
    unref_typ T ==D trm_arrow T1 T2 /\
    exists L, (forall f x, fresh L 2 (x::f::nil) -> (f *~ trm_arrow T1 T2 & x *~ T1) |=D e $^ x $^ f ~: T2 $^ x).
Proof.
  introv Typing. gen_eq E: (@empty (option trm)). gen_eq t: (trm_fix (trm_arrow T1 T2) e). move: T1 T2 e.
  induction Typing; introv Eq EqE; inverts Eq; inverts EqE; try solve [false* empty_push_inv]; autos*.
  - split.
    + simpl. assert (empty |=D trm_arrow T0 T3).
      apply typing_respect_wf_typ with (e := trm_fix (trm_arrow T0 T3) e0). autos*.
      apply* equiv_typ_refl.
    + exists L. intros. forwards* : (H f x). rew_env_concat in H2. apply H2.
  - forwards* K: IHTyping. destruct K as [K1 [L K2]]. split.
    + apply equiv_typ_trans with (T2 := unref_typ T1); auto. apply equiv_typ_sym. apply~ unref_equiv.
    + exists L. auto.
Qed.

Lemma typing_sabs_inv : forall D e T,
    empty |=D (trm_sabs e) ~: T ->
    exists L T', unref_typ T ==D trm_forall T' /\ include_fv_s_orth L D /\ (forall x, x \notin L -> 'x |=D e %^ x ~: T' %^ x).
Proof.
  introv Typing. gen_eq E: (@empty (option trm)). gen_eq t: (trm_sabs e). move: e.
  induction Typing; introv Eq EqE; inverts Eq; inverts EqE; try solve [false* empty_push_inv]; autos*.
  - exists L T. splits 3.
    + simpl.
      assert (empty |=D trm_forall T).
      eapply typing_respect_wf_typ. apply* typing_sabs.
      apply* equiv_typ_refl.
    + auto.
    + intros. specialize (H x). rew_env_concat in H. auto.
  - forwards~ K: IHTyping. destruct K as [L [T']]. exists L T'.
    intuition. apply equiv_typ_trans with (T2 := unref_typ T1); auto.
    apply equiv_typ_sym. apply~ unref_equiv.
Qed.

Lemma typing_choice_inv : forall D e1 e2 b T,
    empty |=D (trm_choice e1 e2 b) ~: T ->
    empty |=(D ⊌ inl b) e1 ~: T /\ empty |=(D ⊌ inr b) e2 ~: T /\ wf_bits empty b.
Proof.
  introv Typing. gen_eq E: (@empty (option trm)). gen_eq t: (trm_choice e1 e2 b). move: e1 e2 b.
  induction Typing; introv Eq EqE; inverts Eq; inverts EqE; try solve [false* empty_push_inv].
  - autos*.
  - forwards~ K: IHTyping. destruct K as [K1 [K2 K3]].
    splits 3.
    + apply* typing_conv. apply* equiv_typ_orth_weaken. auto with sets.
    + apply* typing_conv. apply* equiv_typ_orth_weaken. auto with sets.
    + auto.
  - inversion H. inversion H0.
  - inversion H. inversion H2.
Qed.

Hint Extern 1 (empty |=(?D ⊌ inl ?b) ?e ~: ?T) =>
match goal with
| [ H: empty |=D (trm_choice e _ b) ~: T |- _ ] => simple apply (proj31 (typing_choice_inv H))
end.

Hint Extern 1 (empty |=(?D ⊌ inr ?b) ?e ~: ?T) =>
match goal with
| [ H: empty |=D (trm_choice _ e b) ~: T |- _ ] => simple apply (proj32 (typing_choice_inv H))
end.

Hint Extern 1 (wf_bits empty ?b) =>
match goal with
| [ H: empty |=_ (trm_choice _ _ b) ~: _ |- _ ] => simple apply (proj33 (typing_choice_inv H))
end.

Lemma nvalue_normal : forall nv e D,
    nvalue nv ->
    not (nv -->D e).
Proof.
  intros. move: e. induction H; intros; intro.
  - inverts H.
  - inverts H0; inverts* H.
Qed.

Lemma value_normal : forall v e D,
    value v ->
    not (v -->D e).
Proof.
  intros. move: e. inverts H; intros; intro; try solve [inverts H].
  - apply* nvalue_normal.
Qed.

Lemma orthant_push_l : forall D b,
    orthant D ->
    bits b ->
    orthant (D ⊌ inl b).
Proof.
  intros. inverts H. apply orthant_intro; intros.
  - apply Add_inv in H. destruct~ H. inverts~ H.
  - apply Add_inv in H. destruct~ H. discriminate.
Qed.

Lemma orthant_push_r : forall D b,
    orthant D ->
    bits b ->
    orthant (D ⊌ inr b).
Proof.
  intros. inverts H. apply orthant_intro; intros.
  - apply Add_inv in H. destruct~ H. discriminate.
  - apply Add_inv in H. destruct~ H. inverts~ H.
Qed.

Lemma preservation : forall D e e' T,
    empty |=D e ~: T ->
    e -->D e' ->
    empty |=D e' ~: T.
Proof.
  introv Typing. gen_eq E: (@empty (option trm)). move: e'.
  induction Typing; introv Eq Step;
    try solve [false* empty_push_inv | false* value_normal];
        subst; try solve [inverts* Step].
  - assert (K: empty |=D T2 ^^ e2) by apply* typing_respect_wf_typ.
    inverts* Step.              (* typing_app *)
    + apply typing_fix_inv in Typing1. unpack. simpl in H. inverts H.
      pick_fresh y. pick_fresh f from (fv ([y ~> e2](T3 $^ y))). forwards* : (H0 f y).
      assert (empty |=D trm_arrow T0 T3).
      { eapply wf_env_push_inv. rew_env_concat. eapply wf_env_push_inv.
        apply* (proj2 wf_typ_typing_respects_wf_env). }
      assert (empty |=D T0).
      { apply wf_typ_arrow_inv in H3. destruct H3. pick_fresh z. forwards~ : (H3 z).
        eapply wf_env_push_inv. apply* (proj1 wf_typ_typing_respects_wf_env). }
      rewrite* (@subst_intro y). rewrite* (@subst_intro2 y f).
      apply typing_conv with (T1 := [y ~> e2](T3 $^ y)).
      * rewrite* <- (@subst_fresh f (trm_fix (trm_arrow T0 T3) e) ([y ~> e2](T3 $^ y))).
        -- apply typing_subst with (T' := trm_arrow T0 T3).
           ++ apply typing_subst with (T' := T0).
              ** rew_env_concat. apply H.
              ** apply* typing_weaken.
           ++ apply_fresh typing_fix. rew_env_concat. auto.
      * rewrite* <- subst_intro.
      * apply* equiv_subst. apply* equiv_typ_sym.
    + apply* typing_choice.
      * apply* typing_app. auto.
      * apply* typing_app. auto.
    + apply* typing_choice.
      * apply* typing_conv.
        apply typing_respect_wf_typ in Typing1. apply wf_typ_arrow_inv in Typing1. destruct Typing1 as [L].
        pick_fresh y.
        assert (typ (T2 $^ y)) by forwards~ : (H y).
        assert (orthant (D ⊌ inl b)) by apply* orthant_push_l.
        rewrite* (@subst_intro y). rewrite* (@subst_intro y (trm_choice e0 e3 b)).
        apply* equiv_typ_sym. apply* equiv_typ_spec.
        -- intros. notin_simpl.
           ++ apply* typing_closed_exp.
           ++ apply* typing_closed_s_exp.
        -- apply* step_choice_l. auto with sets.
      * apply* typing_conv.
        apply typing_respect_wf_typ in Typing1. apply wf_typ_arrow_inv in Typing1. destruct Typing1 as [L].
        pick_fresh y.
        assert (typ (T2 $^ y)) by forwards~ : (H y).
        assert (orthant (D ⊌ inr b)) by apply* orthant_push_r.
        rewrite* (@subst_intro y). rewrite* (@subst_intro y (trm_choice e0 e3 b)).
        apply* equiv_typ_sym. apply* equiv_typ_spec.
        -- intros. notin_simpl.
           ++ apply* typing_closed_exp.
           ++ apply* typing_closed_s_exp.
        -- apply* step_choice_r. auto with sets.
    + apply* typing_conv.
      apply typing_respect_wf_typ in Typing1. apply wf_typ_arrow_inv in Typing1. destruct Typing1 as [L].
      pick_fresh y.
      assert (typ (T2 $^ y)) by forwards~ : (H y).
      rewrite* (@subst_intro y). rewrite* (@subst_intro y e2).
      apply* equiv_typ_sym. apply* equiv_typ_spec. intro. notin_simpl.
      * apply* typing_closed_exp.
      * apply* typing_closed_s_exp.
  - assert (K: empty |=D T ^^ b) by apply* typing_respect_wf_typ.
    inverts* Step.              (* typing_sapp *)
    + apply typing_sabs_inv in Typing. destruct Typing as [L [T' [K1 [K2 K3]]]].
      simpl in K1. inverts K1. pick_fresh y. rewrite* (@subst_s_intro y). rewrite* (@subst_s_intro y b T).
      apply typing_conv with (T1 := subst_s y b (T' %^ y)).
      * rewrite* (@subst_orthant_fresh y b L D). apply* typing_subst_s. rew_env_concat. auto.
      * rewrite* <- subst_s_intro.
      * apply* equiv_subst_s. apply* equiv_typ_sym.
  - inverts* Step. apply typing_succ_inv in Typing. intuition.
  - assert (K: empty |=D T) by apply* typing_respect_wf_typ.
    inverts* Step.              (* typing_if *)
  - inverts* Step.              (* typing_choice *)
    + replace D with (D ⊌ inl b); last by apply* Non_disjoint_union. apply* Typing1. 
    + replace D with (D ⊌ inr b); last by apply* Non_disjoint_union. apply* Typing2.
  - inverts* Step.              (* typing_cast *)
    + destruct (wf_typ_arrow_inv H) as [L].
      assert (empty |=D trm_arrow T11 T12) by apply* typing_respect_wf_typ.
      destruct (wf_typ_arrow_inv H2) as [L'].
      assert (empty |=D T21).
      { pick_fresh z. forwards* : (H1 z).
        apply* wf_env_push_inv. apply* (proj1 wf_typ_typing_respects_wf_env). }
      assert (empty |=D T11).
      { pick_fresh z. forwards* : (H3 z).
        apply* wf_env_push_inv. apply* (proj1 wf_typ_typing_respects_wf_env). }
      inverts keep H0.
      inverts keep H5.
      inverts keep H6.
      assert (typ ({0 ~> trm_fvar x}T22)).
      { fold (T22 $^ x). pick_fresh z. rewrite* (@subst_intro z). }
      apply_fresh typing_fix.
      apply* typing_gen_weaken.
      assert (K1: forall t x y, exp (t $^ x) -> t $^ x $^ y = t $^ x).
      { unfold open2. intros. rewrite~ <- open_exp. }
      rewrite* K1; first last.
      { apply* delayed_cast_regular. }
      calc_open_subst.
      replace ({0 ~> trm_fvar y}T22) with ({0 ~> (trm_cast (trm_fvar y) ({0 ~> trm_fvar y} T21) ({0 ~> trm_fvar y} T11))}({0 ~> trm_fvar y}T22)); first last.
      { rewrite* <- open_typ. }
      apply* typing_app.
      * rewrite* close_var_fresh. rewrite* <- (@open_typ 0 (trm_fvar y) T11).
        rewrite* <- subst_as_close_open_typ.
        fold (T22 $^ x). rewrite* subst_open. calc_open_subst. rewrite* subst_fresh.
        fold (T22 $^ y).
        apply_fresh typing_fix.
        apply* typing_gen_weaken.
        { apply_fresh wf_typ_arrow. apply* wf_typ_weaken. unfold open. rewrite* <- open_typ. }
        rewrite* K1; first last.
        { apply* (@delayed_cast_regular_in x y y0 e T11 T12 T21 T22). }
        rewrite* <- subst_as_close_open_exp.
        rewrite* <- (@subst_as_close_open_typ x (trm_fvar y) ({0 ~> trm_fvar x}T22)).
        fold (T22 $^ x). rewrite* subst_open. rewrite* subst_fresh. rewrite* subst_fresh.
        calc_open_subst.
        apply* typing_cast.
        -- apply* typing_app.
           rewrite* <- open_exp.
           apply* typing_weaken. apply* typing_weaken.
           replace (trm_arrow T11 ({2 ~> trm_fvar y}(close_var_rec 2 x T12))) with ({1 ~> trm_fvar y}(close_var_rec 1 x (trm_arrow T11 T12))).
           rewrite* <- subst_as_close_open_typ. rewrite* subst_fresh. simpl. notin_solve.
           simpl. rewrite* <- subst_as_close_open_typ. rewrite* subst_fresh.
        -- fold (T22 $^ y). rewrite* <- open_typ.
        -- rewrite* close_var_open_ind.
           rewrite* <- subst_as_close_open_typ.
           rewrite* subst_fresh.
           ++ fold (T12 $^ y0). fold (T22 $^ y). rewrite* <- open_typ.
              pick_fresh z. rewrite* (@subst_intro z (trm_fvar y0)). rewrite* (@subst_intro z (trm_fvar y)).
              apply* consist_typ_subst.
           ++ apply* fv_open. simpl. notin_solve.
      * rewrite* <- open_typ. rewrite* <- open_typ. apply* typing_cast. apply* consist_typ_sym.
    + apply wf_typ_forall_inv in H. destruct H as [L [K1 K2]].
      inverts H0. apply_fresh* typing_sabs. calc_open_subst. apply* typing_cast.
      apply* typing_sapp. rewrite* <- open_exp.
    + apply* typing_cast. apply consist_typ_refine_inv in H0. assumption.
    + apply* typing_waiting. apply* typing_cast.
      * apply wf_typ_refine_inv in H. destruct H as [L].
        pick_fresh y. forwards~ : (H y). apply* (@wf_env_push_inv y). apply* (proj2 wf_typ_typing_respects_wf_env).
      * inverts* H0. specialize (H10 T0 e1). congruence.
  - inverts* Step.              (* typing_waiting *)
    + apply* typing_active.
      * apply wf_typ_refine_inv in H. destruct H as [L].
        pick_fresh y. rewrite* (@subst_intro y). replace trm_bool with ([y ~> e1]trm_bool); last by simpl.
        apply* typing_subst.
      * apply* mred_refl. apply* open_refine_body_exp.
  - inverts* Step.              (* typing_active *)
    + apply* typing_choice.
      * apply* typing_active. apply* (@mred_trans (D ⊌ inl b) (e2 ^^ v) (trm_choice e0 e3 b)).
        -- apply* (@mstep_orth_weaken D).
           ++ apply* orthant_push_l.
           ++ auto with sets.
        -- apply* mred_step. apply* step_choice_l.
           ++ apply* orthant_push_l.
           ++ auto with sets.
      * apply* typing_active. apply* (@mred_trans (D ⊌ inr b) (e2 ^^ v) (trm_choice e0 e3 b)).
        -- apply* (@mstep_orth_weaken D).
           ++ apply* orthant_push_r.
           ++ auto with sets.
        -- apply* mred_step. apply* step_choice_r.
           ++ apply* orthant_push_r.
           ++ auto with sets.
    + apply* typing_active. apply* mred_trans. apply* mred_step.
Qed.