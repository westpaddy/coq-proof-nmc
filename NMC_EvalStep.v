Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity NMC_Step NMC_Eval.

Ltac resolve_eval_idem :=
  match goal with
  | [ H: ?R1 \\?D ?R2 |- _ ] =>
    let K1 := fresh in
    let K2 := fresh in
    assert (K1: R1 \\D R1) by apply~ eval_result_idem;
    lets K2 : (eval_deterministic H K1);
    clear K1 H;
    inverts K2
  end.

Ltac solve_branching :=
  match goal with
  | [ H1: trm_choice _ _ _ \\?D trm_blame |- trm_choice _ _ _ \\?D _ ] =>
    solve [inverts H1; [apply* eval_choice_l | apply* eval_choice_r]]
  | [ H1: trm_choice _ _ _ \\?D ?nv, H2: nvalue ?nv |- trm_choice _ _ _ \\?D _ ] =>
    solve [inverts H1; [apply* eval_choice_l | apply* eval_choice_r | false* (nvalue_choice_inv H2)]]
  | [ H1: trm_choice _ _ _ \\?D ?v, H2: value ?v |- trm_choice _ _ _ \\?D _ ] =>
    solve [inverts H1; [apply* eval_choice_l | apply* eval_choice_r | false* (value_choice_inv H2)]]
  end.

Ltac solve_ctx_step :=
  match goal with
  | [ H1: ?e -->?D ?e', H2: forall R, ?e \\?D R -> ?e' \\?D R |- _ \\?D _ ] =>
    solve [ forwards* : H2]
  end.

Ltac forwardsChoices :=
  match goal with
  | [ H1: trm_choice _ _ ?b \\?D trm_choice _ _ ?b', H2: trm_choice _ _ ?b' \\?D ?R |- trm_choice _ _ ?b \\?D ?R ] =>
    inverts H1; [apply* eval_choice_l | apply* eval_choice_r | inverts H2; try contradiction]
  end.

Hint Extern 1 (trm_app ?e1 ?e2 \\?D trm_blame) =>
match goal with
| [ H0: e1 \\D ?v, H1: value ?v, H2: e2 \\D trm_blame |- _ ] =>
  apply (eval_blame_app_r H1 H0 H2)
end.

Lemma result_regular : forall D R,
    result D R ->
    orthant D /\ exp R.
Proof. induction 1; intuition. Qed.

Hint Extern 1 =>
match goal with
| [ H: result ?D ?R |- orthant ?D ] => simple apply (proj1 (result_regular H))
| [ H: result ?D ?R |- exp ?R ] => simple apply (proj2 (result_regular H))
end.

Lemma eval_red_left : forall D e1 e2 R,
    e1 -->D e2 ->
    e1 \\D R ->
    e2 \\D R.
Proof.
  introv Step. move: R.
  induction Step; introv Eval1; inverts Eval1;
    try contradiction;
    try solve [repeat resolve_eval_idem;
               try_discriminate2;
               auto using eval_result_idem];
    try solve_branching.
  - resolve_eval_idem. rewrite (delayed_cast_close_var H3 H14). apply~ eval_fix.
    assert (K1: forall t x y, exp (t $^ x) -> exp (t $^ x $^ y)).
    { unfold open, open2. intros. rewrite~ <- open_exp. }
    inverts H0. inverts H1.
    apply_fresh~ exp_fix. apply K1. apply* delayed_cast_regular.
  - resolve_eval_idem. apply~ eval_sabs.
    inverts H0. inverts H1. apply_fresh exp_sabs.
    unfold open in *. repeat (simpl; case_nat).
    apply~ exp_cast. rewrite~ <- open_exp.
  - inverts H7.
    + apply* eval_choice_l.
    + apply* eval_choice_r.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H15).
      inverts H13; resolve_eval_idem; eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H16).
      inverts H17; resolve_eval_idem; eauto.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H17).
      inverts H15; repeat resolve_eval_idem.
      * apply* eval_app.
      * inversion H2. inversion H5.
      * assert (v \\(D ⊌ inl b0) v) by apply~ eval_value_idem. eauto.
      * inversion H2. inversion H5.
      * assert (v \\(D ⊌ inl b0) v) by apply~ eval_value_idem. eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H18).
      inverts H19; repeat resolve_eval_idem.
      * apply* eval_app.
      * inversion H2. inversion H5.
      * assert (v \\(D ⊌ inr b0) v) by apply~ eval_value_idem. eauto.
      * inversion H2. inversion H5.
      * assert (v \\(D ⊌ inr b0) v) by apply~ eval_value_idem. eauto.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H14).
      inverts H12; repeat resolve_eval_idem; eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H15).
      inverts H16; repeat resolve_eval_idem; eauto.
  - inverts* H5.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H14).
      inverts H12; repeat resolve_eval_idem; eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H15).
      inverts H16; repeat resolve_eval_idem; eauto.
  - inverts* H4.
  - inverts H5.
    + apply* eval_choice_l.
    + apply* eval_choice_r.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H14).
      inverts H12; repeat resolve_eval_idem; eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H15).
      inverts H16; repeat resolve_eval_idem; eauto.
  - inverts* H11.
  - inverts* H11.
  - inverts H10.
    + apply~ eval_choice_l. apply* eval_dist_if.
    + apply~ eval_choice_r. apply* eval_dist_if.
    + inverts H11; try contradiction.
      assert (result (D ⊌ inl b0) R1) by apply (eval_result H16).
      assert (result (D ⊌ inr b0) R2) by apply (eval_result H17).
      inverts H14; inverts H18; repeat resolve_eval_idem; eauto.
  - inverts H7.
    + apply* eval_choice_l.
    + apply* eval_choice_r.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H15).
      inverts H13; repeat resolve_eval_idem; eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H16).
      inverts H17; repeat resolve_eval_idem; eauto.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H16).
      inverts H14; repeat resolve_eval_idem; eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H17).
      inverts H18; repeat resolve_eval_idem; eauto.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H15).
      inverts H13; repeat resolve_eval_idem; eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H16).
      inverts H17; repeat resolve_eval_idem; eauto.
  - inverts H13.
    + apply* eval_choice_l.
    + apply* eval_choice_r.
  - inverts H13.
    + apply* eval_choice_l.
    + apply* eval_choice_r.
  - forwardsChoices. apply~ eval_choice.
    + assert (result (D ⊌ inl b0) R1) by apply (eval_result H16).
      inverts H14; repeat resolve_eval_idem; eauto.
    + assert (result (D ⊌ inr b0) R2) by apply (eval_result H17).
      inverts H18; repeat resolve_eval_idem; eauto.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - forwards~ : IHStep. apply H6. apply* eval_dist_if.
  - solve_ctx_step.
  - solve_ctx_step.
  - apply~ eval_choice_l.
    replace (D ⊌ inl b) with D in *; last by rewrite Non_disjoint_union.
    auto.
  - apply~ eval_choice_r.
    replace (D ⊌ inr b) with D in *; last by rewrite Non_disjoint_union.
    auto.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
  - solve_ctx_step.
Qed.

Hint Resolve eval_nvalue_idem eval_value_idem eval_result_idem.

Ltac simplify_eval_idem :=
  match goal with
  | [ H: ?e \\?D ?R |- _ ] =>
    let K1 := fresh in
    let K2 := fresh in
    assert (K1: e \\D e) by auto;
    lets K2 : (eval_deterministic K1 H);
    try (subst; clear K1 H)
  end.

Inductive normal_orth D :=
| normal_orth_intro :
    (forall b, inl b ∈ D -> inr b ∉ D) ->
    (forall b, inr b ∈ D -> inl b ∉ D) ->
    normal_orth D.

Section EvalCtx.

Lemma result_end : forall D e R,
    e \\D R ->
    R \\D R.
Proof. intros. apply eval_result_idem. apply* eval_result. Qed.

Hint Resolve result_end.

Hint Extern 1 =>
match goal with
| [ H1: ?R' \\?D ?R, H2: ?e \\?D ?R' |- _ ] =>
  let K1 := fresh in
  let K2 := fresh in
  assert (K1: R' \\D R') by apply (result_end H2);
    lets K2 : (eval_deterministic K1 H1);
    try (subst; clear K1)
end.

Lemma eval_ctx_app_l : forall D e1 e2 R1 R,
    trm_app R1 e2 \\D R ->
    e1 \\D R1 ->
    trm_app e1 e2 \\D R.
Proof. inversion 1; intros; eauto. Qed.

Lemma eval_ctx_app_r : forall D v1 e2 R2 R,
    trm_app v1 R2 \\D R ->
    value v1 ->
    e2 \\D R2 ->
    trm_app v1 e2 \\D R.
Proof.
  inversion 1; intros.
  - apply~ eval_app. apply H2. apply H3. auto. auto.
  - subst. assert (v1 \\D v1) by eauto. lets : (eval_deterministic H0 H2). subst. inversion H6. inversion H1.
  - subst. simplify_eval_idem. assert (v1 \\D v1) by eauto. apply* eval_dist_app_r.
  - subst. assert (v1 \\D v1) by eauto. lets : (eval_deterministic H0 H5). subst. inversion H6. inversion H1.
  - subst. auto.
Qed.

Lemma eval_ctx_succ : forall D e' R' R,
    trm_succ R' \\D R ->
    e' \\D R' ->
    trm_succ e' \\D R.
Proof. inversion 1; eauto. Qed.

Lemma eval_ctx_pred : forall D e' R' R,
    trm_pred R' \\D R ->
    e' \\D R' ->
    trm_pred e' \\D R.
Proof. inversion 1; eauto. Qed.

Lemma eval_ctx_iszero : forall D e' R' R,
    trm_iszero R' \\D R ->
    e' \\D R' ->
    trm_iszero e' \\D R.
Proof. inversion 1; eauto. Qed.

Lemma eval_ctx_if : forall D e1 e2 e3 R1 R,
    trm_if R1 e2 e3 \\D R ->
    e1 \\D R1 ->
    trm_if e1 e2 e3 \\D R.
Proof. inversion 1; eauto. Qed.

Lemma eval_ctx_sapp : forall D e1 b R1 R,
    trm_sapp R1 b \\D R ->
    e1 \\D R1 ->
    trm_sapp e1 b \\D R.
Proof. inversion 1; eauto. Qed.

Lemma eval_ctx_cast : forall D e1 T1 T2 R1 R,
    trm_cast R1 T1 T2 \\D R ->
    e1 \\D R1 ->
    trm_cast e1 T1 T2 \\D R.
Proof. inversion 1; eauto. Qed.

Lemma eval_ctx_waiting : forall D e1 T R1 R,
    trm_waiting R1 T \\D R ->
    e1 \\D R1 ->
    trm_waiting e1 T \\D R.
Proof. inversion 1; eauto. Qed.

Lemma eval_ctx_active : forall D e' v T R' R,
    trm_active R' v T \\D R ->
    e' \\D R' ->
    trm_active e' v T \\D R.
Proof. inversion 1; eauto. Qed.

Lemma eval_ctx_app_l_inv : forall D e1 e2 R,
    trm_app e1 e2 \\D R ->
    exists R', e1 \\D R' /\ trm_app R' e2 \\D R.
Proof. inversion 1; jauto. Qed.

Lemma eval_ctx_app_r_inv : forall D v e R,
    value v ->
    trm_app v e \\D R ->
    exists R', e \\D R' /\ trm_app v R' \\D R.
Proof.
  intros. assert (v \\D v) by apply* eval_value_idem. inversion H0; repeat simplify_eval_idem.
  - eexists. split; first eauto. apply* eval_app.
  - inversion H. inversion H1.
  - eexists. split; first eauto. apply* eval_dist_app_r.
  - inversion H. inversion H1.
  - eexists. split; first eauto. apply* eval_blame_app_r.
Qed.

Lemma eval_ctx_succ_inv : forall D e R,
    trm_succ e \\D R ->
    exists R', e \\D R' /\ trm_succ R' \\D R.
Proof. inversion 1; jauto. Qed.

Lemma eval_ctx_pred_inv : forall D e R,
    trm_pred e \\D R ->
    exists R', e \\D R' /\ trm_pred R' \\D R.
Proof. inversion 1; jauto. Qed.

Lemma eval_ctx_iszero_inv : forall D e R,
    trm_iszero e \\D R ->
    exists R', e \\D R' /\ trm_iszero R' \\D R.
Proof. inversion 1; jauto. Qed.

Lemma eval_ctx_if_inv : forall D e1 e2 e3 R,
    trm_if e1 e2 e3 \\D R ->
    exists R', e1 \\D R' /\ trm_if R' e2 e3 \\D R.
Proof.
  inversion 1.
  - eexists. split; first eauto. apply* eval_if_t.
  - eexists. split; first eauto. apply* eval_if_f.
  - eexists. split; first eauto. apply* eval_dist_if.
  - eexists. split; first eauto. apply* eval_blame_if.
Qed.

Lemma eval_ctx_sapp_inv : forall D e b R,
    trm_sapp e b \\D R ->
    exists R', e \\D R' /\ trm_sapp R' b \\D R.
Proof. inversion 1; jauto. Qed.

Lemma eval_ctx_cast_inv : forall D e T1 T2 R,
    trm_cast e T1 T2 \\D R ->
    exists R', e \\D R' /\ trm_cast R' T1 T2 \\D R.
Proof. inversion 1; jauto. Qed.

Lemma eval_ctx_waiting_inv : forall D e T R,
    trm_waiting e T \\D R ->
    exists R', e \\D R' /\ trm_waiting R' T \\D R.
Proof. inversion 1; jauto. Qed.

Lemma eval_ctx_active_inv : forall D e v T R,
    trm_active e v T \\D R ->
    exists R', e \\D R' /\ trm_active R' v T \\D R.
Proof. inversion 1; jauto. Qed.

End EvalCtx.

Lemma normal_add_orhtant_l : forall D b,
    normal_orth D ->
    inl b ∉ D ->
    inr b ∉ D ->
    normal_orth (D ⊌ inl b).
Proof.
  intros. inverts H. apply normal_orth_intro.
  * intros. apply Add_inv in H. destruct H.
    -- intro. apply Add_inv in H4. destruct H4.
       ++ forwards* : H2.
       ++ inverts H4.
    -- inverts H. intro. apply Add_inv in H. destruct H.
       ++ forwards* : H3.
       ++ inverts H.
  * intros. apply Add_inv in H. destruct H.
    -- intro. apply Add_inv in H4. destruct H4.
       ++ forwards* : H3.
       ++ inverts H4. contradiction.
    -- inverts H.
Qed.

Lemma normal_add_orhtant_r : forall D b,
    normal_orth D ->
    inl b ∉ D ->
    inr b ∉ D ->
    normal_orth (D ⊌ inr b).
Proof.
  intros. inverts H. apply normal_orth_intro.
  * intros. apply Add_inv in H. destruct H.
    -- intro. apply Add_inv in H4. destruct H4.
       ++ forwards* : H2.
       ++ inverts H4. contradiction.
    -- inverts H.
  * intros. apply Add_inv in H. destruct H.
    -- intro. apply Add_inv in H4. destruct H4.
       ++ forwards* : H3.
       ++ inverts H4.
    -- inverts H. intro. apply Add_inv in H. destruct H.
       ++ forwards* : H2.
       ++ inverts H.
Qed.

Lemma eval_red_right : forall D e1 e2 R,
    e1 -->D e2 ->
    normal_orth D ->
    e2 \\D R ->
    e1 \\D R.
Proof.
  introv Step. move: R.
  induction Step; introv Normal Eval1; auto;
    try solve [simplify_eval_idem; auto].
  - autos*.
  - autos*.
  - autos*.
  - simplify_eval_idem. eauto.
  - inverts Normal. apply~ eval_choice_l.
  - inverts Normal. apply~ eval_choice_r.
  - assert (exp (trm_fix (trm_arrow T21 T22)
            (trm_app
               (close_var x
                  (trm_fix (trm_arrow T11 (T22 $^ x)) (trm_cast (trm_app v (trm_bvar 0)) T12 (T22 $^ x))))
               (trm_cast (trm_bvar 0) T21 T11)))).
    { assert (K1: forall t x y, exp (t $^ x) -> exp (t $^ x $^ y)).
      { unfold open, open2. intros. rewrite~ <- open_exp. }
      inverts H0. inverts H1.
      apply_fresh~ exp_fix. apply K1. apply* delayed_cast_regular. }
    simplify_eval_idem. auto.
  - assert (exp (trm_sabs (trm_cast (trm_sapp v (trm_bvar 0)) T1 T2))).
    { inverts H0. inverts H1. apply_fresh exp_sabs.
      unfold open in *. repeat (simpl; case_nat).
      apply~ exp_cast. rewrite~ <- open_exp. }
    simplify_eval_idem. auto.
  - autos*.
  - autos*.
  - inverts Eval1.
    + apply eval_ctx_app_l_inv in H12. unpack. apply* eval_ctx_app_l.
    + apply eval_ctx_app_l_inv in H12. unpack. apply* eval_ctx_app_l.
    + apply eval_ctx_app_l_inv in H11. apply eval_ctx_app_l_inv in H12. unpack.
      apply~ eval_dist_app_l. apply* eval_choice. apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_app_r_inv in H12; auto. unpack. apply* eval_ctx_app_r.
    + apply eval_ctx_app_r_inv in H12; auto. unpack. apply* eval_ctx_app_r.
    + apply eval_ctx_app_r_inv in H11; auto. apply eval_ctx_app_r_inv in H12; auto. unpack.
      apply~ eval_dist_app_r. auto. apply* eval_choice. apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_succ_inv in H11. unpack. apply* eval_ctx_succ.
    + apply eval_ctx_succ_inv in H11. unpack. apply* eval_ctx_succ.
    + apply eval_ctx_succ_inv in H10. apply eval_ctx_succ_inv in H11. unpack.
      apply~ eval_dist_succ; apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_pred_inv in H11. unpack. apply* eval_ctx_pred.
    + apply eval_ctx_pred_inv in H11. unpack. apply* eval_ctx_pred.
    + apply eval_ctx_pred_inv in H10. apply eval_ctx_pred_inv in H11. unpack.
      apply~ eval_dist_pred; apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_iszero_inv in H11. unpack. apply* eval_ctx_iszero.
    + apply eval_ctx_iszero_inv in H11. unpack. apply* eval_ctx_iszero.
    + apply eval_ctx_iszero_inv in H10. apply eval_ctx_iszero_inv in H11. unpack.
      apply~ eval_dist_iszero; apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_if_inv in H13. unpack. apply* eval_ctx_if.
    + apply eval_ctx_if_inv in H13. unpack. apply* eval_ctx_if.
    + apply eval_ctx_if_inv in H12. apply eval_ctx_if_inv in H13. unpack.
      apply~ eval_dist_if; apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_sapp_inv in H12. unpack. apply* eval_ctx_sapp.
    + apply eval_ctx_sapp_inv in H12. unpack. apply* eval_ctx_sapp.
    + apply eval_ctx_sapp_inv in H11. apply eval_ctx_sapp_inv in H12. unpack.
      apply~ eval_dist_sapp; apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_cast_inv in H13. unpack. apply* eval_ctx_cast.
    + apply eval_ctx_cast_inv in H13. unpack. apply* eval_ctx_cast.
    + apply eval_ctx_cast_inv in H12. apply eval_ctx_cast_inv in H13. unpack.
      apply~ eval_dist_cast; apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_waiting_inv in H12. unpack. apply* eval_ctx_waiting.
    + apply eval_ctx_waiting_inv in H12. unpack. apply* eval_ctx_waiting.
    + apply eval_ctx_waiting_inv in H11. apply eval_ctx_waiting_inv in H12. unpack.
      apply~ eval_dist_waiting; apply* eval_choice.
  - inverts Eval1.
    + apply eval_ctx_active_inv in H13. unpack. apply* eval_ctx_active.
    + apply eval_ctx_active_inv in H13. unpack. apply* eval_ctx_active.
    + apply eval_ctx_active_inv in H12. apply eval_ctx_active_inv in H13. unpack.
      apply~ eval_dist_active; apply* eval_choice.
  - apply eval_ctx_app_l_inv in Eval1. unpack. apply* eval_ctx_app_l.
  - apply eval_ctx_app_r_inv in Eval1; auto. unpack. apply* eval_ctx_app_r.
  - apply eval_ctx_succ_inv in Eval1. unpack. apply* eval_ctx_succ.
  - apply eval_ctx_pred_inv in Eval1. unpack. apply* eval_ctx_pred.
  - apply eval_ctx_iszero_inv in Eval1. unpack. apply* eval_ctx_iszero.
  - apply eval_ctx_if_inv in Eval1. unpack. apply* eval_ctx_if.
  - apply eval_ctx_sapp_inv in Eval1. unpack. apply* eval_ctx_sapp.
  - inverts Eval1.
    + apply~ eval_choice_l. assert (D ⊌ inl b = D) by rewrite~ Non_disjoint_union. rewrite H0 in IHStep. auto.
    + apply~ eval_choice_r.
    + apply~ eval_choice. apply* IHStep. apply~ normal_add_orhtant_l.
  - inverts Eval1.
    + apply~ eval_choice_l.
    + apply~ eval_choice_r. assert (D ⊌ inr b = D) by rewrite~ Non_disjoint_union. rewrite H0 in IHStep. auto.
    + apply~ eval_choice. apply* IHStep. apply~ normal_add_orhtant_r.
  - apply eval_ctx_cast_inv in Eval1. unpack. apply* eval_ctx_cast.
  - apply eval_ctx_waiting_inv in Eval1. unpack. apply* eval_ctx_waiting.
  - apply eval_ctx_active_inv in Eval1. unpack. apply* eval_ctx_active.
  - simplify_eval_idem. apply* eval_blame_app_r.
Qed.

Lemma mstep_head_ind : forall (P : trm -> orth -> trm -> Prop),
    (forall D e, orthant D -> exp e -> P e D e) ->
    (forall D e1 e2 e3, e1 -->D e2 -> e2 -->*D e3 -> P e2 D e3 -> P e1 D e3) ->
    forall e1 D e2, e1 -->*D e2 -> P e1 D e2.
Proof.
  intros. move: P H H0. induction H1.
  - auto.
  - intros. apply* H1. apply* mred_refl.
  - move: e3 H1_0 IHmstep2. eapply IHmstep1.
    + intros. apply IHmstep2.
      * intros. eapply H1. auto. auto.
      * intros. eapply H2. eapply H3. eapply H4. eapply H5.
    + intros. eapply H3. apply H. eapply mred_trans. apply H0. apply H1_0.
      eapply H1. auto. auto. auto. auto.
Qed.

Lemma mstep_eval : forall D e R,
    normal_orth D ->
    result D R ->
    e -->*D R ->
    e \\D R.
Proof.
  induction 3 using mstep_head_ind.
  - by apply eval_result_idem.
  - apply eval_red_right with (e2 := e2).
    + assumption.
    + assumption.
    + apply~ IHmstep.
Qed.

Section EvalMstep.

Local Hint Resolve mred_refl mred_step.

Local Hint Extern 1 (?e1 -->*?D ?e3) =>
match goal with
| [ H1: e1 -->*D ?e2, H2: ?e2 -->*D e3 |- _ ] => apply (mred_trans H1 H2)
end.

Lemma mstep_ctx_app_l : forall D e1 e1' e2,
    exp e2 ->
    e1 -->*D e1' ->
    trm_app e1 e2 -->*D trm_app e1' e2.
Proof. induction 2; auto. Qed.

Lemma mstep_ctx_app_r : forall D v e e',
    value v ->
    e -->*D e' ->
    trm_app v e -->*D trm_app v e'.
Proof. induction 2; auto. Qed.

Lemma mstep_ctx_succ : forall D e e',
    e -->*D e' ->
    trm_succ e -->*D trm_succ e'.
Proof. induction 1; auto. Qed.

Lemma mstep_ctx_pred : forall D e e',
    e -->*D e' ->
    trm_pred e -->*D trm_pred e'.
Proof. induction 1; auto. Qed.

Lemma mstep_ctx_iszero : forall D e e',
    e -->*D e' ->
    trm_iszero e -->*D trm_iszero e'.
Proof. induction 1; auto. Qed.

Lemma mstep_ctx_if : forall D e e' e2 e3,
    exp e2 ->
    exp e3 ->
    e -->*D e' ->
    trm_if e e2 e3 -->*D trm_if e' e2 e3.
Proof. induction 3; auto. Qed.

Lemma mstep_ctx_sapp : forall D e e' b,
    bits b ->
    e -->*D e' ->
    trm_sapp e b -->*D trm_sapp e' b.
Proof. induction 2; auto. Qed.

Lemma mstep_ctx_cast : forall D e e' T1 T2,
    typ T1 ->
    typ T2 ->
    e -->*D e' ->
    trm_cast e T1 T2 -->*D trm_cast e' T1 T2.
Proof. induction 3; auto. Qed.

Lemma mstep_ctx_waiting : forall D e e' T e2,
    typ (trm_refine T e2) ->
    e -->*D e' ->
    trm_waiting e (trm_refine T e2) -->*D trm_waiting e' (trm_refine T e2).
Proof. induction 2; auto. Qed.

Lemma mstep_ctx_active : forall D e e' v T e2,
    value v ->
    typ (trm_refine T e2) ->
    e -->*D e' ->
    trm_active e v (trm_refine T e2) -->*D trm_active e' v (trm_refine T e2).
Proof. induction 3; auto. Qed.

Local Hint Resolve mstep_ctx_app_l mstep_ctx_app_r mstep_ctx_succ mstep_ctx_pred mstep_ctx_iszero
      mstep_ctx_if mstep_ctx_sapp mstep_ctx_cast mstep_ctx_waiting mstep_ctx_active.

Local Ltac ctx_red :=
  match goal with
  | [ IH: ?e -->*?D ?e' |- ?t -->*?D ?R ] =>
    match t with
    | context C [e] =>
      assert (exp e) by auto; assert (exp e') by auto; assert (orthant D) by auto;
      let t' := context C[e'] in
      assert (t -->*D t'); [by auto | clear IH]
    end
  end.

Local Ltac squeeze :=
  match goal with
  | [ H: ?e -->*?D ?e' |- ?e -->*?D _ ] =>
    first [apply H | apply mred_trans with (e2 := e'); [exact H|]]
  | [ H: ?e' -->*?D ?e |- _ -->*?D ?e ] =>
    first [apply H | apply mred_trans with (e2 := e'); [|exact H]]
  end.

Local Ltac solve_red :=
  solve [repeat (try ctx_red; progress squeeze); auto].

Local Ltac pickLC :=
  match goal with
  | [ H: ?e \\?D ?e' |- _ ] =>
    assert (exp e) by apply (eval_regular_pre H);
    assert (exp e') by apply (eval_regular_post H);
    assert (orthant D) by apply (eval_regular_orthant H);
    clear H
  end.

Local Hint Extern 1 (bits ?b) =>
match goal with
| [ H: exp ?t |- _ ] => match t with context [b] => inversion H end
| [ H: typ ?t |- _ ] => match t with context [b] => inversion H end
end.

Local Hint Extern 1 (typ ?T) =>
match goal with
| [ H: exp ?t |- _ ] => match t with context [T] => inversion H end
| [ H: typ ?t |- _ ] => match t with context [T] => inversion H end
end.

Local Hint Extern 1 (exp ?e) =>
match goal with
| [ H: exp ?t |- _ ] => match t with context [e] => inversion H end
| [ H: typ ?t |- _ ] => match t with context [e] => inversion H end
end.

Local Hint Extern 1 (value ?v) =>
match goal with
| [ H: exp ?t |- _ ] => match t with context [v] => inversion H end
| [ H: typ ?t |- _ ] => match t with context [v] => inversion H end
end.

Lemma eval_mstep : forall D e R,
    e \\D R ->
    e -->*D R.
Proof.
  induction 1; repeat pickLC; try solve_red.
  - apply mred_trans with (e2 := trm_choice R1 e2 b).
    + gen_eq D': (D ⊌ inl b). induction IHeval1; intros; subst; auto.
      apply* mred_trans.
    + apply mred_trans with (e2 := trm_choice R1 R2 b).
      * gen_eq D': (D ⊌ inr b). induction IHeval2; intros; subst; auto.
        apply* mred_trans.
      * auto.
Qed.

End EvalMstep.