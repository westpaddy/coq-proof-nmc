Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity NMC_Step NMC_Eval NMC_EvalStep NMC_Coterm NMC_EquivType NMC_Preservation.

Lemma csr_coterm : forall D e1 e2 x v,
    normal_orth D ->
    value v ->
    csr_exp e1 D e2 ->
    ([x ~> v]e1 -->*D trm_true -> [x ~> v]e2 -->*D trm_true) /\
    ([x ~> v]e2 -->*D trm_true -> [x ~> v]e1 -->*D trm_true).
Proof.
  intros. induction H1; try solve [intuition].
  - assert (forall x, x \notin fv e1).
    { intros. specialize (H3 x1). auto. }
    assert (forall x, x \notin fv e2).
    { intros. apply* step_keep_fv. }
    pick_fresh z.
    rewrite~ (@pull_out_closed_subst z).
    rewrite~ (@pull_out_closed_subst z x v x0 e2 e).
    split; intro.
    + apply coterm_true_left with (e1 := e1); auto.
    + apply coterm_true_right with (e1 := e2); auto.
Qed.

Fixpoint refinements (t : trm) {struct t} : list trm :=
  match t with
  | trm_refine t1 t2 => (t2 :: refinements t1)
  | _ => nil
  end.

Inductive value_satisfies_refinements : orth -> trm -> list trm -> Prop :=
| vsr_empty : forall D v,
    orthant D ->
    value v ->
    value_satisfies_refinements D v nil
| vsr_cons : forall D v t l,
    value_satisfies_refinements D v l ->
    (t ^^ v) -->*D trm_true ->
    value_satisfies_refinements D v (t :: l).

Lemma equiv_typ_coterm : forall D T1 T2 v,
    normal_orth D ->
    value v ->
    T1 ==D T2 ->
    value_satisfies_refinements D v (refinements T1) ->
    value_satisfies_refinements D v (refinements T2).
Proof.
  induction 3; simpl; auto.
  - intros. inverts H3. apply~ vsr_cons.
    pick_fresh z. forwards~ : (H2 z). rewrite~ (@subst_intro z).
    apply~ csr_coterm. apply csr_exp_sym. apply H3. rewrite~ <- (@subst_intro z).
Qed.

Local Ltac falsify_value H :=
  let K := fresh in
  inversion H as [? K | fail | fail | fail | fail]; inversion K.

Lemma value_inversion : forall E D v T,
    E |=D v ~: T ->
    empty = E ->
    normal_orth D ->
    value v ->
    value_satisfies_refinements D v (refinements T).
Proof.
  induction 1; introv Eq Norm Val; simpl;
    try solve [apply~ vsr_empty | false (empty_push_inv Eq) | falsify_value Val].
  - pick_fresh f; pick_fresh y. forwards~ : (H f y).
    apply~ vsr_empty.
  - pick_fresh y. forwards~ : (H y).
    apply~ vsr_empty.
  - apply~ equiv_typ_coterm. auto.
  - forwards~ : IHtyping. inverts~ H1.
  - forwards~ : IHtyping. apply~ vsr_cons.
Qed.

Inductive result_satisfies_refinement : orth -> trm -> trm -> Prop :=
| rsr_value : forall D v T e,
    value v ->
    typ (trm_refine T e) ->
    e ^^ v -->*D trm_true ->
    result_satisfies_refinement D v (trm_refine T e)
| rsr_blame : forall D T e,
    orthant D ->
    typ (trm_refine T e) ->
    result_satisfies_refinement D trm_blame (trm_refine T e)
| rsr_choice : forall D R1 R2 b T e,
    result_satisfies_refinement (D ⊌ inl b) R1 (trm_refine T e) ->
    result_satisfies_refinement (D ⊌ inr b) R2 (trm_refine T e) ->
    result_satisfies_refinement D (trm_choice R1 R2 b) (trm_refine T e).

Lemma contract_satisfy : forall D R T e,
    empty |=D R ~: trm_refine T e ->
    result D R ->
    normal_orth D ->
    result_satisfies_refinement D R (trm_refine T e).
Proof.
  introv Typing Result Norm. induction Result.
  - apply~ rsr_value. forwards~ : (value_inversion Typing).
    inverts* H1.
  - apply~ rsr_blame.
  - apply typing_choice_inv in Typing. unpack.
    apply~ rsr_choice.
    + apply~ IHResult1. apply~ normal_add_orhtant_l.
    + apply~ IHResult2. apply~ normal_add_orhtant_r.
Qed.

Lemma canonical_nat : forall E D v T,
    E |=D v ~: T ->
    empty = E ->
    value v ->
    unref_typ T = trm_nat ->
    nvalue v.
Proof.
  induction 1; introv Eq Val Typ; try false (empty_push_inv Eq);
    try solve [falsify_value Val]; simpl in Typ; try discriminate; auto.
  - inverts* Val.
  - apply unref_equiv in H1. rewrite Typ in H1. inverts* H1.
Qed.

Lemma canonical_bool : forall E D v T,
    E |=D v ~: T ->
    empty = E ->
    value v ->
    unref_typ T = trm_bool ->
    v = trm_true \/ v = trm_false.
Proof.
  induction 1; introv Eq Val Typ; try false (empty_push_inv Eq);
    try solve [falsify_value Val]; simpl in Typ; try discriminate; auto.
  - apply unref_equiv in H1. rewrite Typ in H1. inverts* H1.
Qed.

Lemma canonical_arrow : forall E D v T T1 T2,
    E |=D v ~: T ->
    empty = E ->
    value v ->
    unref_typ T = trm_arrow T1 T2 ->
    exists T1' T2' e', v = trm_fix (trm_arrow T1' T2') e'.
Proof.
  introv Typing. move: T1 T2.
  induction Typing; introv Eq Val Typ; try false (empty_push_inv Eq);
    try solve [falsify_value Val]; simpl in Typ; try discriminate; eauto.
  - apply unref_equiv in H0. rewrite Typ in H0. inverts* H0.
Qed.

Lemma canonical_forall : forall E D v T T',
    E |=D v ~: T ->
    empty = E ->
    value v ->
    unref_typ T = trm_forall T' ->
    exists e', v = trm_sabs e'.
Proof.
  introv Typing. move: T'.
  induction Typing; introv Eq Val Typ; try false (empty_push_inv Eq);
    try solve [falsify_value Val]; simpl in Typ; try discriminate; eauto.
  - apply unref_equiv in H0. rewrite Typ in H0. inverts* H0.
Qed.

Local Ltac forwardsIH :=
  match goal with
  | [ H1: empty = empty -> normal_orth ?D -> _, H2: normal_orth ?D |- _ ] =>
    forwards : H1; [reflexivity | apply H2 | clear H1]
  end.

Local Ltac crushIH H :=
  destruct H as [H | [? H]]; [
    inverts H; [| right; eauto; eauto 6 | right; eauto; eauto 6] |
    right; eauto
  ].

(* Inductive normal_orth' : orth -> Prop := *)
(* | normal_orth_intro' : forall D, *)
(*     (forall b, (inl b ∉ D /\ inr b ∉ D) \/ *)
(*           (inl b ∈ D /\ inr b ∉ D) \/ *)
(*           (inl b ∉ D /\ inr b ∈ D)) -> *)
(*     normal_orth' D. *)

(* Lemma normal_orth_spec : forall D, *)
(*     normal_orth' D -> *)
(*     (forall b, inl b ∈ D -> inr b ∉ D) /\ (forall b, inr b ∈ D -> inl b ∉ D). *)
(* Proof. *)
(*   destruct 1. split; intros. *)
(*   - specialize (H b) as [H|[H|H]]; unpack. *)
(*     + contradiction. *)
(*     + assumption. *)
(*     + contradiction. *)
(*   - specialize (H b) as [H|[H|H]]; unpack. *)
(*     + contradiction. *)
(*     + contradiction. *)
(*     + assumption. *)
(* Qed. *)

Lemma normal_orth_cases : forall D b,
    normal_orth D ->
    (inl b ∉ D /\ inr b ∉ D) \/
    (inl b ∈ D /\ inr b ∉ D) \/
    (inl b ∉ D /\ inr b ∈ D).
Proof.
  destruct 1. specialize (n b). specialize (n0 b).
  destruct (classic (inl b ∈ D)).
  - auto with sets.
  - destruct (classic (inr b ∈ D)).
    + auto with sets.
    + auto with sets.
Qed.

Lemma progress_result : forall E D e T,
    E |=D e ~: T ->
    empty = E ->
    normal_orth D ->
    result D e \/ exists e', e -->D e'.
Proof.
  induction 1; introv Eq Norm; try false (empty_push_inv Eq); auto;
    subst; repeat forwardsIH.
  - crushIH H1. crushIH H2.
    apply canonical_arrow with (T1 := T1) (T2 := T2) in H; auto.
    unpack. subst. eauto.
  - crushIH H1. apply canonical_forall with (T' := T) in H; auto.
    unpack. subst. right. eauto.
  - left. apply result_value.
    + pick_fresh f; pick_fresh y. forwards~ : (H f y).
    + apply value_fix. apply_fresh exp_fix.
      * pick_fresh f; pick_fresh y. forwards~ : (H f y).
        apply (@environment_push_inv empty f).
        eapply environment_push_inv.
        eapply wf_typ_typing_regular.
        apply H1.
      * forwards~ : (H f y).
  - crushIH H0. apply canonical_nat in H; eauto.
  - crushIH H0. right. forwards~ : (canonical_nat H). inverts H0.
    + forwards~ : (value_inversion H). inverts H0.
      unfold open in H8. simpl in H8. case_nat.
      apply mstep_eval in H8; auto.
      assert (trm_if (trm_iszero trm_zero) trm_false trm_true \\D trm_false) by auto.
      lets : (eval_deterministic H8 H0). discriminate.
    + eauto.
  - crushIH H0. apply canonical_nat in H; auto.
    inverts* H.
  - crushIH H2. apply canonical_bool in H; auto.
    destruct H; subst; eauto.
  - left. apply result_value.
    + pick_fresh y. forwards~ : (H y).
    + apply value_sabs. apply_fresh exp_sabs.
      forwards~ : (H y).
  - destruct (@normal_orth_cases D b Norm) as [K|[K|K]]; destruct K.
    + assert (normal_orth (D ⊌ inl b)) by apply* normal_add_orhtant_l.
      assert (normal_orth (D ⊌ inr b)) by apply* normal_add_orhtant_r.
      forwards~ : IHtyping1. destruct H7.
      * forwards~ : IHtyping2. destruct H8.
        -- auto.
        -- right. destruct H8. eauto.
      * right. destruct H7. eauto.
    + right. eauto.
    + right. eauto.
  - crushIH H2. inverts H1; right; eauto.
    + pick_fresh y. eexists. apply step_c_arrow with (x := y); auto.
    + assert (typ T1) by auto.
      destruct H1; eexists; try (apply~ step_c_precheck; congruence).
      apply~ step_c_forget.
  - crushIH H1. eauto.
  - crushIH H5. apply canonical_bool in H0; auto. destruct H0; subst; eauto.
Qed.
