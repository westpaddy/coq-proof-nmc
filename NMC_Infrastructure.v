Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Export NMC_Definition.

Fixpoint fv_a (t : trm) {struct t} : vars :=
  match t with
  | trm_fvar_s x => \{x}
  | trm_on => \{}
  | trm_off => \{}
  | trm_concat t1 t2 => (fv_a t1) \u (fv_a t2)
  | trm_nat => \{}
  | trm_bool => \{}
  | trm_arrow t1 t2 => (fv_a t1) \u (fv_a t2)
  | trm_refine t1 t2 => (fv_a t1) \u (fv_a t2)
  | trm_forall t1 => (fv_a t1)
  | trm_bvar i => \{}
  | trm_fvar x => \{x}
  | trm_app t1 t2 => (fv_a t1) \u (fv_a t2)
  | trm_fix t1 t2 => (fv_a t1) \u (fv_a t2)
  | trm_zero => \{}
  | trm_succ t1 => (fv_a t1)
  | trm_pred t1 => (fv_a t1)
  | trm_iszero t1 => (fv_a t1)
  | trm_true => \{}
  | trm_false => \{}
  | trm_if t1 t2 t3 => (fv_a t1) \u (fv_a t2) \u (fv_a t3)
  | trm_sapp t1 t2 => (fv_a t1) \u (fv_a t2)
  | trm_sabs t1 => (fv_a t1)
  | trm_choice t1 t2 t3 => (fv_a t1) \u (fv_a t2) \u (fv_a t3)
  | trm_cast t1 t2 t3 => (fv_a t1) \u (fv_a t2) \u (fv_a t3)
  | trm_waiting t1 t2 => (fv_a t1) \u (fv_a t2)
  | trm_active t1 t2 t3 => (fv_a t1) \u (fv_a t2) \u (fv_a t3)
  | trm_blame => \{}
  end.

Ltac gather_vars :=
  let A := gather_vars_with (fun x : var => \{x}) in
  let B := gather_vars_with (fun x : vars => x) in
  let C := gather_vars_with (fun x : trm => fv x \u fv_s x \u fv_a x) in
  let D := gather_vars_with (fun x : env => dom x) in
  constr:(A \u B \u C \u D).

Tactic Notation "pick_fresh" ident(x) :=
  let L := gather_vars in (pick_fresh_gen L x).

Tactic Notation "pick_fresh" ident(x) "from" constr(E) :=
  let L := gather_vars in (pick_fresh_gen (L \u E) x).

Tactic Notation "apply_fresh" constr(H) :=
  apply_fresh_base_simple H gather_vars;
  try match goal with
      | |- forall _, _ \notin _ -> _ =>
        let y := fresh "y" in
        let Fr := fresh "Fr" in
        intros y Fr
      | |- forall _ _, fresh _ 2 _ -> _ =>
        let f := fresh "f" in
        let y := fresh "y" in
        let Fr := fresh "Fr" in
        intros f y Fr
      end.

Tactic Notation "apply_fresh" "~" constr(H) :=
  apply_fresh H; autos.

Tactic Notation "apply_fresh" "*" constr(H) :=
  apply_fresh H; autos*.

Ltac exists_fresh :=
  let L := gather_vars in
  exists L; try match goal with
           | |- forall _, _ \notin _ -> _ =>
             let y := fresh "y" in
             let Fr := fresh "Fr" in
             intros y Fr
           | |- forall _ _, fresh _ 2 _ -> _ =>
             let f := fresh "f" in
             let y := fresh "y" in
             let Fr := fresh "Fr" in
             intros f y Fr
           end.

Tactic Notation "calc_open_subst" :=
  unfold open, open2; repeat (simpl; first [case_nat | case_var]); simpl.

Tactic Notation "calc_open_subst" "~" :=
  calc_open_subst; auto_tilde.

Ltac destruct_eq_push H :=
  let K := fresh in
  rew_env_concat in H; destruct (eq_push_inv H) as [?H [K ?H]];
  try injection K as ?K; subst; clear H.

Ltac falsify_eq_push H :=
  let K := fresh in
  rew_env_concat in H; destruct (eq_push_inv H) as [?H [K ?H]]; inversion K.

Scheme Minimality for typ Sort Prop with Minimality for exp Sort Prop with Minimality for value Sort Prop.
Combined Scheme typ_exp_value_ind from typ_ind, exp_ind, value_ind.

Scheme Minimality for wf_typ Sort Prop with Minimality for typing Sort Prop.
Combined Scheme wf_typ_typing_ind from wf_typ_ind, typing_ind.

Hint Constructors bits nvalue typ exp value environment step equiv_typ consist_typ wf_bits wf_typ typing.

Remove Hints typ_arrow typ_refine typ_forall exp_fix exp_sabs
       equiv_typ_arrow equiv_typ_refine equiv_typ_forall
       consist_typ_arrow consist_typ_forall
       wf_typ_arrow wf_typ_refine wf_typ_forall typing_fix typing_sabs.

Hint Extern 1 (typ (trm_arrow _ _)) => apply_fresh typ_arrow.
Hint Extern 1 (typ (trm_refine _ _)) => apply_fresh typ_refine.
Hint Extern 1 (typ (trm_forall _)) => apply_fresh typ_forall.
Hint Extern 1 (exp (trm_fix _ _)) => apply_fresh exp_fix.
Hint Extern 1 (exp (trm_sabs _)) => apply_fresh exp_sabs.
Hint Extern 1 (equiv_typ (trm_arrow _ _) _ (trm_arrow _ _)) => apply_fresh equiv_typ_arrow.
Hint Extern 1 (equiv_typ (trm_refine _ _) _ (trm_refine _ _)) => apply_fresh equiv_typ_refine.
Hint Extern 1 (equiv_typ (trm_forall _) _ (trm_forall _)) => apply_fresh equiv_typ_forall.
Hint Extern 1 ((trm_arrow _ _) ~= (trm_arrow _ _)) => apply_fresh consist_typ_arrow.
Hint Extern 1 ((trm_forall _) ~= (trm_forall _)) => apply_fresh consist_typ_forall.
Hint Extern 1 (_ |= _ (trm_arrow _ _)) => apply_fresh wf_typ_arrow.
Hint Extern 1 (_ |= _ (trm_refine _ _)) => apply_fresh wf_typ_refine.
Hint Extern 1 (_ |= _ (trm_forall _)) => apply_fresh wf_typ_forall.
Hint Extern 1 (_ |= _ (trm_fix _ _) ~: _) => apply_fresh typing_fix.
Hint Extern 1 (_ |= _ (trm_sabs _) ~: _) => apply_fresh typing_sabs.

(** * locally-closed terms are irrelevant to open *)

Lemma open_rec_term_core : forall j v t i u,
    i <> j ->
    {j ~> v}t = {i ~> u}({j ~> v}t) ->
    t = {i ~> u}t.
Proof.
  move=> j v t. move: j v.
  induction t; simpl; introv Neq Eq; inverts Eq; f_equal; eauto.
  - case_nat~. case_nat~.
Qed.

Lemma open_bits : forall k u b,
    bits b ->
    b = {k ~> u}b.
Proof. induction 1; simpl; f_equal; auto. Qed.

Lemma open_nvalue : forall k u nv,
    nvalue nv ->
    nv = {k ~> u}nv.
Proof. induction 1; simpl; f_equal; auto. Qed.

Lemma open_typ_exp_value :
  (forall T, typ T -> forall k u, T = {k ~> u}T) /\
  (forall e, exp e -> forall k u, e = {k ~> u}e) /\
  (forall v, value v -> forall k u, v = {k ~> u}v).
Proof.
  apply typ_exp_value_ind; try easy; simpl; intros; f_equal; auto using open_bits, open_nvalue.
  - pick_fresh y. apply* (@open_rec_term_core 0 (trm_fvar y)).
  - pick_fresh y. apply* (@open_rec_term_core 0 (trm_fvar y)).
  - pick_fresh y. apply* (@open_rec_term_core 0 (trm_fvar_s y)).
  - pick_fresh f; pick_fresh y.
    apply* (@open_rec_term_core 0 (trm_fvar y)).
    apply* (@open_rec_term_core 1 (trm_fvar f)).
  - pick_fresh y. apply* (@open_rec_term_core 0 (trm_fvar_s y)).
Qed.

Lemma open_typ : forall k u T, typ T -> T = {k ~> u}T.
Proof. intros. by apply (proj31 open_typ_exp_value). Qed.

Lemma open_exp : forall k u e, exp e -> e = {k ~> u}e.
Proof. intros. by apply (proj32 open_typ_exp_value). Qed.

Lemma open_value : forall k u v, value v -> v = {k ~> u}v.
Proof. intros. by apply (proj33 open_typ_exp_value). Qed.

Lemma open_var_inj : forall k x t1 t2,
    x \notin (fv t1) ->
    x \notin (fv t2) ->
    ({k ~> trm_fvar x}t1 = {k ~> trm_fvar x}t2) -> (t1 = t2).
Proof.
  intros k x t1. move: k.
  induction t1; destruct t2; simpl; intros; inversion H1;
    try solve [ fequals*
              | do 2 try case_nat; inversions~ H1; try notin_false ].
Qed.

Lemma open_var_inj_s : forall k x t1 t2,
    x \notin (fv_s t1) ->
    x \notin (fv_s t2) ->
    ({k ~> trm_fvar_s x}t1 = {k ~> trm_fvar_s x}t2) -> (t1 = t2).
Proof.
  intros k x t1. move: k.
  induction t1; destruct t2; simpl; intros; inversion H1;
    try solve [ fequals*
              | do 2 try case_nat; inversions~ H1; try notin_false ].
Qed.

(** * substitutions disribute over open *)
Lemma subst_open_gen : forall x u t1 t2 k,
    exp u ->
    [x ~> u]({k ~> t2}t1) = {k ~> [x ~> u]t2}([x ~> u]t1).
Proof.
  induction t1; simpl; intros; f_equal; auto.
  - case_nat~.
  - case_var~. rewrite~ <- open_exp.
Qed.

Lemma subst_open : forall x u t1 t2,
    exp u ->
    [x ~> u](t1 ^^ t2) = ([x ~> u]t1) ^^ ([x ~> u]t2).
Proof. unfold open. intros. now apply subst_open_gen. Qed.

Lemma subst_open_gen2 : forall x u t1 t2 t3 i j,
    exp u ->
    [x ~> u]({i ~> t3}({j ~> t2}t1)) = {i ~> [x ~> u]t3}({j ~> [x ~> u]t2}([x ~> u]t1)).
Proof.
  induction t1; simpl; intros; f_equal; auto.
  - case_nat~.
    + rewrite~ subst_open_gen.
    + simpl. case_nat~.
  - case_var~. rewrite~ <- (@open_exp j). rewrite~ <- open_exp.
Qed.

Lemma subst_open2 : forall x u t1 t2 t3,
    exp u ->
    [x ~> u](t1 ^^ t2 ^^ t3) = ([x ~> u]t1) ^^ ([x ~> u]t2) ^^ ([x ~> u]t3).
Proof. unfold open2. intros. now apply subst_open_gen2. Qed.

Lemma subst_s_open_gen : forall x u t1 t2 k,
    bits u ->
    subst_s x u ({k ~> t2}t1) = {k ~> subst_s x u t2}(subst_s x u t1).
Proof.
  induction t1; simpl; intros; f_equal; auto.
  - calc_open_subst~.
  - calc_open_subst~. rewrite~ <- open_bits.
Qed.

Lemma subst_s_open : forall x u t1 t2,
    bits u ->
    subst_s x u (t1 ^^ t2) = (subst_s x u t1) ^^ (subst_s x u t2).
Proof. unfold open. intros. now apply subst_s_open_gen. Qed.

Lemma subst_s_open2 : forall x u t1 t2 t3,
    bits u->
    subst_s x u (t1 ^^ t2 ^^ t3) = (subst_s x u t1) ^^ (subst_s x u t2) ^^ (subst_s x u t3).
Proof.
  introv. unfold open2. move: 0.
  induction t1; simpl; intros; f_equal; auto.
  - calc_open_subst~. rewrite~ subst_s_open_gen.
  - calc_open_subst~. rewrite~ <- (@open_bits n). rewrite~ <- open_bits.
Qed.

(** ** derived lemmas for fresh variable opening *)

Lemma subst_open_var : forall x y u t,
    x <> y ->
    exp u ->
    ([x ~> u]t) $^ y = [x ~> u](t $^ y).
Proof. intros. rewrite~ subst_open. calc_open_subst~. Qed.

Lemma subst_open_var2 : forall x y z u t,
    x <> y ->
    x <> z ->
    exp u ->
    ([x ~> u]t) $^ y $^ z = [x ~> u](t $^ y $^ z).
Proof. intros. rewrite~ subst_open2. calc_open_subst~. Qed.

Lemma subst_open_var_s : forall x y u t,
    exp u ->
    ([x ~> u]t) %^ y = [x ~> u](t %^ y).
Proof. intros. rewrite~ subst_open. Qed.

Lemma subst_s_open_var : forall x y u t,
    bits u ->
    (subst_s x u t) $^ y = subst_s x u (t $^ y).
Proof. intros. rewrite~ subst_s_open. Qed.

Lemma subst_s_open_var_s : forall x y u t,
    x <> y ->
    bits u ->
    (subst_s x u t) %^ y = subst_s x u (t %^ y).
Proof. intros. rewrite~ subst_s_open. calc_open_subst~. Qed.

Lemma subst_s_open_var2 : forall x y z u t,
    bits u ->
    (subst_s x u t) $^ y $^ z = subst_s x u (t $^ y $^ z).
Proof. intros. rewrite~ subst_s_open2. Qed.

Lemma subst_fresh : forall x u t,
    x \notin (fv t) ->
    [x ~> u]t = t.
Proof.
  induction t; simpl; intros; f_equal; auto.
  - case_var~.
Qed.

Lemma subst_s_fresh : forall x u t,
    x \notin (fv_s t) ->
    subst_s x u t = t.
Proof.
  induction t; simpl; intros; f_equal; auto.
  - calc_open_subst~.
Qed.

Lemma subst_intro_gen : forall k x u t,
    x \notin (fv t) ->
    exp u ->
    {k ~> u}t = [x ~> u]({k ~> trm_fvar x}t).
Proof. intros. rewrite~ subst_open_gen. calc_open_subst. rewrite~ subst_fresh. Qed.

Lemma subst_intro : forall x u t,
    x \notin (fv t) ->
    exp u ->
    t ^^ u = [x ~> u](t $^ x).
Proof. intros. unfold open. now apply subst_intro_gen. Qed.

Lemma subst_intro2 : forall x y u1 u2 t,
    x \notin (fv t) ->
    y \notin \{x} \u (fv t) \u (fv u1) ->
    exp u1 ->
    exp u2 ->
    t ^^ u1 ^^ u2 = [y ~> u2]([x ~> u1](t $^ x $^ y)).
Proof.
  intros. rewrite~ subst_open2. calc_open_subst. rewrite~ (@subst_fresh x).
  rewrite~ subst_open_gen2. calc_open_subst. do 2 rewrite~ subst_fresh.
Qed.

Lemma subst_s_intro : forall x u t,
    x \notin (fv_s t) ->
    bits u ->
    t ^^ u = subst_s x u (t %^ x).
Proof. intros. rewrite~ subst_s_open. calc_open_subst. rewrite~ subst_s_fresh. Qed.

Lemma close_var_fresh : forall x t k,
    x \notin fv t ->
    close_var_rec k x t = t.
Proof.
  induction t; simpl; intros; f_equal*.
  - case_var~.
Qed.

Lemma close_var_open_ind : forall x y z t i j,
    i <> j ->
    y <> x ->
    {i ~> trm_fvar y} ({j ~> trm_fvar z} (close_var_rec j x t) )
    = {j ~> trm_fvar z} (close_var_rec j x ({i ~> trm_fvar y}t) ).
Proof.
  induction t; simpl; intros; try solve [f_equal; auto].
  - calc_open_subst~.
  - calc_open_subst~.
Qed.

Lemma close_var_open_ind_s : forall x y z t i j,
    i <> j ->                     (* y <> x is not required *)
    {i ~> trm_fvar_s y} ({j ~> trm_fvar z} (close_var_rec j x t) )
    = {j ~> trm_fvar z} (close_var_rec j x ({i ~> trm_fvar_s y}t) ).
Proof.
  induction t; simpl; intros; try solve [f_equal; auto].
  - calc_open_subst~.
  - calc_open_subst~.
Qed.

Lemma close_var_open_bits : forall k x b,
    bits b ->
    {k ~> trm_fvar x}(close_var_rec k x b) = b.
Proof. introv. move: k. induction 1; intros; simpl; f_equal; auto. Qed.

Lemma close_var_open_nvalue : forall k x nv,
    nvalue nv ->
    {k ~> trm_fvar x}(close_var_rec k x nv) = nv.
Proof. introv. move: k. induction 1; intros; simpl; f_equal; auto. Qed.

Ltac pick_fresh_from_goal y :=
  match goal with
  | [ |- ?t = ?t' ] => pick_fresh y from (fv t \u fv t' \u fv_s t \u fv_s t')
  end.

Lemma close_var_open :
  (forall T, typ T -> forall k x, {k ~> trm_fvar x}(close_var_rec k x T) = T) /\
  (forall e, exp e -> forall k x, {k ~> trm_fvar x}(close_var_rec k x e) = e) /\
  (forall v, value v -> forall k x, {k ~> trm_fvar x}(close_var_rec k x v) = v).
Proof.
  apply typ_exp_value_ind; try easy; simpl; intros; f_equal;
    auto using close_var_open_bits, close_var_open_nvalue;
    try solve [pick_fresh_from_goal y; apply* (@open_var_inj 0 y); rewrite* close_var_open_ind
              |pick_fresh_from_goal y; apply* (@open_var_inj_s 0 y); rewrite* close_var_open_ind_s].
  - calc_open_subst~.
  - pick_fresh_from_goal y. apply* (@open_var_inj 0 y).
    pick_fresh_from_goal f. apply* (@open_var_inj 1 f).
    do 2 rewrite* close_var_open_ind.
Qed.

Lemma close_var_notin : forall x t k,
    x \notin fv (close_var_rec k x t).
Proof.
  induction t; intros; simpl; notin_simpl; auto.
  - case_var; simpl; auto.
Qed.

Lemma subst_as_close_open_typ : forall x u t k,
    exp u ->
    typ t ->
    [x ~> u]t = {k ~> u}(close_var_rec k x t).
Proof.
  intros. rewrite (@subst_intro_gen k x).
  - apply close_var_notin.
  - auto.
  - unfold open, close_var. rewrite* (proj31 close_var_open).
Qed.

Lemma subst_as_close_open_exp : forall x u e k,
    exp u ->
    exp e ->
    [x ~> u]e = {k ~> u}(close_var_rec k x e).
Proof.
  intros. rewrite (@subst_intro_gen k x).
  - apply close_var_notin.
  - auto.
  - unfold open, close_var. rewrite* (proj32 close_var_open).
Qed.

Lemma fv_a_include_fv : forall x t,
    x \notin fv_a t ->
    x \notin fv t.
Proof. induction t; simpl; auto. Qed.

Lemma fv_a_include_fv_s : forall x t,
    x \notin fv_a t ->
    x \notin fv_s t.
Proof. induction t; simpl; auto. Qed.

Lemma fv_a_spec : forall x t,
    x \notin fv_a t <-> x \notin fv t /\ x \notin fv_s t.
Proof.
  intros. split.
  - split.
    + apply (fv_a_include_fv H).
    + apply (fv_a_include_fv_s H).
  - induction t; simpl; intuition.
Qed.

Lemma fv_open_rec : forall t1 t2 x k,
    x \notin fv t1 \u fv t2 ->
    x \notin fv ({k ~> t2}t1).
Proof. induction t1; simpl; intros; auto; calc_open_subst~. Qed.

Lemma fv_open : forall t1 t2 x,
    x \notin fv t1 \u fv t2 ->
    x \notin fv (t1 ^^ t2).
Proof. intros. apply* fv_open_rec. Qed.

Lemma fv_open2 : forall t1 t2 t3 x,
    x \notin fv t1 \u fv t2 \u fv t3 ->
    x \notin fv (t1 ^^ t2 ^^ t3).
Proof. intros. apply* fv_open_rec. notin_solve. apply* fv_open_rec. Qed.

Lemma fv_close_var_rec : forall t x y k,
    x \notin fv t ->
    x \notin fv (close_var_rec k y t).
Proof. induction t; simpl; intros; auto; calc_open_subst~. Qed.

Lemma fv_subst : forall x y u t,
    x \notin (fv t \u fv u) ->
    x \notin fv ([y ~> u]t).
Proof.
  induction t; simpl; intros; auto.
  - calc_open_subst~.
Qed.

Lemma fv_subst_s : forall x y u t,
    x \notin (fv t \u fv u) ->
    x \notin fv (subst_s y u t).
Proof.
  induction t; simpl; intros; auto.
  - calc_open_subst~.
Qed.

Lemma open_fv_gen : forall t1 t2 x k,
    x \notin fv t1 \u fv t2 ->
    x \notin fv ({k ~> t2}t1).
Proof.
  induction t1; simpl; intros; auto.
  - calc_open_subst~.
Qed.

Lemma open_fv : forall t1 t2 x,
    x \notin fv t1 \u fv t2 ->
    x \notin fv (t1 ^^ t2).
Proof. intros. apply* open_fv_gen. Qed.

Lemma open2_fv : forall t1 t2 t3 x,
    x \notin fv t1 \u fv t2 \u fv t3 ->
    x \notin fv (t1 ^^ t2 ^^ t3).
Proof.
  introv. unfold open2. move: 0.
  induction t1; simpl; intros; auto.
  - calc_open_subst~. apply* open_fv_gen.
Qed.

Lemma close_var_rec_fv : forall t k x y,
    x \notin fv t ->
    x \notin fv (close_var_rec k y t).
Proof.
  induction t; simpl; intros; auto.
  - calc_open_subst~.
Qed.

Lemma fv_s_subst_s : forall x y u t,
    x \notin (fv_s t \u fv_s u) ->
    x \notin fv_s (subst_s y u t).
Proof.
  induction t; simpl; intros; auto.
  - calc_open_subst~.
Qed.

Lemma open_fv_s_gen : forall t1 t2 x k,
    x \notin fv_s t1 \u fv_s t2 ->
    x \notin fv_s ({k ~> t2}t1).
Proof.
  induction t1; simpl; intros; auto.
  - calc_open_subst~.
Qed.

Lemma open_fv_s : forall t1 t2 x,
    x \notin fv_s t1 \u fv_s t2 ->
    x \notin fv_s (t1 ^^ t2).
Proof. intros. apply* open_fv_s_gen. Qed.

Lemma open2_fv_s : forall t1 t2 t3 x,
    x \notin fv_s t1 \u fv_s t2 \u fv_s t3 ->
    x \notin fv_s (t1 ^^ t2 ^^ t3).
Proof.
  introv. unfold open2. move: 0.
  induction t1; simpl; intros; auto.
  - calc_open_subst~. apply* open_fv_s_gen.
Qed.

Lemma close_var_rec_fv_s : forall t k x y,
    x \notin fv_s t ->
    x \notin fv_s (close_var_rec k y t).
Proof.
  induction t; simpl; intros; auto.
  - calc_open_subst~.
Qed.

Lemma fv_ex_bound_lv : forall t x y i j,
    x \notin fv t ->
    x \notin fv (close_var_rec i y ({j ~> trm_fvar y}t)).
Proof.
  induction t; simpl; intros; auto.
  - calc_open_subst~.
  - calc_open_subst~.
Qed.
