Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From NMC Require Import NMC_Regularity.

Reserved Notation "e \\ D e'" (at level 68, D at level 9).
Inductive eval : trm -> orth -> trm -> Prop :=
| eval_app : forall D T1 T2 e1 e1' e2 v2 R,
    value v2 ->
    e1 \\D (trm_fix (trm_arrow T1 T2) e1') ->
    e2 \\D v2 ->
    (e1' ^^ v2 ^^ (trm_fix (trm_arrow T1 T2) e1')) \\D R ->
    (trm_app e1 e2) \\D R
| eval_fix : forall D T1 T2 e,
    orthant D ->
    exp (trm_fix (trm_arrow T1 T2) e) ->
    (trm_fix (trm_arrow T1 T2) e) \\D (trm_fix (trm_arrow T1 T2) e)
| eval_zero : forall D,
    orthant D ->
    trm_zero \\D trm_zero
| eval_succ : forall D e nv,
    nvalue nv ->
    e \\D nv ->
    (trm_succ e) \\D (trm_succ nv)
| eval_pred : forall D e nv,
    nvalue nv ->
    e \\D (trm_succ nv) ->
    (trm_pred e) \\D nv
| eval_iszero_t : forall D e,
    e \\D trm_zero ->
    (trm_iszero e) \\D trm_true
| eval_iszero_f : forall D e nv,
    nvalue nv ->
    e \\D (trm_succ nv) ->
    (trm_iszero e) \\D trm_false
| eval_true : forall D,
    orthant D ->
    trm_true \\D trm_true
| eval_false : forall D,
    orthant D ->
    trm_false \\D trm_false
| eval_if_t : forall D e1 e2 e3 R,
    exp e3 ->
    e1 \\D trm_true ->
    e2 \\D R ->
    (trm_if e1 e2 e3) \\D R
| eval_if_f : forall D e1 e2 e3 R,
    exp e2 ->
    e1 \\D trm_false ->
    e3 \\D R ->
    (trm_if e1 e2 e3) \\D R
| eval_sapp : forall D e1 e1' b R,
    bits b ->
    e1 \\D (trm_sabs e1') ->
    (e1' ^^ b) \\D R ->
    (trm_sapp e1 b) \\D R
| eval_sabs : forall D e,
    orthant D ->
    exp (trm_sabs e) ->
    (trm_sabs e) \\D (trm_sabs e)
| eval_waiting : forall D T e1 e2 v R,
    value v ->
    e1 \\D v ->
    (trm_active (e2 ^^ v) v (trm_refine T e2)) \\D R ->
    (trm_waiting e1 (trm_refine T e2)) \\D R
| eval_active_s : forall D T e1 e2 v,
    typ (trm_refine T e2) ->
    value v ->
    e1 \\D trm_true ->
    (trm_active e1 v (trm_refine T e2)) \\D v
| eval_active_f : forall D T e1 e2 v,
    typ (trm_refine T e2) ->
    value v ->
    e1 \\D trm_false ->
    (trm_active e1 v (trm_refine T e2)) \\D trm_blame
| eval_blame : forall D,
    orthant D ->
    trm_blame \\D trm_blame
| eval_c_bool : forall D e v,
    value v ->
    e \\D v ->
    (trm_cast e trm_bool trm_bool) \\D v
| eval_c_nat : forall D e v,
    value v ->
    e \\D v ->
    (trm_cast e trm_nat trm_nat) \\D v
| eval_c_arrow : forall D T11 T12 T21 T22 e v x,
    typ (trm_arrow T11 T12) ->
    typ (trm_arrow T21 T22) ->
    value v ->
    x \notin (fv v) \u (fv T11) \u (fv T12) \u (fv T21) \u (fv T22) ->
    e \\D v ->
    (trm_cast e (trm_arrow T11 T12) (trm_arrow T21 T22))
      \\D (trm_fix (trm_arrow T21 T22)
                   (trm_app (close_var x (trm_fix (trm_arrow T11 (T22 $^ x))
                                                  (trm_cast (trm_app v (trm_bvar 0)) T12 (T22 $^ x))))
                            (trm_cast (trm_bvar 0) T21 T11)))
| eval_c_forall : forall D T1 T2 e v,
    typ (trm_forall T1) ->
    typ (trm_forall T2) ->
    value v ->
    e \\D v ->
    (trm_cast e (trm_forall T1) (trm_forall T2)) \\D (trm_sabs (trm_cast (trm_sapp v (trm_bvar 0)) T1 T2))
| eval_c_forget : forall D T1 T2 e1 e2 v R,
    typ (trm_refine T1 e2) ->
    value v ->
    e1 \\D v ->
    (trm_cast v T1 T2) \\D R ->
    (trm_cast e1 (trm_refine T1 e2) T2) \\D R
| eval_c_precheck : forall D T1 T2 e1 e2 v R,
    value v ->
    (forall T' e', T1 <> (trm_refine T' e')) ->
    e1 \\D v ->
    (trm_waiting (trm_cast v T1 T2) (trm_refine T2 e2)) \\D R ->
    (trm_cast e1 T1 (trm_refine T2 e2)) \\D R
| eval_dist_app_l : forall D e1 e2 R1 R2 R b,
    e1 \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_app R1 e2) (trm_app R2 e2) b) \\D R ->
    (trm_app e1 e2) \\D R
| eval_dist_app_r : forall D e1 e2 R1 R2 R b v,
    value v ->
    e1 \\D v ->
    e2 \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_app v R1) (trm_app v R2) b) \\D R ->
    (trm_app e1 e2) \\D R
| eval_dist_succ : forall D e R1 R2 R b,
    e \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_succ R1) (trm_succ R2) b) \\D R ->
    (trm_succ e) \\D R
| eval_dist_pred : forall D e R1 R2 R b,
    e \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_pred R1) (trm_pred R2) b) \\D R ->
    (trm_pred e) \\D R
| eval_dist_iszero : forall D e R1 R2 R b,
    e \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_iszero R1) (trm_iszero R2) b) \\D R ->
    (trm_iszero e) \\D R
| eval_dist_if : forall D e1 e2 e3 R1 R2 R b,
    e1 \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_if R1 e2 e3) (trm_if R2 e2 e3) b) \\D R ->
    (trm_if e1 e2 e3) \\D R
| eval_dist_sapp : forall D e R1 R2 R b1 b2,
    e \\D (trm_choice R1 R2 b1) ->
    (trm_choice (trm_sapp R1 b2) (trm_sapp R2 b2) b1) \\D R ->
    (trm_sapp e b2) \\D R
| eval_dist_cast : forall D T1 T2 e R1 R2 R b,
    e \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_cast R1 T1 T2) (trm_cast R2 T1 T2) b) \\D R ->
    (trm_cast e T1 T2) \\D R
| eval_dist_waiting : forall D T e1 e2 R1 R2 R b,
    e1 \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_waiting R1 (trm_refine T e2)) (trm_waiting R2 (trm_refine T e2)) b) \\D R ->
    (trm_waiting e1 (trm_refine T e2)) \\D R
| eval_dist_active : forall D T e1 e2 R1 R2 R v b,
    e1 \\D (trm_choice R1 R2 b) ->
    (trm_choice (trm_active R1 v (trm_refine T e2)) (trm_active R2 v (trm_refine T e2)) b) \\D R ->
    (trm_active e1 v (trm_refine T e2)) \\D R
| eval_blame_app_l : forall D e1 e2,
    exp e2 ->
    e1 \\D trm_blame ->
    (trm_app e1 e2) \\D trm_blame
| eval_blame_app_r : forall D e1 e2 v,
    value v ->
    e1 \\D v ->
    e2 \\D trm_blame ->
    (trm_app e1 e2) \\D trm_blame
| eval_blame_succ : forall D e,
    e \\D trm_blame ->
    (trm_succ e) \\D trm_blame
| eval_blame_pred : forall D e,
    e \\D trm_blame ->
    (trm_pred e) \\D trm_blame
| eval_blame_iszero : forall D e,
    e \\D trm_blame ->
    (trm_iszero e) \\D trm_blame
| eval_blame_if : forall D e1 e2 e3,
    exp e2 ->
    exp e3 ->
    e1 \\D trm_blame ->
    (trm_if e1 e2 e3) \\D trm_blame
| eval_blame_sapp : forall D e b,
    bits b ->
    e \\D trm_blame ->
    (trm_sapp e b) \\D trm_blame
| eval_blame_cast : forall D e T1 T2,
    typ T1 ->
    typ T2 ->
    e \\D trm_blame ->
    (trm_cast e T1 T2) \\D trm_blame
| eval_blame_waiting : forall D T e1 e2,
    typ (trm_refine T e2) ->
    e1 \\D trm_blame ->
    (trm_waiting e1 (trm_refine T e2)) \\D trm_blame
| eval_blame_active : forall D T e1 e2 v,
    value v ->
    typ (trm_refine T e2) ->
    e1 \\D trm_blame ->
    (trm_active e1 v (trm_refine T e2)) \\D trm_blame
| eval_choice_l : forall D e1 e2 R b,
    exp e2 ->
    inl b ∈ D ->
    inr b ∉ D ->
    e1 \\D R ->
    (trm_choice e1 e2 b) \\D R
| eval_choice_r : forall D e1 e2 R b,
    exp e1 ->
    inl b ∉ D ->
    inr b ∈ D ->
    e2 \\D R ->
    (trm_choice e1 e2 b) \\D R
| eval_choice : forall D e1 e2 R1 R2 b,
    inl b ∉ D ->
    inr b ∉ D ->
    e1 \\(D ⊌ inl b) R1 ->
    e2 \\(D ⊌ inr b) R2 ->
    (trm_choice e1 e2 b) \\D (trm_choice R1 R2 b)
where "e \\ D e'" := (eval e D e').

Hint Constructors result eval.

Lemma eval_regular_orthant : forall D e1 e2,
    e1 \\D e2 ->
    orthant D.
Proof. induction 1; auto. Qed.

Hint Resolve eval_regular_orthant.

Lemma eval_regular_pre : forall D e1 e2,
    e1 \\D e2 ->
    exp e1.
Proof. induction 1; autos*. Qed.

Hint Resolve eval_regular_pre.

Lemma eval_regular_post : forall D e1 e2,
    e1 \\D e2 ->
    exp e2.
Proof.
  induction 1; autos*.
  - apply* cast_arrow_post_regular.
  - apply* cast_forall_post_regular.
Qed.

Hint Resolve eval_regular_post.

Lemma eval_result : forall D e1 e2,
    e1 \\D e2 ->
    result D e2.
Proof. induction 1; auto; eauto 6. Qed.

Lemma eval_nvalue_idem : forall D nv,
    orthant D ->
    nvalue nv ->
    nv \\D nv.
Proof. induction 2; auto. Qed.

Lemma eval_value_idem : forall D v,
    orthant D ->
    value v ->
    v \\D v.
Proof. induction 2; auto using eval_nvalue_idem. Qed.

Lemma eval_result_idem : forall D R,
    result D R ->
    R \\D R.
Proof. induction 1; auto using eval_value_idem. Qed.

Lemma nvalue_neq_blame : forall nv,
    nvalue nv ->
    nv <> trm_blame.
Proof. destruct 1; discriminate. Qed.

Lemma value_neq_blame : forall v,
    value v ->
    v <> trm_blame.
Proof. destruct 1; try discriminate. apply~ nvalue_neq_blame. Qed.

Lemma nvalue_neq_choice : forall nv e1 e2 b,
    nvalue nv ->
    nv <> (trm_choice e1 e2 b).
Proof. destruct 1; discriminate. Qed.

Lemma value_neq_choice : forall v e1 e2 b,
    value v ->
    v <> (trm_choice e1 e2 b).
Proof. destruct 1; try discriminate. apply~ nvalue_neq_choice. Qed.

Lemma value_blame_inv :
  value trm_blame -> False.
Proof. inversion 1. inversion H0. Qed.

Lemma nvalue_choice_inv : forall e1 e2 b,
    nvalue (trm_choice e1 e2 b) -> False.
Proof. inversion 1. Qed.

Lemma value_choice_inv : forall e1 e2 b,
    value (trm_choice e1 e2 b) -> False.
Proof. inversion 1. inversion H0. Qed.

Ltac try_discriminate2 :=
  try solve [match goal with
             | [ H: nvalue trm_blame |- _ ] =>
               inversion H
             | [ H: value trm_blame |- _ ] =>
               false (value_blame_inv H)
             | [ H: nvalue (trm_choice _ _ _) |- _ ] =>
               inversion H
             | [ H: value (trm_choice _ _ _) |- _ ] =>
               false (value_choice_inv H)
             | [ H: forall T' e', (trm_refine ?T ?e <> trm_refine T' e') |- _ ] =>
               specialize (H T e); congruence
             end].

Lemma close_var_rec_rename : forall x y t k,
    y \notin fv t ->
    close_var_rec k x t = close_var_rec k y ([x ~> trm_fvar y]t).
Proof.
  induction t; simpl; intros; f_equal; auto.
  - calc_open_subst~.
Qed.

Lemma delayed_cast_close_var : forall T11 T12 T21 T22 x x0 v0,
    x \notin fv v0 \u fv T11 \u fv T12 \u fv T21 \u fv T22 ->
    x0 \notin fv v0 \u fv T11 \u fv T12 \u fv T21 \u fv T22 ->
    trm_fix (trm_arrow T21 T22)
            (trm_app (close_var x (trm_fix (trm_arrow T11 (T22 $^ x))
                                           (trm_cast (trm_app v0 (trm_bvar 0)) T12 (T22 $^ x))))
                     (trm_cast (trm_bvar 0) T21 T11)) =
    trm_fix (trm_arrow T21 T22)
            (trm_app (close_var x0 (trm_fix (trm_arrow T11 (T22 $^ x0))
                                            (trm_cast (trm_app v0 (trm_bvar 0)) T12 (T22 $^ x0))))
                     (trm_cast (trm_bvar 0) T21 T11)).
Proof.
  intros. unfold close_var. simpl.
  repeat rewrite~ (@close_var_fresh x T11).
  repeat rewrite~ (@close_var_fresh x0 T11).
  repeat rewrite~ (@close_var_fresh x T12).
  repeat rewrite~ (@close_var_fresh x0 T12).
  repeat rewrite~ (@close_var_fresh x v0).
  repeat rewrite~ (@close_var_fresh x0 v0).
  pick_fresh z. rewrite~ (@subst_intro z). rewrite~ (@subst_intro z (trm_fvar x0)).
  assert (x \notin fv (T22 $^ z)).
  { apply fv_open. simpl. notin_solve. }
  assert (x0 \notin fv (T22 $^ z)).
  { apply fv_open. simpl. notin_solve. }
  repeat rewrite~ <- close_var_rec_rename.
Qed.

Local Ltac forwardsIH :=
  match goal with
  | [ H1: ?e \\?D ?R1, H2: forall R2, ?e \\?D R2 -> ?R3 = R2 |- _ ] =>
    let K := fresh in
    lets K : (H2 R1 H1); clear H1; try inverts K
  end.

Local Ltac try_discriminate :=
  try solve [congruence |
             match goal with
             | [ H: nvalue trm_blame |- _ ] =>
               inversion H
             | [ H: value trm_blame |- _ ] =>
               false (value_blame_inv H)
             | [ H: nvalue (trm_choice _ _ _) |- _ ] =>
               inversion H
             | [ H: value (trm_choice _ _ _) |- _ ] =>
               false (value_choice_inv H)
             | [ H: forall T' e', (trm_refine ?T ?e <> trm_refine T' e') |- _ ] =>
               specialize (H T e); congruence
             end].

Lemma eval_deterministic : forall D e R1 R2,
    e \\D R1 ->
    e \\D R2 ->
    R1 = R2.
Proof.
  introv Eval1. move: R2.
  induction Eval1; introv Eval2; inverts Eval2; repeat forwardsIH; try_discriminate.
  - apply~ delayed_cast_close_var.
Qed.

Lemma eval_keep_fv : forall D e e' x,
    e \\D e' ->
    x \notin fv e ->
    x \notin fv e'.
Proof.
  induction 1; simpl; intros; auto.
  - apply IHeval3. apply~ fv_open2. forwards~ : IHeval1.
  - apply IHeval2. apply~ fv_open.
  - apply IHeval2. simpl. notin_solve. apply~ fv_open. auto.
  - notin_solve.
    + apply* fv_close_var_rec.
    + apply* fv_ex_bound_lv.
    + apply* close_var_rec_fv.
    + apply* close_var_rec_fv.
    + apply* fv_ex_bound_lv.
  - apply IHeval2. simpl. auto.
  - apply IHeval2. simpl. auto.
  - apply IHeval2. simpl. forwards~ : IHeval1.
  - apply IHeval3. simpl. forwards~ : IHeval2.
  - apply IHeval2. simpl. forwards~ : IHeval1.
  - apply IHeval2. simpl. forwards~ : IHeval1.
  - apply IHeval2. simpl. forwards~ : IHeval1.
  - apply IHeval2. simpl. forwards~ : IHeval1.
  - apply IHeval2. simpl. forwards~ : IHeval1.
Qed.
